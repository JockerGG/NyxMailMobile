package com.nyx.mailapptest;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nyx.mailapptest.utils.Preferences;

/**
 *
 * Created by nyx on 2/26/15.
 *
 */
public class BaseActivity extends AppCompatActivity {

    public static boolean sIsActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preferences pref = Preferences.getPreferences(this);
        pref.setAppStatus(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        sIsActive = true;
         // Clear all notification
        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();

        Preferences pref = Preferences.getPreferences(this);
        if (pref.getLockStatus() && pref.hasPassword()) {
            Intent i = new Intent(this, PasswordActivity.class);
            i.putExtra("type",1);
            startActivity(i);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        sIsActive = false;
        Preferences pref = Preferences.getPreferences(this);
        if(!pref.getAppStatus())
            pref.setLockStatus(true); // locking the app
        else
            pref.setLockStatus(false);
    }

}
