package com.nyx.mailapptest;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.nyx.mailapptest.utils.Preferences;

/**
 *
 * Created by nyx on 2/26/15.
 *
 */
public class SecuritySettingsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {

    private static final String PREFERENCE_USE_PASSWORD = "UsarPassword";
    private static final String PREFERENCE_CHANGE_PASSWORD = "ChangePass";

    private CheckBoxPreference prefUsePassword;
    private Preference prefChangePassword;

    private Preferences pref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        pref = Preferences.getPreferences(getActivity());

        setupBar();

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.security_preferences);

        prefUsePassword = (CheckBoxPreference) findPreference(PREFERENCE_USE_PASSWORD);
        prefChangePassword = findPreference(PREFERENCE_CHANGE_PASSWORD);

        prefUsePassword.setChecked(pref.hasPassword());
        prefChangePassword.setOnPreferenceClickListener(this);
    }

    @Override
    public void onPause() {
        saveSettings();
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPreferenceClick(Preference pref) {
        if(pref == prefChangePassword){
            Intent i = new Intent(getActivity(), PasswordActivity.class);
            i.putExtra("type",2);
            startActivity(i);
            return true;
        }
        return false;
    }

    private void setupBar(){
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        activity.getSupportActionBar().setTitle(R.string.lbl_security);
    }

    private void saveSettings(){
        pref.setHasPassword(prefUsePassword.isChecked());
    }
}
