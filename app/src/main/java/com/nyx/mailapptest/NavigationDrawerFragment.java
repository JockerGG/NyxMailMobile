package com.nyx.mailapptest;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nyx.mailapptest.adapters.FoldersAdapter;
import com.nyx.mailapptest.custom.view.RoundedImageView;
import com.nyx.mailapptest.search.SearchAccount;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.BaseAccount;
import com.nyx.mailapptest.utils.Preferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment implements View.OnClickListener{

    private static final int SPECIAL_ACCOUNTS_COUNT = 2;
    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private RoundedImageView mUserImage;
    private ImageButton fabAccount, btnSettings;
    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerListView;
    private View mFragmentContainerView;

    //private AccountsHandler mHandler = new AccountsHandler();
    private SearchAccount mAllMessagesAccount = null;
    private SearchAccount mUnifiedInboxAccount = null;
    private FoldersAdapter mAdapter;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;

    private Account[] accounts;
    private HashMap<BaseAccount, List<String>> listDataChild;

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }

        // Select either the default item (0) or the last selected item.
       // selectItem(0, mCurrentSelectedPosition);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        mDrawerListView = (ExpandableListView)v.findViewById(R.id.drawer_list);
        btnSettings = (ImageButton)v.findViewById(R.id.btn_settings);
        fabAccount = (ImageButton)v.findViewById(R.id.fabAccount);
        mUserImage = (RoundedImageView)v.findViewById(R.id.img_user);

        prepareListData();

        if(getUserProfilePhoto(getActivity()) != null)
            mUserImage.setImageBitmap(getUserProfilePhoto(getActivity()));

        fabAccount.setOnClickListener(this);
        btnSettings.setOnClickListener(this);

        mDrawerListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                TextView folder = (TextView)v.findViewById(R.id.lbl_folder);
                selectItem(groupPosition, childPosition, folder.getText().toString());
                return false;
            }
        });
        mDrawerListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if(groupPosition <= 1)
                    selectGroup(groupPosition);
                return false;
            }
        });
        //mDrawerListView.setAdapter(new FoldersAdapter(getActivity(), listDataHeader, listDataChild));
        mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);

        int size = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 280, getResources().getDisplayMetrics());
        int right = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        int left = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 46, getResources().getDisplayMetrics());
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mDrawerListView.setIndicatorBounds(size - left, size - right);
        } else {
            mDrawerListView.setIndicatorBoundsRelative(size - left, size - right);
        }
        return v;
    }

    @Override
    public void onClick(View v){
        if(v == fabAccount) {
            newAccount();
        }else if(v == btnSettings){
            toSettings();
        }
    }

    public ActionBarDrawerToggle getDrawerToggle(){
        return mDrawerToggle;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void closeDrawer() {
         mDrawerLayout.closeDrawer(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                toolbar,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void toSettings(){
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerSettings();
        }
    }

    private void newAccount(){
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerIntent();
        }
    }
    private void selectGroup(int position){

        mAdapter.selectedItem(position, 0);

        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            switch (position){
                case 0:
                    mCallbacks.onNavigationDrawerIntent(mUnifiedInboxAccount);
                    break;
                case 1:
                    mCallbacks.onNavigationDrawerIntent(mAllMessagesAccount);
                    break;
                default:
                    mCallbacks.onNavigationDrawerIntent(mUnifiedInboxAccount);
                    break;
            }
        }
    }

    public void selectChild(int pos){
        mAdapter.setSelectedChild(pos);
    }

    private void selectItem(int parent, int position, String folder) {
        mCurrentSelectedPosition = position;
        mAdapter.selectedItem(parent, position);

        if (mDrawerListView != null) {
            mDrawerListView.setSelectedChild(parent, position,true);
            //mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected((Account)mAdapter.getGroup(parent), position, folder);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    public void refresh(){
        mAdapter = null;
        mDrawerListView.setAdapter(mAdapter);
        prepareListData();
    }

    private void prepareListData() {
        listDataChild = new HashMap<>();

        // Adding child data
        List<String> top250 = new ArrayList<>();
        top250.add(getString(R.string.special_mailbox_name_inbox));
        top250.add(getString(R.string.special_mailbox_name_drafts));
        top250.add(getString(R.string.special_mailbox_name_sent));
        top250.add(getString(R.string.special_mailbox_name_archive));
        top250.add(getString(R.string.special_mailbox_name_trash));
        top250.add(getString(R.string.special_mailbox_name_spam));

        accounts = null;

        //Account[]aux = Preferences.getPreferences(getActivity()).getAvailableAccounts().
        accounts = Preferences.getPreferences(getActivity()).getAccounts();

        List<Account> auxList = new LinkedList<>(Arrays.asList(accounts));
        for(int i = 0; i < auxList.size(); i++){
            Account mAc = auxList.get(i);
            if(mAc != null) {
                if (mAc == Preferences.getPreferences(getActivity()).getDefaultAccount()) {
                    auxList.remove(i);
                    auxList.add(0, mAc);
                }
            }else{
                auxList.remove(i);
            }
        }

        // see if we should show the welcome message
//        if (accounts.length < 1) {
//            WelcomeMessage.showWelcomeMessage(this);
//            finish();
//        }

        List<BaseAccount> newAccounts;
        if (accounts.length > 0) {
            if (mUnifiedInboxAccount == null || mAllMessagesAccount == null) {
                createSpecialAccounts();
            }

            newAccounts = new ArrayList<>(accounts.length +
                    SPECIAL_ACCOUNTS_COUNT);
            newAccounts.add(mUnifiedInboxAccount);
            newAccounts.add(mAllMessagesAccount);
        } else {
            newAccounts = new ArrayList<>(accounts.length);
        }

        newAccounts.addAll(auxList);

        for(int i = 2; i < newAccounts.size(); i++){
            listDataChild.put(newAccounts.get(i),top250);
        }

        /*
        mAdapter = new AccountsAdapter(newAccounts);
        getListView().setAdapter(mAdapter);
        */
        if (!newAccounts.isEmpty()) {
            mAdapter = new FoldersAdapter(getActivity(), newAccounts, listDataChild);
            mDrawerListView.setAdapter(mAdapter);
            mDrawerListView.expandGroup(2);
        }

            //mHandler.progress(Window.PROGRESS_START);
        //mHandler.refreshTitle();

       // MessagingController controller = MessagingController.getInstance(getActivity());
    }

    private void createSpecialAccounts() {
        mUnifiedInboxAccount = SearchAccount.createUnifiedInboxAccount(getActivity());
        mAllMessagesAccount = SearchAccount.createAllMessagesAccount(getActivity());
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(Account parent, int child, String folder);
        void onNavigationDrawerIntent();
        void onNavigationDrawerIntent(BaseAccount account);
        void onNavigationDrawerSettings();
    }

    //Get Photo
    private static Bitmap getUserProfilePhoto(Context context) {
        Uri imageUri = null;
        final ContentResolver content = context.getContentResolver();

        try {
            final Cursor cursor = content.query(
                    // Retrieves data rows for the device user's 'profile' contact
                    Uri.withAppendedPath(
                            ContactsContract.Profile.CONTENT_URI,
                            ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
                    new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO_URI,
                            ContactsContract.Contacts.Data.MIMETYPE},

                    // Selects only email addresses or names
                    ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                            + ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                            + ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                            + ContactsContract.Contacts.Data.MIMETYPE + "=?",
                    new String[]{
                            ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                    },

                    // Show primary rows first. Note that there won't be a primary email address if the
                    // user hasn't specified one.
                    ContactsContract.Contacts.Data.IS_PRIMARY + " DESC"
            );

            String mime_type;
            while (cursor.moveToNext()) {
                mime_type = cursor.getString(1);
                if (mime_type.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE))
                    imageUri = Uri.parse(cursor.getString(0));
            }

            cursor.close();

            try {
                return MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
            } catch (IOException e) {
                return null;
            }
        }catch(Exception e){
            return null;
        }
    }
}
