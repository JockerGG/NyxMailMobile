package com.nyx.mailapptest.custom.multiselector;

/**
 *
 * Created by nyx on 1/28/15.
 *
 */
public interface SelectableHolder {
    void setSelectable(boolean selectable);
    boolean isSelectable();
    void setActivated(boolean activated);
    boolean isActivated();
    int getPosition();
    long getItemId();
}