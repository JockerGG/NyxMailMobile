package com.nyx.mailapptest.custom.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 *
 * Created by nyx on 2/6/15.
 *
 */
public class EolConvertingEditText extends EditText {

    public EolConvertingEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Return the text the EolConvertingEditText is displaying.
     *
     * @return A string with any line endings converted to {@code \r\n}.
     */
    public String getCharacters() {
        return getText().toString().replace("\n", "\r\n");
    }

    /**
     * Sets the string value of the EolConvertingEditText. Any line endings
     * in the string will be converted to {@code \n}.
     *
     * @param text
     */
    public void  setCharacters(CharSequence text) {
        setText(text.toString().replace("\r\n", "\n"));
    }
}
