package com.nyx.mailapptest;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nyx.mailapptest.controller.MessagingController;
import com.nyx.mailapptest.data.App;
import com.nyx.mailapptest.data.NYXMail;
import com.nyx.mailapptest.lib.AuthType;
import com.nyx.mailapptest.lib.AuthenticationFailedException;
import com.nyx.mailapptest.lib.CertificateValidationException;
import com.nyx.mailapptest.lib.ConnectionSecurity;
import com.nyx.mailapptest.lib.ServerSettings;
import com.nyx.mailapptest.lib.Store;
import com.nyx.mailapptest.lib.Transport;
import com.nyx.mailapptest.lib.store.ImapStore;
import com.nyx.mailapptest.lib.store.WebDavStore;
import com.nyx.mailapptest.lib.transport.SmtpTransport;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;
import com.nyx.mailapptest.utils.Utility;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Locale;

/**
 *
 * Created by nyx on 1/25/15.
 *
 */
public class NewAccountActivity extends BaseActivity implements
        AccountTypeFragment.OnAccountListener,
        NewAccountFragment.OnNewAccountListener,
        ManualIncomingFragment.OnIncomingListener,
        ManualOutgoingFragment.OnOutgoingListener{

    private Account mAccount;
    private Provider mProvider;
    private ProgressDialog mProgress;

    private boolean mManual = false;
    private boolean mCanceled;
    private boolean mDestroyed;
    private boolean mFromActivity = false;
    private Boolean mHasConnectivity;
    private boolean isGmail = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        Bundle extras = getIntent().getExtras();

        if(extras != null)
            mFromActivity = extras.getBoolean("newAccount");

        //restoreActionBar(mFromActivity);

        if (findViewById(R.id.container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            AccountTypeFragment firstFragment = new AccountTypeFragment();

            // Add the fragment to the 'fragment_container' FrameLayout
            FragmentTransaction trans = getFragmentManager().beginTransaction()
                    .add(R.id.container, AccountTypeFragment.newInstance(mFromActivity));
            trans.commit();
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        App.activity5 = this;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        App.activity5 = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        mHasConnectivity = Utility.hasConnectivity(this.getApplication());
    }

    @Override
    public void onBackPressed() {
        // turn on the Navigation Drawer image;
        // this is called in the LowerLevelFragments
        if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            //restoreActionBar(mFromActivity);
            if(getFragmentManager().getBackStackEntryCount() == 1)
                mManual = false;
        }else
            super.onBackPressed();
    }

    @Override
    public void onAccountSelected(String type, int icon){

        if(type.equals("Gmail")){
            isGmail = true;
        }else{
            isGmail = false;
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction
                .setCustomAnimations(R.animator.push_left_in,R.animator.push_left_out,R.animator.push_right_in,R.animator.push_right_out)
                .replace(R.id.container, NewAccountFragment.newInstance(type, icon)).addToBackStack(null)
                .commit();
    }

    @Override
    public void onAccountCreated(String email, String password, int icon){
        finishAutoSetup(email,password, icon);
        /*
        Intent i = new Intent(NewAccountActivity.this,MainActivity.class);
        startActivity(i);
        finish();
        */
    }

    @Override
    public void incomingCreated(Account account){
        mAccount = account;
        mManual = true;
        accountSettings(CheckDirection.INCOMING);
    }

    @Override
    public void outgoingCreated(Account account){
        mAccount = account;
        mManual = false;
        accountSettings(CheckDirection.OUTGOING);
    }

    private void showNicknameDialog(){
        final Context mContext = this;
        final Resources res = getResources();
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
                new Runnable() {
                    @Override
                    public void run() {
                        new MaterialDialog.Builder(mContext)
                                .cancelable(false)
                                .title(R.string.title_nickname)
                                .customView(R.layout.dialog_account_name, false)
                                .titleColor(res.getColor(R.color.accent))
                                .positiveColor(res.getColor(R.color.accent))
                                .positiveText(R.string.btn_ok)
                                .callback(new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onPositive(MaterialDialog dialog) {
                                        View v = dialog.getCustomView();
                                        EditText edit = (EditText) v.findViewById(R.id.editNickname);
                                        toAccounts(edit.getText().toString());
                                        super.onPositive(dialog);
                                    }
                                })
                                .show();
                    }
                }
        );
    }

    private void showErrorDialog(final int title, final String content){
        final Context mContext = this;
        final Resources res = getResources();
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
                new Runnable(){
                    @Override
                    public void run()
                    {
                        new MaterialDialog.Builder(mContext)
                                .content(title)
                                .titleColor(res.getColor(R.color.accent))
                                .positiveColor(res.getColor(R.color.accent))
                                .positiveText(R.string.btn_ok)
                                .show();

                    }
                }
        );
    }

    private void onManualSetup(String email, String password){
        String[] emailParts = splitEmail(email);
        String user = emailParts[0];
        String domain = emailParts[1];

        AuthType authenticationType = AuthType.PLAIN;

        if (mAccount == null) {
            mAccount = Preferences.getPreferences(this).newAccount();
        }
        mAccount.setName(getOwnerName());
        mAccount.setEmail(email);

        // set default uris
        // NOTE: they will be changed again in AccountSetupAccountType!
        ServerSettings storeServer = new ServerSettings(ImapStore.STORE_TYPE, "mailapp." + domain, -1,
                ConnectionSecurity.SSL_TLS_REQUIRED, authenticationType, user, password, null);
        ServerSettings transportServer = new ServerSettings(SmtpTransport.TRANSPORT_TYPE, "mailapp." + domain, -1,
                ConnectionSecurity.SSL_TLS_REQUIRED, authenticationType, user, password, null);
        String storeUri = Store.createStoreUri(storeServer);
        String transportUri = Transport.createTransportUri(transportServer);
        mAccount.setStoreUri(storeUri);
        mAccount.setTransportUri(transportUri);

        mAccount.setDraftsFolderName(getString(R.string.special_mailbox_name_drafts));
        mAccount.setTrashFolderName(getString(R.string.special_mailbox_name_trash));
        mAccount.setSentFolderName(getString(R.string.special_mailbox_name_sent));
        mAccount.setArchiveFolderName(getString(R.string.special_mailbox_name_archive));
        // Yahoo! has a special folder for Spam, called "Bulk Mail".
        if (domain.endsWith(".yahoo.com")) {
            mAccount.setSpamFolderName("Bulk Mail");
        } else {
            mAccount.setSpamFolderName(getString(R.string.special_mailbox_name_spam));
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction
                .setCustomAnimations(R.animator.push_left_in,R.animator.push_left_out,R.animator.push_right_in,R.animator.push_right_out)
                .replace(R.id.container, ManualIncomingFragment.newInstance(mAccount))
                .addToBackStack(null)
                .commit();
    }

    private void toOutgoing(){
        try {

            ServerSettings settings = Transport.decodeTransportUri(mAccount.getTransportUri());

            URI oldUri = new URI(mAccount.getTransportUri());
            ServerSettings transportServer = new ServerSettings(SmtpTransport.TRANSPORT_TYPE, oldUri.getHost(), oldUri.getPort(),
                    ConnectionSecurity.SSL_TLS_REQUIRED, AuthType.PLAIN, settings.username, settings.password, null);
            String transportUri = Transport.createTransportUri(transportServer);
            mAccount.setTransportUri(transportUri);
        } catch (URISyntaxException use) {

        }

        try {
            if (new URI(mAccount.getStoreUri()).getScheme().startsWith("webdav")) {
                mAccount.setTransportUri(mAccount.getStoreUri());
                mManual = false;
                accountSettings(CheckDirection.OUTGOING);
                return;
            }
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction
                .setCustomAnimations(R.animator.push_left_in,R.animator.push_left_out,R.animator.push_right_in,R.animator.push_right_out)
                .replace(R.id.container, ManualOutgoingFragment.newInstance(mAccount))
                .addToBackStack(null)
                .commit();
    }

    private void finishAutoSetup(String email, String password, int icon) {
        String[] emailParts = splitEmail(email);
        String user = emailParts[0];
        String domain = emailParts[1];

        mProvider = findProviderForDomain(domain);
        if (mProvider == null) {
            /*
             * We don't have default settings for this account, start the manual
             * setup process.
             */
            onManualSetup(email, password);
            return;
        }

        if (mProvider.note != null) {
            showErrorDialog(R.string.alert_setup_failed_title, "");
            return;
        }

        URI incomingUri;
        URI outgoingUri;
        try {
            String userEnc = URLEncoder.encode(user, "UTF-8");
            String passwordEnc = URLEncoder.encode(password, "UTF-8");

            String incomingUsername = mProvider.incomingUsernameTemplate;
            incomingUsername = incomingUsername.replaceAll("\\$email", email);
            incomingUsername = incomingUsername.replaceAll("\\$user", userEnc);
            incomingUsername = incomingUsername.replaceAll("\\$domain", domain);

            URI incomingUriTemplate = mProvider.incomingUriTemplate;
            incomingUri = new URI(incomingUriTemplate.getScheme(), incomingUsername + ":"
                    + passwordEnc, incomingUriTemplate.getHost(), incomingUriTemplate.getPort(), null,
                    null, null);

            String outgoingUsername = mProvider.outgoingUsernameTemplate;

            URI outgoingUriTemplate = mProvider.outgoingUriTemplate;


            if (outgoingUsername != null) {
                outgoingUsername = outgoingUsername.replaceAll("\\$email", email);
                outgoingUsername = outgoingUsername.replaceAll("\\$user", userEnc);
                outgoingUsername = outgoingUsername.replaceAll("\\$domain", domain);
                outgoingUri = new URI(outgoingUriTemplate.getScheme(), outgoingUsername + ":"
                        + passwordEnc, outgoingUriTemplate.getHost(), outgoingUriTemplate.getPort(), null,
                        null, null);

            } else {
                outgoingUri = new URI(outgoingUriTemplate.getScheme(),
                        null, outgoingUriTemplate.getHost(), outgoingUriTemplate.getPort(), null,
                        null, null);


            }
            if (mAccount == null) {
                mAccount = Preferences.getPreferences(this).newAccount();
            }
            mAccount.setName(getOwnerName());
            mAccount.setEmail(email);
            mAccount.setStoreUri(incomingUri.toString());
            mAccount.setTransportUri(outgoingUri.toString());
            mAccount.setDraftsFolderName(getString(R.string.special_mailbox_name_drafts));
            mAccount.setTrashFolderName(getString(R.string.special_mailbox_name_trash));
            mAccount.setArchiveFolderName(getString(R.string.special_mailbox_name_archive));
            mAccount.setIcon(icon);
            // Yahoo! has a special folder for Spam, called "Bulk Mail".
            if (incomingUriTemplate.getHost().toLowerCase(Locale.US).endsWith(".yahoo.com")) {
                mAccount.setSpamFolderName("Bulk Mail");
            } else {
                mAccount.setSpamFolderName(getString(R.string.special_mailbox_name_spam));
            }
            mAccount.setSentFolderName(getString(R.string.special_mailbox_name_sent));
            if (incomingUri.toString().startsWith("imap")) {
                mAccount.setDeletePolicy(Account.DELETE_POLICY_ON_DELETE);
            } else if (incomingUri.toString().startsWith("pop3")) {
                mAccount.setDeletePolicy(Account.DELETE_POLICY_NEVER);
            }
            // Check incoming here.  Then check outgoing in onActivityResult()
            accountSettings(CheckDirection.INCOMING);
        } catch (UnsupportedEncodingException enc) {
            // This really shouldn't happen since the encoding is hardcoded to UTF-8
            Log.e(NYXMail.LOG_TAG, "Couldn't urlencode username or password.", enc);
        } catch (URISyntaxException use) {
            /*
             * If there is some problem with the URI we give up and go on to
             * manual setup.
             */
            onManualSetup(email, password);
        }
    }

    private void toAccounts(String nickname){
        if(nickname.isEmpty()) {
            showNicknameDialog();
            Toast.makeText(this,R.string.toast_empty_name, Toast.LENGTH_SHORT).show();
        }else {
            mAccount.setDescription(nickname);
            mAccount.setName(nickname);
            mAccount.save(Preferences.getPreferences(this));
            NYXMail.setServicesEnabled(this);

            Intent i = new Intent(NewAccountActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    private void accountSettings(CheckDirection direction){
        new CheckAccountTask(mAccount).execute(direction);
    }

    private void setMessage(final int resId) {
        mProgress.setMessage(getString(resId));
    }

    class CheckAccountTask extends AsyncTask<CheckDirection, Integer, Void> {
        private final Account account;

        CheckAccountTask(Account account) {
            this.account = account;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(mProgress == null)
                mProgress = ProgressDialog.show(NewAccountActivity.this,"",getString(R.string.alert_progress));
            else
                mProgress.show();
            mDestroyed = false;
            mCanceled = false;

        }

        @Override
        protected Void doInBackground(CheckDirection... params) {
            final CheckDirection direction = params[0];
            try {
                if (mDestroyed) {
                    return null;
                }
                if (mCanceled) {
                    finish();
                    return null;
                }
                final MessagingController ctrl = MessagingController.getInstance(getApplication());
                //ctrl.clearCertificateErrorNotifications(NewAccountActivity.this,account, direction);

                if (direction == CheckDirection.INCOMING) {
                    Store store = account.getRemoteStore();
                    if (store instanceof WebDavStore) {
                        publishProgress(R.string.account_setup_check_settings_authenticate);
                    } else {
                        publishProgress(R.string.account_setup_check_settings_check_incoming_msg);
                    }
                    store.checkSettings();

                    if (store instanceof WebDavStore) {
                        publishProgress(R.string.account_setup_check_settings_fetch);
                    }
                    MessagingController.getInstance(getApplication()).listFoldersSynchronous(account, true, null);
                    MessagingController.getInstance(getApplication())
                            .synchronizeMailbox(account, account.getInboxFolderName(), null, null);
                }
                if (mDestroyed) {
                    return null;
                }
                if (mCanceled) {
                    finish();
                    return null;
                }
                if (direction == CheckDirection.OUTGOING) {
                    if (!(account.getRemoteStore() instanceof WebDavStore)) {
                        publishProgress(R.string.account_setup_check_settings_check_outgoing_msg);
                    }
                    Transport transport = Transport.getInstance(account);
                    transport.close();
                    transport.open();
                    transport.close();
                }
                if (mDestroyed) {
                    return null;
                }
                if (mCanceled) {
                    finish();
                    return null;
                }
                if(mProgress != null && mProgress.isShowing())
                    mProgress.dismiss();

                if(!mManual)
                    showNicknameDialog();
                else
                    toOutgoing();
            } catch (AuthenticationFailedException afe) {
                this.cancel(true);
                if(mProgress != null && mProgress.isShowing())
                    mProgress.dismiss();
                Preferences.getPreferences(getApplicationContext()).deleteAccount(mAccount);
                mAccount = null;
                if(isGmail){
                    showErrorDialog(
                            R.string.alert_setup_failed_dlg_auth_message_fmt_google,
                            afe.getMessage() == null ? "" : afe.getMessage());
                }else {
                    showErrorDialog(
                            R.string.alert_setup_failed_dlg_auth_message_fmt,
                            afe.getMessage() == null ? "" : afe.getMessage());
                }
            } catch (CertificateValidationException cve) {
                //handleCertificateValidationException(cve);
            } catch (Throwable t) {
                this.cancel(true);
                if(mProgress != null && mProgress.isShowing())
                    mProgress.dismiss();
                Preferences.getPreferences(getApplicationContext()).deleteAccount(mAccount);
                mAccount = null;
                if(!mHasConnectivity){
                    showErrorDialog(R.string.alert_no_internet,"");
                }else {
                    showErrorDialog(R.string.alert_setup_failed_dlg_server_message_fmt,"");
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            setMessage(values[0]);
        }

        @Override
        protected void onCancelled(){
            mAccount = null;
            mDestroyed = true;
            mCanceled = true;
        }
    }

    private String getOwnerName() {
        String name = null;
        try {
            name = getDefaultAccountName();
        } catch (Exception e) {
        }

        if (name == null) {
            name = "";
        }
        return name;
    }

    private String getDefaultAccountName() {
        String name = null;
        Account account = Preferences.getPreferences(this).getDefaultAccount();
        if (account != null) {
            name = account.getName();
        }
        return name;
    }

    private String[] splitEmail(String email) {
        String[] retParts = new String[2];
        String[] emailParts = email.split("@");
        retParts[0] = (emailParts.length > 0) ? emailParts[0] : "";
        retParts[1] = (emailParts.length > 1) ? emailParts[1] : "";
        return retParts;
    }

    private String getXmlAttribute(XmlResourceParser xml, String name) {
        int resId = xml.getAttributeResourceValue(null, name, 0);
        if (resId == 0) {
            return xml.getAttributeValue(null, name);
        } else {
            return getString(resId);
        }
    }

    private Provider findProviderForDomain(String domain) {
        try {
            XmlResourceParser xml = getResources().getXml(R.xml.providers);
            int xmlEventType;
            Provider provider = null;
            while ((xmlEventType = xml.next()) != XmlResourceParser.END_DOCUMENT) {
                if (xmlEventType == XmlResourceParser.START_TAG
                        && "provider".equals(xml.getName())
                        && domain.equalsIgnoreCase(getXmlAttribute(xml, "domain"))) {
                    provider = new Provider();
                    provider.id = getXmlAttribute(xml, "id");
                    provider.label = getXmlAttribute(xml, "label");
                    provider.domain = getXmlAttribute(xml, "domain");
                    provider.note = getXmlAttribute(xml, "note");
                } else if (xmlEventType == XmlResourceParser.START_TAG
                        && "incoming".equals(xml.getName())
                        && provider != null) {
                    provider.incomingUriTemplate = new URI(getXmlAttribute(xml, "uri"));
                    provider.incomingUsernameTemplate = getXmlAttribute(xml, "username");
                } else if (xmlEventType == XmlResourceParser.START_TAG
                        && "outgoing".equals(xml.getName())
                        && provider != null) {
                    provider.outgoingUriTemplate = new URI(getXmlAttribute(xml, "uri"));
                    provider.outgoingUsernameTemplate = getXmlAttribute(xml, "username");
                } else if (xmlEventType == XmlResourceParser.END_TAG
                        && "provider".equals(xml.getName())
                        && provider != null) {
                    return provider;
                }
            }
        } catch (Exception e) {
            Log.e("Account", "Error al obtener los providers.", e);
        }
        return null;
    }

    public enum CheckDirection {
        INCOMING,
        OUTGOING
    }

    static class Provider implements Serializable {
        private static final long serialVersionUID = 8511656164616538989L;

        public String id;

        public String label;

        public String domain;

        public URI incomingUriTemplate;

        public String incomingUsernameTemplate;

        public URI outgoingUriTemplate;

        public String outgoingUsernameTemplate;

        public String note;
    }

}
