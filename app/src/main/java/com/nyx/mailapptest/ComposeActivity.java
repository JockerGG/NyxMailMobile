package com.nyx.mailapptest;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.util.Rfc822Tokenizer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nyx.mailapptest.activity.Attachment;
import com.nyx.mailapptest.activity.AttachmentContentLoader;
import com.nyx.mailapptest.activity.AttachmentInfoLoader;
import com.nyx.mailapptest.activity.InsertableHtmlContent;
import com.nyx.mailapptest.activity.MessageReference;
import com.nyx.mailapptest.activity.TextBodyBuilder;
import com.nyx.mailapptest.adapters.AccountsComposeAdapter;
import com.nyx.mailapptest.controller.MessagingController;
import com.nyx.mailapptest.controller.MessagingListener;
import com.nyx.mailapptest.crypto.PgpData;
import com.nyx.mailapptest.custom.view.EolConvertingEditText;
import com.nyx.mailapptest.custom.view.MessageWebView;
import com.nyx.mailapptest.data.App;
import com.nyx.mailapptest.data.NYXMail;
import com.nyx.mailapptest.helper.Contacts;
import com.nyx.mailapptest.helper.HtmlConverter;
import com.nyx.mailapptest.helper.IdentityHelper;
import com.nyx.mailapptest.lib.Body;
import com.nyx.mailapptest.lib.Flag;
import com.nyx.mailapptest.lib.Message;
import com.nyx.mailapptest.lib.Message.RecipientType;
import com.nyx.mailapptest.lib.Address;
import com.nyx.mailapptest.lib.MessagingException;
import com.nyx.mailapptest.lib.Multipart;
import com.nyx.mailapptest.lib.Part;
import com.nyx.mailapptest.lib.internet.MimeBodyPart;
import com.nyx.mailapptest.lib.internet.MimeHeader;
import com.nyx.mailapptest.lib.internet.MimeMessage;
import com.nyx.mailapptest.lib.internet.MimeMultipart;
import com.nyx.mailapptest.lib.internet.MimeUtility;
import com.nyx.mailapptest.lib.internet.TextBody;
import com.nyx.mailapptest.lib.store.LocalStore.LocalAttachmentBody;
import com.nyx.mailapptest.lib.store.LocalStore.TempFileBody;
import com.nyx.mailapptest.lib.store.LocalStore.TempFileMessageBody;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Account.MessageFormat;
import com.nyx.mailapptest.utils.Account.QuoteStyle;
import com.nyx.mailapptest.adapters.EmailAddressAdapter;
import com.nyx.mailapptest.utils.EmailAddressValidator;
import com.nyx.mailapptest.utils.Identity;
import com.nyx.mailapptest.utils.Preferences;
import com.nyx.mailapptest.utils.Utility;

import org.apache.james.mime4j.codec.EncoderUtil;
import org.apache.james.mime4j.util.MimeUtil;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.SimpleHtmlSerializer;
import org.htmlcleaner.TagNode;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Created by nyx on 1/25/15.
 *
 */
public class ComposeActivity extends BaseActivity implements View.OnClickListener{

    private static final String[] INITIAL_PERMS={
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS
    };
    private static final String[] STORAGE_PERMS={
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int INITIAL_REQUEST = 133;
    private static final int STORAGE_REQUEST = 134;

    private static final String ACTION_COMPOSE = "com.nyx.mailapp.intent.action.COMPOSE";
    private static final String ACTION_REPLY = "com.nyx.mailapp.intent.action.REPLY";
    private static final String ACTION_REPLY_ALL = "com.nyx.mailapp.intent.action.REPLY_ALL";
    private static final String ACTION_FORWARD = "com.nyx.mailapp.intent.action.FORWARD";
    private static final String ACTION_EDIT_DRAFT = "com.nyx.mailapp.intent.action.EDIT_DRAFT";

    private static final String STATE_KEY_ATTACHMENTS =
            "com.nyx.mailapp.ComposeActivity.attachments";
    private static final String STATE_KEY_CC_SHOWN =
            "com.nyx.mailapp.ComposeActivity.ccShown";
    private static final String STATE_KEY_BCC_SHOWN =
            "com.nyx.mailapp.ComposeActivity.bccShown";
    private static final String STATE_KEY_QUOTED_TEXT_MODE =
            "com.nyx.mailapp.ComposeActivity.QuotedTextShown";
    private static final String STATE_KEY_SOURCE_MESSAGE_PROCED =
            "com.nyx.mailapp.ComposeActivity.stateKeySourceMessageProced";
    private static final String STATE_KEY_DRAFT_ID = "com.nyx.mailapp.ComposeActivity.draftId";
    private static final String STATE_KEY_HTML_QUOTE = "com.nyx.mailapp.ComposeActivity.HTMLQuote";
    private static final String STATE_IDENTITY_CHANGED =
            "com.nyx.mailapp.ComposeActivity.identityChanged";
    private static final String STATE_IDENTITY =
            "com.nyx.mailapp.ComposeActivity.identity";
    private static final String STATE_PGP_DATA = "pgpData";
    private static final String STATE_IN_REPLY_TO = "com.nyx.mailapp.ComposeActivity.inReplyTo";
    private static final String STATE_REFERENCES = "com.nyx.mailapp.ComposeActivity.references";
    private static final String STATE_KEY_READ_RECEIPT = "com.nyx.mailapp.ComposeActivity.messageReadReceipt";
    private static final String STATE_KEY_DRAFT_NEEDS_SAVING = "com.nyx.mailapp.ComposeActivity.mDraftNeedsSaving";
    private static final String STATE_KEY_FORCE_PLAIN_TEXT =
            "com.nyx.mailapp.ComposeActivity.forcePlainText";
    private static final String STATE_KEY_QUOTED_TEXT_FORMAT =
            "com.nyx.mailapp.ComposeActivity.quotedTextFormat";
    private static final String STATE_KEY_NUM_ATTACHMENTS_LOADING = "numAttachmentsLoading";
    private static final String STATE_KEY_WAITING_FOR_ATTACHMENTS = "waitingForAttachments";

    private static final String EXTRA_ACCOUNT = "account";
    private static final String EXTRA_MESSAGE_BODY  = "messageBody";
    private static final String EXTRA_MESSAGE_REFERENCE = "message_reference";

    private static final String LOADER_ARG_ATTACHMENT = "attachment";
    private static final String IDENTITY_VERSION_1 = "!";

    private static final int ACTIVITY_REQUEST_PICK_ATTACHMENT = 1;

    private static final long INVALID_DRAFT_ID = MessagingController.INVALID_MESSAGE_ID;

    private static final int MSG_PROGRESS_ON = 1;
    private static final int MSG_PROGRESS_OFF = 2;
    private static final int MSG_SKIPPED_ATTACHMENTS = 3;
    private static final int MSG_SAVED_DRAFT = 4;
    private static final int MSG_DISCARDED_DRAFT = 5;
    private static final int MSG_PERFORM_STALLED_ACTION = 6;

    private static final int REPLY_WRAP_LINE_WIDTH = 72;
    private static final int QUOTE_BUFFER_LENGTH = 512;

    private static final Pattern PREFIX = Pattern.compile("^AW[:\\s]\\s*", Pattern.CASE_INSENSITIVE);

    private static final Pattern FIND_INSERTION_POINT_HTML = Pattern.compile("(?si:.*?(<html(?:>|\\s+[^>]*>)).*)");
    private static final Pattern FIND_INSERTION_POINT_HEAD = Pattern.compile("(?si:.*?(<head(?:>|\\s+[^>]*>)).*)");
    private static final Pattern FIND_INSERTION_POINT_BODY = Pattern.compile("(?si:.*?(<body(?:>|\\s+[^>]*>)).*)");
    private static final Pattern FIND_INSERTION_POINT_HTML_END = Pattern.compile("(?si:.*(</html>).*?)");
    private static final Pattern FIND_INSERTION_POINT_BODY_END = Pattern.compile("(?si:.*(</body>).*?)");

    private static final Pattern DASH_SIGNATURE_PLAIN = Pattern.compile("\r\n-- \r\n.*", Pattern.DOTALL);
    private static final Pattern DASH_SIGNATURE_HTML = Pattern.compile("(<br( /)?>|\r?\n)-- <br( /)?>", Pattern.CASE_INSENSITIVE);
    private static final Pattern BLOCKQUOTE_START = Pattern.compile("<blockquote", Pattern.CASE_INSENSITIVE);
    private static final Pattern BLOCKQUOTE_END = Pattern.compile("</blockquote>", Pattern.CASE_INSENSITIVE);

    private static final int FIND_INSERTION_POINT_FIRST_GROUP = 1;
    private static final String FIND_INSERTION_POINT_HTML_CONTENT = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n<html>";
    private static final String FIND_INSERTION_POINT_HTML_END_CONTENT = "</html>";
    private static final String FIND_INSERTION_POINT_HEAD_CONTENT = "<head><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\"></head>";
    private static final int FIND_INSERTION_POINT_START_OF_STRING = 0;

    enum Action {
        COMPOSE,
        REPLY,
        REPLY_ALL,
        FORWARD,
        EDIT_DRAFT
    }

    enum SimpleMessageFormat {
        TEXT,
        HTML
    }

    private enum QuotedTextMode {
        NONE,
        SHOW,
        HIDE
    }

    private enum WaitingAction {
        NONE,
        SEND,
        SAVE
    }

    private enum IdentityField {
        LENGTH("l"),
        OFFSET("o"),
        FOOTER_OFFSET("fo"),
        PLAIN_LENGTH("pl"),
        PLAIN_OFFSET("po"),
        MESSAGE_FORMAT("f"),
        MESSAGE_READ_RECEIPT("r"),
        SIGNATURE("s"),
        NAME("n"),
        EMAIL("e"),
        // TODO - store a reference to the message being replied so we can mark it at the time of send.
        ORIGINAL_MESSAGE("m"),
        CURSOR_POSITION("p"),   // Where in the message your cursor was when you saved.
        QUOTED_TEXT_MODE("q"),
        QUOTE_STYLE("qs");

        private final String value;

        IdentityField(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }

        /**
         * Get the list of IdentityFields that should be integer values.
         *
         * <p>
         * These values are sanity checked for integer-ness during decoding.
         * </p>
         *
         * @return The list of integer {@link IdentityField}s.
         */
        public static IdentityField[] getIntegerFields() {
            return new IdentityField[] { LENGTH, OFFSET, FOOTER_OFFSET, PLAIN_LENGTH, PLAIN_OFFSET };
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_PROGRESS_ON:
                    setSupportProgressBarIndeterminate(true);
                    break;
                case MSG_PROGRESS_OFF:
                    setSupportProgressBarIndeterminate(false);
                    break;
                case MSG_SKIPPED_ATTACHMENTS:
                    Toast.makeText(
                            ComposeActivity.this,
                            getString(R.string.compose_attachments_skipped_toast),
                            Toast.LENGTH_SHORT).show();
                    break;
                case MSG_SAVED_DRAFT:
                    Toast.makeText(
                            ComposeActivity.this,
                            getString(R.string.message_saved_toast),
                            Toast.LENGTH_SHORT).show();
                    break;
                case MSG_DISCARDED_DRAFT:
                    Toast.makeText(
                            ComposeActivity.this,
                            getString(R.string.message_discarded_toast),
                            Toast.LENGTH_SHORT).show();
                    break;
                case MSG_PERFORM_STALLED_ACTION:
                    performStalledAction();
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    };

    private Account mAccount;
    private Contacts mContacts;
    private Identity mIdentity;

    private boolean mIdentityChanged = false;
    private boolean mSignatureChanged = false;

    private String mSourceMessageBody;
    private MessageReference mMessageReference;
    private Message mSourceMessage;
    private PgpData mPgpData = null;

    private ProgressDialog loading;

    private Spinner spinnerAccounts;
    private LinearLayout layoutCc,layoutBcc,layoutAttachments;
    private RelativeLayout mQuotedTextBar;
    private MultiAutoCompleteTextView editTo, editCc,editBcc;
    private ImageButton btnExpand;
    private EolConvertingEditText editMessage;
    private EolConvertingEditText mQuotedText;
    private EolConvertingEditText mSignatureView;
    private EditText editSubject;
    private MessageWebView quoteView;

    private InsertableHtmlContent mQuotedHtmlContent;

    private Listener mListener = new Listener();
    private EmailAddressAdapter mAddressAdapter;
    private AutoCompleteTextView.Validator mAddressValidator;

    private SimpleMessageFormat mQuotedTextFormat;
    private SimpleMessageFormat mMessageFormat;
    private QuotedTextMode mQuotedTextMode = QuotedTextMode.NONE;
    private QuoteStyle mQuoteStyle;

    private boolean shown = false;
    private boolean mIgnoreOnPause = false;
    private boolean mSourceMessageProcessed = false;
    private boolean mReadReceipt = false;
    private boolean mDraftNeedsSaving = false;
    private boolean mPreventDraftSaving = false;
    private boolean mSourceProcessed = false;
    private boolean mForcePlainText = false;

    private int mMaxLoaderId = 0;
    private int mNumAttachmentsLoading = 0;

    private long mDraftId = INVALID_DRAFT_ID;
    private WaitingAction mWaitingForAttachments = WaitingAction.NONE;

    private String mReferences;
    private String mInReplyTo;

    private Action mAction;

    public static void actionCompose(Context context, Account account) {
        String accountUuid = (account == null) ?
                Preferences.getPreferences(context).getDefaultAccount().getUuid() :
                account.getUuid();

        Intent i = new Intent(context, ComposeActivity.class);
        i.putExtra(EXTRA_ACCOUNT, accountUuid);
        i.setAction(ACTION_COMPOSE);
        context.startActivity(i);
    }

    public static void actionReply(Context context, Message message, boolean replyAll, String messageBody) {
        context.startActivity(getActionReplyIntent(context, message, replyAll, messageBody));
    }

    public static void actionForward(Context context, Message message, String messageBody) {
        Intent i = new Intent(context, ComposeActivity.class);
        i.putExtra(EXTRA_MESSAGE_BODY, messageBody);
        i.putExtra(EXTRA_MESSAGE_REFERENCE, message.makeMessageReference());
        i.setAction(ACTION_FORWARD);
        context.startActivity(i);
    }

    public static void actionEditDraft(Context context, MessageReference messageReference) {
        Intent i = new Intent(context, ComposeActivity.class);
        i.putExtra(EXTRA_MESSAGE_REFERENCE, messageReference);
        i.setAction(ACTION_EDIT_DRAFT);
        context.startActivity(i);
    }

    public static Intent getActionReplyIntent(Context context,Account account,Message message,boolean replyAll,String messageBody) {
        Intent i = new Intent(context, ComposeActivity.class);
        i.putExtra(EXTRA_MESSAGE_BODY, messageBody);
        i.putExtra(EXTRA_MESSAGE_REFERENCE, message.makeMessageReference());
        if (replyAll) {
            i.setAction(ACTION_REPLY_ALL);
        } else {
            i.setAction(ACTION_REPLY);
        }
        return i;
    }

    public static Intent getActionReplyIntent(Context context, Message message, boolean replyAll, String messageBody) {
        Intent i = new Intent(context, ComposeActivity.class);
        i.putExtra(EXTRA_MESSAGE_BODY, messageBody);
        i.putExtra(EXTRA_MESSAGE_REFERENCE, message.makeMessageReference());
        if (replyAll) {
            i.setAction(ACTION_REPLY_ALL);
        } else {
            i.setAction(ACTION_REPLY);
        }
        return i;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(PackageManager.PERMISSION_GRANTED == checkSelfPermission(Manifest.permission.READ_CONTACTS)))
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
        }

        spinnerAccounts = (Spinner)findViewById(R.id.spinAccount);
        layoutCc = (LinearLayout)findViewById(R.id.layout_cc);
        layoutBcc = (LinearLayout)findViewById(R.id.layout_bcc);
        layoutAttachments = (LinearLayout)findViewById(R.id.layout_attachments);
        editTo = (MultiAutoCompleteTextView)findViewById(R.id.edit_to);
        editCc = (MultiAutoCompleteTextView)findViewById(R.id.edit_cc);
        editBcc = (MultiAutoCompleteTextView)findViewById(R.id.edit_bcc);
        editSubject = (EditText)findViewById(R.id.edit_subject);
        editMessage = (EolConvertingEditText)findViewById(R.id.message_body);
        mQuotedTextBar = (RelativeLayout)findViewById(R.id.quoted_text_bar);
        mQuotedText = (EolConvertingEditText)findViewById(R.id.quoted_text);
        quoteView = (MessageWebView)findViewById(R.id.quote);
        btnExpand = (ImageButton)findViewById(R.id.btn_expand);

        EolConvertingEditText upperSignature = (EolConvertingEditText)findViewById(R.id.upper_signature);
        EolConvertingEditText lowerSignature = (EolConvertingEditText)findViewById(R.id.lower_signature);

        spinnerAccounts.setAdapter(new AccountsComposeAdapter(this));
        mAddressAdapter = new EmailAddressAdapter(getApplicationContext());
        mAddressValidator = new EmailAddressValidator();

        btnExpand.setOnClickListener(this);

        final Intent intent = getIntent();

        mMessageReference = intent.getParcelableExtra(EXTRA_MESSAGE_REFERENCE);
        mSourceMessageBody = intent.getStringExtra(EXTRA_MESSAGE_BODY);

        final String accountUuid = (mMessageReference != null) ?
                mMessageReference.accountUuid :
                intent.getStringExtra(EXTRA_ACCOUNT);

        mAccount = Preferences.getPreferences(this).getAccount(accountUuid);

        if (mAccount == null) {
            mAccount = Preferences.getPreferences(this).getDefaultAccount();
        }

        mContacts = Contacts.getInstance(ComposeActivity.this);
        loading = new ProgressDialog(this);
        loading.setMessage("Cargando archivos adjuntos...");
        loading.setCancelable(false);

        mQuotedText.getInputExtras(true).putBoolean("allowEmoji", true);
        quoteView.configure();
        quoteView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }
        });

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int after) {
                /* do nothing */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDraftNeedsSaving = true;
            }

            @Override
            public void afterTextChanged(android.text.Editable s) { /* do nothing */ }
        };

        // For watching changes to the To:, Cc:, and Bcc: fields for auto-encryption on a matching
        // address.
        TextWatcher recipientWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int after) {
                /* do nothing */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDraftNeedsSaving = true;
            }

            @Override
            public void afterTextChanged(android.text.Editable s) {
                /* do nothing */
            }
        };

        TextWatcher sigWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int after) {
                /* do nothing */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDraftNeedsSaving = true;
                mSignatureChanged = true;
            }

            @Override
            public void afterTextChanged(android.text.Editable s) { /* do nothing */ }
        };

        editTo.setAdapter(mAddressAdapter);
        editTo.setTokenizer(new Rfc822Tokenizer());
        editTo.setValidator(mAddressValidator);
        editTo.addTextChangedListener(recipientWatcher);

        editCc.setAdapter(mAddressAdapter);
        editCc.setTokenizer(new Rfc822Tokenizer());
        editCc.setValidator(mAddressValidator);
        editCc.addTextChangedListener(recipientWatcher);

        editBcc.setAdapter(mAddressAdapter);
        editBcc.setTokenizer(new Rfc822Tokenizer());
        editBcc.setValidator(mAddressValidator);
        editBcc.addTextChangedListener(recipientWatcher);

        editSubject.addTextChangedListener(watcher);
        editMessage.addTextChangedListener(watcher);

        if (initFromIntent(intent)) {
            mAction = Action.COMPOSE;
            mDraftNeedsSaving = true;
        } else {
            String action = intent.getAction();
            if (ACTION_COMPOSE.equals(action)) {
                mAction = Action.COMPOSE;
            } else if (ACTION_REPLY.equals(action)) {
                mAction = Action.REPLY;
            } else if (ACTION_REPLY_ALL.equals(action)) {
                mAction = Action.REPLY_ALL;
            } else if (ACTION_FORWARD.equals(action)) {
                mAction = Action.FORWARD;
            } else if (ACTION_EDIT_DRAFT.equals(action)) {
                mAction = Action.EDIT_DRAFT;
            } else {
                // This shouldn't happen
                mAction = Action.COMPOSE;
            }
        }

        if (mIdentity == null) {
            mIdentity = mAccount.getIdentity(0);
        }

        if (mAccount.isSignatureBeforeQuotedText()) {
            mSignatureView = upperSignature;
            lowerSignature.setVisibility(View.GONE);
        } else {
            mSignatureView = lowerSignature;
            upperSignature.setVisibility(View.GONE);
        }
        mSignatureView.addTextChangedListener(sigWatcher);

        if (!mIdentity.getSignatureUse()) {
            mSignatureView.setVisibility(View.GONE);
        }

        mReadReceipt = mAccount.isMessageReadReceiptAlways();
        mQuoteStyle = mAccount.getQuoteStyle();

        updateFrom();

        if (!mSourceMessageProcessed) {
            updateSignature();

            if (mAction == Action.REPLY || mAction == Action.REPLY_ALL ||
                    mAction == Action.FORWARD || mAction == Action.EDIT_DRAFT) {
                /*
                 * If we need to load the message we add ourself as a message listener here
                 * so we can kick it off. Normally we add in onResume but we don't
                 * want to reload the message every time the activity is resumed.
                 * There is no harm in adding twice.
                 */
                MessagingController.getInstance(getApplication()).addListener(mListener);

                final Account account = Preferences.getPreferences(this).getAccount(mMessageReference.accountUuid);
                final String folderName = mMessageReference.folderName;
                final String sourceMessageUid = mMessageReference.uid;
                MessagingController.getInstance(getApplication()).loadMessageForView(account, folderName, sourceMessageUid, null);
            }

            if (mAction != Action.EDIT_DRAFT) {
                addAddresses(editBcc, mAccount.getAlwaysBcc());
            }
        }

        if (mAction == Action.REPLY || mAction == Action.REPLY_ALL) {
            mMessageReference.flag = Flag.ANSWERED;
        }

        if (mAction == Action.REPLY || mAction == Action.REPLY_ALL ||
                mAction == Action.EDIT_DRAFT) {
            //change focus to message body.
            editMessage.requestFocus();
        } else {
            // Explicitly set focus to "To:" input field (see issue 2998)
            editTo.requestFocus();
        }

        if (mAction == Action.FORWARD) {
            mMessageReference.flag = Flag.FORWARDED;
        }

        showOrHideQuotedText(QuotedTextMode.NONE);
        
        initializeCrypto();

        updateMessageFormat();

        setTitle();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIgnoreOnPause = false;
        MessagingController.getInstance(getApplication()).addListener(mListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        MessagingController.getInstance(getApplication()).removeListener(mListener);
        // Save email as draft when activity is changed (go to home screen, call received) or screen locked
        // don't do this if only changing orientations
        if (!mIgnoreOnPause && (getChangingConfigurations() & ActivityInfo.CONFIG_ORIENTATION) == 0) {
            saveIfNeeded();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<Attachment> attachments = new ArrayList<Attachment>();
        for (int i = 0, count = layoutAttachments.getChildCount(); i < count; i++) {
            View view = layoutAttachments.getChildAt(i);
            Attachment attachment = (Attachment) view.getTag();
            attachments.add(attachment);
        }

        outState.putInt(STATE_KEY_NUM_ATTACHMENTS_LOADING, mNumAttachmentsLoading);
        outState.putString(STATE_KEY_WAITING_FOR_ATTACHMENTS, mWaitingForAttachments.name());
        outState.putParcelableArrayList(STATE_KEY_ATTACHMENTS, attachments);
        outState.putBoolean(STATE_KEY_CC_SHOWN, layoutCc.getVisibility() == View.VISIBLE);
        outState.putBoolean(STATE_KEY_BCC_SHOWN, layoutBcc.getVisibility() == View.VISIBLE);
        outState.putSerializable(STATE_KEY_QUOTED_TEXT_MODE, mQuotedTextMode);
        outState.putBoolean(STATE_KEY_SOURCE_MESSAGE_PROCED, mSourceMessageProcessed);
        outState.putLong(STATE_KEY_DRAFT_ID, mDraftId);
        outState.putSerializable(STATE_IDENTITY, mIdentity);
        outState.putBoolean(STATE_IDENTITY_CHANGED, mIdentityChanged);
        outState.putSerializable(STATE_PGP_DATA, mPgpData);
        outState.putString(STATE_IN_REPLY_TO, mInReplyTo);
        outState.putString(STATE_REFERENCES, mReferences);
        outState.putSerializable(STATE_KEY_HTML_QUOTE, mQuotedHtmlContent);
        outState.putBoolean(STATE_KEY_READ_RECEIPT, mReadReceipt);
        outState.putBoolean(STATE_KEY_DRAFT_NEEDS_SAVING, mDraftNeedsSaving);
        outState.putBoolean(STATE_KEY_FORCE_PLAIN_TEXT, mForcePlainText);
        outState.putSerializable(STATE_KEY_QUOTED_TEXT_FORMAT, mQuotedTextFormat);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        layoutAttachments.removeAllViews();
        mMaxLoaderId = 0;

        mNumAttachmentsLoading = savedInstanceState.getInt(STATE_KEY_NUM_ATTACHMENTS_LOADING);
        mWaitingForAttachments = WaitingAction.NONE;
        try {
            String waitingFor = savedInstanceState.getString(STATE_KEY_WAITING_FOR_ATTACHMENTS);
            mWaitingForAttachments = WaitingAction.valueOf(waitingFor);
        } catch (Exception e) {
            Log.w(NYXMail.LOG_TAG, "Couldn't read value \" + STATE_KEY_WAITING_FOR_ATTACHMENTS +" +
                    "\" from saved instance state", e);
        }

        List<Attachment> attachments = savedInstanceState.getParcelableArrayList(STATE_KEY_ATTACHMENTS);
        for (Attachment attachment : attachments) {
            addAttachmentView(attachment);
            if (attachment.loaderId > mMaxLoaderId) {
                mMaxLoaderId = attachment.loaderId;
            }

            if (attachment.state == Attachment.LoadingState.URI_ONLY) {
                initAttachmentInfoLoader(attachment);
            } else if (attachment.state == Attachment.LoadingState.METADATA) {
                initAttachmentContentLoader(attachment);
            }
        }

        mReadReceipt = savedInstanceState
                .getBoolean(STATE_KEY_READ_RECEIPT);
        layoutCc.setVisibility(savedInstanceState.getBoolean(STATE_KEY_CC_SHOWN) ? View.VISIBLE
                : View.GONE);
        layoutBcc.setVisibility(savedInstanceState
                .getBoolean(STATE_KEY_BCC_SHOWN) ? View.VISIBLE : View.GONE);

        // This method is called after the action bar menu has already been created and prepared.
        // So compute the visibility of the "Add Cc/Bcc" menu item again.
        //computeAddCcBccVisibility();

        mQuotedHtmlContent =
                (InsertableHtmlContent) savedInstanceState.getSerializable(STATE_KEY_HTML_QUOTE);
        if (mQuotedHtmlContent != null && mQuotedHtmlContent.getQuotedContent() != null) {
            quoteView.setText(mQuotedHtmlContent.getQuotedContent());
        }

        mDraftId = savedInstanceState.getLong(STATE_KEY_DRAFT_ID);
        mIdentity = (Identity)savedInstanceState.getSerializable(STATE_IDENTITY);
        mIdentityChanged = savedInstanceState.getBoolean(STATE_IDENTITY_CHANGED);
        mPgpData = (PgpData) savedInstanceState.getSerializable(STATE_PGP_DATA);
        mInReplyTo = savedInstanceState.getString(STATE_IN_REPLY_TO);
        mReferences = savedInstanceState.getString(STATE_REFERENCES);
        mDraftNeedsSaving = savedInstanceState.getBoolean(STATE_KEY_DRAFT_NEEDS_SAVING);
        mForcePlainText = savedInstanceState.getBoolean(STATE_KEY_FORCE_PLAIN_TEXT);
        mQuotedTextFormat = (SimpleMessageFormat) savedInstanceState.getSerializable(
                STATE_KEY_QUOTED_TEXT_FORMAT);

        showOrHideQuotedText(
                (QuotedTextMode) savedInstanceState.getSerializable(STATE_KEY_QUOTED_TEXT_MODE));

        initializeCrypto();
        updateFrom();
        updateSignature();
        //updateEncryptLayout();

        updateMessageFormat();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (data == null) {
            return;
        }
        switch (requestCode) {
            case ACTIVITY_REQUEST_PICK_ATTACHMENT:
                addAttachmentsFromResultIntent(data);
                mDraftNeedsSaving = true;
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode) {
            case STORAGE_REQUEST:
                if (canAccessStorage()) {
                    onAddAttachment();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.compose, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                overridePendingTransition(R.anim.appear_zoom, R.anim.disappear_bottom);
                return true;
            case R.id.action_attach:
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!(PackageManager.PERMISSION_GRANTED == checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE))){
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    }else
                        onAddAttachment();
                }else
                    onAddAttachment();
                return true;
            case R.id.action_send:
                onSend();
                return true;
            case R.id.action_draft:
                onSave();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        App.activity3 = this;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        App.activity3 = null;
        if(mAddressAdapter != null)
            mAddressAdapter.closeCursor();
    }

    @Override
    public void onBackPressed(){
        if (mDraftNeedsSaving) {
            if (!mAccount.hasDraftsFolder()) {
                showDiscardDialog();
            }else{
                showSaveDiscardDialog();
            }
        }else{
            if (mDraftId == INVALID_DRAFT_ID) {
                onDiscard();
            }else {
                super.onBackPressed();
                overridePendingTransition(R.anim.appear_zoom, R.anim.disappear_bottom);
            }
        }
    }

    @Override
    public void onClick(View v){
        if(v == btnExpand){
            onAddCcBcc(!shown);
        }else{
            layoutAttachments.removeView((View) v.getTag());
        }
    }

    private void setTitle() {
        switch (mAction) {
            case REPLY: {
                setTitle(R.string.title_reply);
                break;
            }
            case REPLY_ALL: {
                setTitle(R.string.title_reply_all);
                break;
            }
            case FORWARD: {
                setTitle(R.string.title_forward);
                break;
            }
            case COMPOSE:
            default: {
                setTitle(R.string.title_compose);
                break;
            }
        }
    }

    private void showOrHideQuotedText(QuotedTextMode mode) {
        mQuotedTextMode = mode;
        switch (mode) {
            case NONE:
            case HIDE: {
                mQuotedTextBar.setVisibility(View.GONE);
                mQuotedText.setVisibility(View.GONE);
                quoteView.setVisibility(View.GONE);
                //mQuotedTextEdit.setVisibility(View.GONE);
                break;
            }
            case SHOW: {
                //mQuotedTextShow.setVisibility(View.GONE);
                mQuotedTextBar.setVisibility(View.VISIBLE);

                if (mQuotedTextFormat == SimpleMessageFormat.HTML) {
                    mQuotedText.setVisibility(View.GONE);
                    quoteView.setVisibility(View.VISIBLE);
                    //mQuotedTextEdit.setVisibility(View.VISIBLE);
                } else {
                    mQuotedText.setVisibility(View.VISIBLE);
                    quoteView.setVisibility(View.GONE);
                    //mQuotedTextEdit.setVisibility(View.GONE);
                }
                break;
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void addAttachmentsFromResultIntent(Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ClipData clipData = data.getClipData();
            if (clipData != null) {
                for (int i = 0, end = clipData.getItemCount(); i < end; i++) {
                    Uri uri = clipData.getItemAt(i).getUri();
                    if (uri != null) {
                        addAttachment(uri);
                    }
                }
                return;
            }
        }

        Uri uri = data.getData();
        if (uri != null) {
            addAttachment(uri);
        }
    }

    private void initializeCrypto() {
        if (mPgpData != null) {
            return;
        }
        mPgpData = new PgpData();
    }

    private void switchToIdentity(Identity identity) {
        mIdentity = identity;
        mIdentityChanged = true;
        mDraftNeedsSaving = true;
        updateFrom();
        //updateBcc();
        updateSignature();
        updateMessageFormat();
    }

    private void updateFrom() {
        spinnerAccounts.setSelection(0);
    }

    private void updateSignature() {
        if (mIdentity.getSignatureUse()) {
            mSignatureView.setCharacters(mIdentity.getSignature());
            mSignatureView.setVisibility(View.VISIBLE);
        } else {
            mSignatureView.setVisibility(View.GONE);
        }
    }

    private void onSend(){
        if (getAddresses(editTo).length == 0 && getAddresses(editCc).length == 0 && getAddresses(editBcc).length == 0) {
            Toast.makeText(this, getString(R.string.error_recipients), Toast.LENGTH_SHORT).show();
            return;
        }

        if (mWaitingForAttachments != WaitingAction.NONE) {
            return;
        }

        if (mNumAttachmentsLoading > 0) {
            mWaitingForAttachments = WaitingAction.SEND;
            if(loading != null && !loading.isShowing())
                loading.show();
        } else {
            performSend();
        }

    }

    private void performSend() {
        /*
        if (mOpenPgpProvider != null) {
            // OpenPGP Provider API

            // If not already encrypted but user wants to encrypt...
            if (mPgpData.getEncryptedData() == null &&
                    (mEncryptCheckbox.isChecked() || mCryptoSignatureCheckbox.isChecked())) {

                String[] emailsArray = null;
                if (mEncryptCheckbox.isChecked()) {
                    // get emails as array
                    List<String> emails = new ArrayList<String>();

                    for (Address address : getRecipientAddresses()) {
                        emails.add(address.getAddress());
                    }
                    emailsArray = emails.toArray(new String[emails.size()]);
                }
                if (mEncryptCheckbox.isChecked() && mCryptoSignatureCheckbox.isChecked()) {
                    Intent intent = new Intent(OpenPgpApi.ACTION_SIGN_AND_ENCRYPT);
                    intent.putExtra(OpenPgpApi.EXTRA_USER_IDS, emailsArray);
                    executeOpenPgpMethod(intent);
                } else if (mCryptoSignatureCheckbox.isChecked()) {
                    Intent intent = new Intent(OpenPgpApi.ACTION_SIGN);
                    executeOpenPgpMethod(intent);
                } else if (mEncryptCheckbox.isChecked()) {
                    Intent intent = new Intent(OpenPgpApi.ACTION_ENCRYPT);
                    intent.putExtra(OpenPgpApi.EXTRA_USER_IDS, emailsArray);
                    executeOpenPgpMethod(intent);
                }

                // onSend() is called again in SignEncryptCallback and with
                // encryptedData set in pgpData!
                return;
            }
        }*/
        sendMessage();

        if (mMessageReference != null && mMessageReference.flag != null) {

            final Account account = Preferences.getPreferences(this).getAccount(mMessageReference.accountUuid);
            final String folderName = mMessageReference.folderName;
            final String sourceMessageUid = mMessageReference.uid;
            MessagingController.getInstance(getApplication()).setFlag(account, folderName, sourceMessageUid, mMessageReference.flag, true);
        }

        mDraftNeedsSaving = false;
        finish();
        overridePendingTransition(R.anim.appear_zoom, R.anim.disappear_bottom);
    }

    private void sendMessage() {
        new SendMessageTask().execute();
    }

    private void onDiscard() {
        if (mDraftId != INVALID_DRAFT_ID) {
            MessagingController.getInstance(getApplication()).deleteDraft(mAccount, mDraftId);
            mDraftId = INVALID_DRAFT_ID;
        }
        mHandler.sendEmptyMessage(MSG_DISCARDED_DRAFT);
        mDraftNeedsSaving = false;
        finish();
        overridePendingTransition(R.anim.appear_zoom, R.anim.disappear_bottom);
    }

    private void onSave(){
        if (mWaitingForAttachments != WaitingAction.NONE) {
            return;
        }

        if (mNumAttachmentsLoading > 0) {
            mWaitingForAttachments = WaitingAction.SAVE;
            if(loading != null && !loading.isShowing())
                loading.show();
        } else {
            performSave();
        }
    }

    private void performSave() {
        saveIfNeeded();
        finish();
        overridePendingTransition(R.anim.appear_zoom, R.anim.disappear_bottom);
    }

    private void saveMessage() {
        new SaveMessageTask().execute();
    }

    private void saveIfNeeded() {
        if (!mDraftNeedsSaving || mPreventDraftSaving || mPgpData.hasEncryptionKeys() || !mAccount.hasDraftsFolder()) {
            return;
        }

        mDraftNeedsSaving = false;
        saveMessage();
    }

    private void showSaveDiscardDialog(){
        Resources res = getResources();
        new MaterialDialog.Builder(this)
                .title(R.string.alert_save_discard_title)
                .content(R.string.alert_save_discard)
                .titleColor(res.getColor(R.color.accent))
                .positiveColor(res.getColor(R.color.accent))
                .negativeColor(res.getColor(R.color.gray_text))
                .positiveText(R.string.btn_save_draft)
                .negativeText(R.string.btn_discard)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        onSave();
                    }
                    @Override
                    public void onNegative(MaterialDialog dialog){
                        onDiscard();
                    }
                })
                .show();
    }

    private void showDiscardDialog(){
        Resources res = getResources();
        new MaterialDialog.Builder(this)
                .title(R.string.alert_discard_title)
                .content(R.string.alert_discard)
                .titleColor(res.getColor(R.color.accent))
                .positiveColor(res.getColor(R.color.accent))
                .negativeColor(res.getColor(R.color.gray_text))
                .positiveText(R.string.btn_cancel)
                .negativeText(R.string.btn_discard)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onNegative(MaterialDialog dialog){
                        Toast.makeText(ComposeActivity.this,R.string.toast_discard,Toast.LENGTH_SHORT).show();
                        onDiscard();
                    }
                })
                .show();
    }

    private Map<IdentityField, String> parseIdentityHeader(final String identityString) {
        Map<IdentityField, String> identity = new HashMap<IdentityField, String>();

        if (NYXMail.DEBUG) {
            Log.d(NYXMail.LOG_TAG, "Decoding identity: " + identityString);
        }

        if (identityString == null || identityString.length() < 1) {
            return identity;
        }

        // Check to see if this is a "next gen" identity.
        if (identityString.charAt(0) == IDENTITY_VERSION_1.charAt(0) && identityString.length() > 2) {
            Uri.Builder builder = new Uri.Builder();
            builder.encodedQuery(identityString.substring(1));  // Need to cut off the ! at the beginning.
            Uri uri = builder.build();
            for (IdentityField key : IdentityField.values()) {
                String value = uri.getQueryParameter(key.value());
                if (value != null) {
                    identity.put(key, value);
                }
            }

            // Sanity check our Integers so that recipients of this result don't have to.
            for (IdentityField key : IdentityField.getIntegerFields()) {
                if (identity.get(key) != null) {
                    try {
                        Integer.parseInt(identity.get(key));
                    } catch (NumberFormatException e) {
                        Log.e(NYXMail.LOG_TAG, "Invalid " + key.name() + " field in identity: " + identity.get(key));
                    }
                }
            }
        } else {
            // Legacy identity

            if (NYXMail.DEBUG) {
                Log.d(NYXMail.LOG_TAG, "Got a saved legacy identity: " + identityString);
            }
            StringTokenizer tokenizer = new StringTokenizer(identityString, ":", false);

            // First item is the body length. We use this to separate the composed reply from the quoted text.
            if (tokenizer.hasMoreTokens()) {
                String bodyLengthS = Utility.base64Decode(tokenizer.nextToken());
                try {
                    identity.put(IdentityField.LENGTH, Integer.valueOf(bodyLengthS).toString());
                } catch (Exception e) {
                    Log.e(NYXMail.LOG_TAG, "Unable to parse bodyLength '" + bodyLengthS + "'");
                }
            }
            if (tokenizer.hasMoreTokens()) {
                identity.put(IdentityField.SIGNATURE, Utility.base64Decode(tokenizer.nextToken()));
            }
            if (tokenizer.hasMoreTokens()) {
                identity.put(IdentityField.NAME, Utility.base64Decode(tokenizer.nextToken()));
            }
            if (tokenizer.hasMoreTokens()) {
                identity.put(IdentityField.EMAIL, Utility.base64Decode(tokenizer.nextToken()));
            }
            if (tokenizer.hasMoreTokens()) {
                identity.put(IdentityField.QUOTED_TEXT_MODE, Utility.base64Decode(tokenizer.nextToken()));
            }
        }

        return identity;
    }

    private void onAddAttachment() {
        onAddAttachment2("*/*");
    }

    /**
     * Kick off a picker for the specified MIME type and let Android take over.
     *
     * @param mime_type
     *         The MIME type we want our attachment to have.
     */
    @SuppressLint("InlinedApi")
    private void onAddAttachment2(final String mime_type) {
        if (mAccount.getOpenPgpProvider() != null) {
            //Toast.makeText(this, R.string.attachment_encryption_unsupported, Toast.LENGTH_LONG).show();
        }
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType(mime_type);
        mIgnoreOnPause = true;
        startActivityForResult(Intent.createChooser(i, null), ACTIVITY_REQUEST_PICK_ATTACHMENT);
    }

    private boolean loadAttachments(Part part, int depth) throws MessagingException {
        if (part.getBody() instanceof Multipart) {
            Multipart mp = (Multipart) part.getBody();
            boolean ret = true;
            for (int i = 0, count = mp.getCount(); i < count; i++) {
                if (!loadAttachments(mp.getBodyPart(i), depth + 1)) {
                    ret = false;
                }
            }
            return ret;
        }

        String contentType = MimeUtility.unfoldAndDecode(part.getContentType());
        String name = MimeUtility.getHeaderParameter(contentType, "name");
        if (name != null) {
            Body body = part.getBody();
            if (body != null && body instanceof LocalAttachmentBody) {
                final Uri uri = ((LocalAttachmentBody) body).getContentUri();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        addAttachment(uri);
                    }
                });
            } else {
                return false;
            }
        }
        return true;
    }

    private void onAddCcBcc(boolean visible) {
        shown = visible;
        btnExpand.setImageResource(visible ? R.drawable.ic_action_collapse : R.drawable.ic_action_expand);
        layoutCc.setVisibility(visible ? View.VISIBLE: View.GONE);
        layoutBcc.setVisibility(visible ? View.VISIBLE: View.GONE);
    }

    private void addAddresses(MultiAutoCompleteTextView view, String addresses) {
        if (TextUtils.isEmpty(addresses)) {
            return;
        }
        for (String address : addresses.split(",")) {
            addAddress(view, new Address(address, ""));
        }
    }

    private void addAddresses(MultiAutoCompleteTextView view, Address[] addresses) {
        if (addresses == null) {
            return;
        }
        for (Address address : addresses) {
            addAddress(view, address);
        }
    }

    private void addAddress(MultiAutoCompleteTextView view, Address address) {
        view.append(address + ", ");
    }

    private Address[] getAddresses(MultiAutoCompleteTextView view) {

        return Address.parseUnencoded(view.getText().toString().trim());
    }

    private boolean initFromIntent(final Intent intent) {
        boolean startedByExternalIntent = false;
        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action) || Intent.ACTION_SENDTO.equals(action)) {
            /*
             * Someone has clicked a mailto: link. The address is in the URI.
             */
            if (intent.getData() != null) {
                Uri uri = intent.getData();
                if ("mailto".equals(uri.getScheme())) {
                    initializeFromMailto(uri);
                }
            }

            /*
             * Note: According to the documentation ACTION_VIEW and ACTION_SENDTO don't accept
             * EXTRA_* parameters.
             * And previously we didn't process these EXTRAs. But it looks like nobody bothers to
             * read the official documentation and just copies wrong sample code that happens to
             * work with the AOSP Email application. And because even big players get this wrong,
             * we're now finally giving in and read the EXTRAs for those actions (below).
             */
        }

        if (Intent.ACTION_SEND.equals(action) || Intent.ACTION_SEND_MULTIPLE.equals(action) ||
                Intent.ACTION_SENDTO.equals(action) || Intent.ACTION_VIEW.equals(action)) {
            startedByExternalIntent = true;

            /*
             * Note: Here we allow a slight deviation from the documented behavior.
             * EXTRA_TEXT is used as message body (if available) regardless of the MIME
             * type of the intent. In addition one or multiple attachments can be added
             * using EXTRA_STREAM.
             */
            CharSequence text = intent.getCharSequenceExtra(Intent.EXTRA_TEXT);
            // Only use EXTRA_TEXT if the body hasn't already been set by the mailto URI
            if (text != null && editMessage.getText().length() == 0) {
                editMessage.setCharacters(text);
            }

            String type = intent.getType();
            if (Intent.ACTION_SEND.equals(action)) {
                Uri stream = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
                if (stream != null) {
                    addAttachment(stream, type);
                }
            } else {
                List<Parcelable> list = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
                if (list != null) {
                    for (Parcelable parcelable : list) {
                        Uri stream = (Uri) parcelable;
                        if (stream != null) {
                            addAttachment(stream, type);
                        }
                    }
                }
            }

            String subject = intent.getStringExtra(Intent.EXTRA_SUBJECT);
            // Only use EXTRA_SUBJECT if the subject hasn't already been set by the mailto URI
            if (subject != null && editSubject.getText().length() == 0) {
                editSubject.setText(subject);
            }

            String[] extraEmail = intent.getStringArrayExtra(Intent.EXTRA_EMAIL);
            String[] extraCc = intent.getStringArrayExtra(Intent.EXTRA_CC);
            String[] extraBcc = intent.getStringArrayExtra(Intent.EXTRA_BCC);

            if (extraEmail != null) {
                addRecipients(editTo, Arrays.asList(extraEmail));
            }

            boolean ccOrBcc = false;
            if (extraCc != null) {
                ccOrBcc |= addRecipients(editCc, Arrays.asList(extraCc));
            }

            if (extraBcc != null) {
                ccOrBcc |= addRecipients(editBcc, Arrays.asList(extraBcc));
            }

            if (ccOrBcc) {
                // Display CC and BCC text fields if CC or BCC recipients were set by the intent.
               onAddCcBcc(true);
            }
        }

        return startedByExternalIntent;
    }

    private void initializeFromMailto(Uri mailtoUri) {
        String schemaSpecific = mailtoUri.getSchemeSpecificPart();
        int end = schemaSpecific.indexOf('?');
        if (end == -1) {
            end = schemaSpecific.length();
        }

        // Extract the recipient's email address from the mailto URI if there's one.
        String recipient = Uri.decode(schemaSpecific.substring(0, end));

        /*
         * mailto URIs are not hierarchical. So calling getQueryParameters()
         * will throw an UnsupportedOperationException. We avoid this by
         * creating a new hierarchical dummy Uri object with the query
         * parameters of the original URI.
         */
        CaseInsensitiveParamWrapper uri = new CaseInsensitiveParamWrapper(
                Uri.parse("foo://bar?" + mailtoUri.getEncodedQuery()));

        // Read additional recipients from the "to" parameter.
        List<String> to = uri.getQueryParameters("to");
        if (recipient.length() != 0) {
            to = new ArrayList<String>(to);
            to.add(0, recipient);
        }
        //addRecipients(mToView, to);

        // Read carbon copy recipients from the "cc" parameter.
        boolean ccOrBcc = addRecipients(editCc, uri.getQueryParameters("cc"));

        // Read blind carbon copy recipients from the "bcc" parameter.
        ccOrBcc |= addRecipients(editBcc, uri.getQueryParameters("bcc"));

        if (ccOrBcc) {
            // Display CC and BCC text fields if CC or BCC recipients were set by the intent.
            onAddCcBcc(true);
        }

        // Read subject from the "subject" parameter.
        List<String> subject = uri.getQueryParameters("subject");
        if (!subject.isEmpty()) {
            editSubject.setText(subject.get(0));
        }

        // Read message body from the "body" parameter.
        List<String> body = uri.getQueryParameters("body");
        if (!body.isEmpty()) {
            editMessage.setCharacters(body.get(0));
        }
    }

    private static class CaseInsensitiveParamWrapper {
        private final Uri uri;
        private Set<String> mParamNames;

        public CaseInsensitiveParamWrapper(Uri uri) {
            this.uri = uri;
        }

        public List<String> getQueryParameters(String key) {
            final List<String> params = new ArrayList<String>();
            for (String paramName : uri.getQueryParameterNames()) {
                if (paramName.equalsIgnoreCase(key)) {
                    params.addAll(uri.getQueryParameters(paramName));
                }
            }
            return params;
        }

    }

    private boolean addRecipients(TextView view, List<String> recipients) {
        if (recipients == null || recipients.isEmpty()) {
            return false;
        }

        StringBuilder addressList = new StringBuilder();

        // Read current contents of the TextView
        String text = view.getText().toString();
        addressList.append(text);

        // Add comma if necessary
        if (text.length() != 0 && !(text.endsWith(", ") || text.endsWith(","))) {
            addressList.append(", ");
        }

        // Add recipients
        for (String recipient : recipients) {
            addressList.append(recipient);
            addressList.append(", ");
        }

        view.setText(addressList);

        return true;
    }

    private void addAttachment(Uri uri) {
        addAttachment(uri, null);
    }

    private void addAttachment(Uri uri, String contentType) {
        Attachment attachment = new Attachment();
        attachment.state = Attachment.LoadingState.URI_ONLY;
        attachment.uri = uri;
        attachment.contentType = contentType;
        attachment.loaderId = ++mMaxLoaderId;

        addAttachmentView(attachment);

        initAttachmentInfoLoader(attachment);
    }

    private void initAttachmentInfoLoader(Attachment attachment) {
        LoaderManager loaderManager = getLoaderManager();
        Bundle bundle = new Bundle();
        bundle.putParcelable(LOADER_ARG_ATTACHMENT, attachment);
        loaderManager.initLoader(attachment.loaderId, bundle, mAttachmentInfoLoaderCallback);
    }

    private void initAttachmentContentLoader(Attachment attachment) {
        LoaderManager loaderManager = getLoaderManager();
        Bundle bundle = new Bundle();
        bundle.putParcelable(LOADER_ARG_ATTACHMENT, attachment);
        loaderManager.initLoader(attachment.loaderId, bundle, mAttachmentContentLoaderCallback);
    }

    private void addAttachmentView(Attachment attachment) {
        boolean hasMetadata = (attachment.state != Attachment.LoadingState.URI_ONLY);
        boolean isLoadingComplete = (attachment.state == Attachment.LoadingState.COMPLETE);

        View view = getLayoutInflater().inflate(R.layout.item_compose_attachment, layoutAttachments, false);
        TextView nameView = (TextView) view.findViewById(R.id.txt_attachment_name);
        View progressBar = view.findViewById(R.id.progressBar);

        if (hasMetadata) {
            nameView.setText(attachment.name);
        } else {
            nameView.setText(R.string.lbl_attachment);
        }

        progressBar.setVisibility(isLoadingComplete ? View.GONE : View.VISIBLE);

        ImageButton delete = (ImageButton) view.findViewById(R.id.btn_delete_attachment);
        delete.setOnClickListener(ComposeActivity.this);
        delete.setTag(view);

        view.setTag(attachment);
        layoutAttachments.addView(view);
    }

    private View getAttachmentView(int loaderId) {
        for (int i = 0, childCount = layoutAttachments.getChildCount(); i < childCount; i++) {
            View view = layoutAttachments.getChildAt(i);
            Attachment tag = (Attachment) view.getTag();
            if (tag != null && tag.loaderId == loaderId) {
                return view;
            }
        }

        return null;
    }

    private TextBody buildText(boolean isDraft) {
        return buildText(isDraft, mMessageFormat);
    }

    private TextBody buildText(boolean isDraft, SimpleMessageFormat messageFormat) {
        String messageText = editMessage.getCharacters();

        TextBodyBuilder textBodyBuilder = new TextBodyBuilder(messageText);

        /*
         * Find out if we need to include the original message as quoted text.
         *
         * We include the quoted text in the body if the user didn't choose to
         * hide it. We always include the quoted text when we're saving a draft.
         * That's so the user is able to "un-hide" the quoted text if (s)he
         * opens a saved draft.
         */
        boolean includeQuotedText = (isDraft || mQuotedTextMode == QuotedTextMode.SHOW);
        boolean isReplyAfterQuote = (mQuoteStyle == QuoteStyle.PREFIX && mAccount.isReplyAfterQuote());

        textBodyBuilder.setIncludeQuotedText(false);
        if (includeQuotedText) {
            if (messageFormat == SimpleMessageFormat.HTML && mQuotedHtmlContent != null) {
                textBodyBuilder.setIncludeQuotedText(true);
                textBodyBuilder.setQuotedTextHtml(mQuotedHtmlContent);
                textBodyBuilder.setReplyAfterQuote(isReplyAfterQuote);
            }

            String quotedText = mQuotedText.getCharacters();
            if (messageFormat == SimpleMessageFormat.TEXT && quotedText.length() > 0) {
                textBodyBuilder.setIncludeQuotedText(true);
                textBodyBuilder.setQuotedText(quotedText);
                textBodyBuilder.setReplyAfterQuote(isReplyAfterQuote);
            }
        }

        textBodyBuilder.setInsertSeparator(!isDraft);

        boolean useSignature = (!isDraft && mIdentity.getSignatureUse());
        if (useSignature) {
            textBodyBuilder.setAppendSignature(true);
            textBodyBuilder.setSignature(mSignatureView.getCharacters());
            textBodyBuilder.setSignatureBeforeQuotedText(mAccount.isSignatureBeforeQuotedText());
        } else {
            textBodyBuilder.setAppendSignature(false);
        }

        TextBody body;
        if (messageFormat == SimpleMessageFormat.HTML) {
            body = textBodyBuilder.buildTextHtml();
        } else {
            body = textBodyBuilder.buildTextPlain();
        }
        return body;
    }

    private MimeMessage createMessage(boolean isDraft) throws MessagingException {
        MimeMessage message = new MimeMessage();
        message.addSentDate(new Date());
        Address from = new Address(mIdentity.getEmail(), mIdentity.getName());
        message.setFrom(from);
        message.setRecipients(RecipientType.TO, getAddresses(editTo));
        message.setRecipients(RecipientType.CC, getAddresses(editCc));
        message.setRecipients(RecipientType.BCC, getAddresses(editBcc));
        message.setSubject(editSubject.getText().toString());
        if (mReadReceipt) {
            message.setHeader("Disposition-Notification-To", from.toEncodedString());
            message.setHeader("X-Confirm-Reading-To", from.toEncodedString());
            message.setHeader("Return-Receipt-To", from.toEncodedString());
        }


        if (!NYXMail.hideUserAgent()) {
            message.setHeader("User-Agent", getString(R.string.message_header_mua));
        }


        final String replyTo = mIdentity.getReplyTo();
        if (replyTo != null) {
            message.setReplyTo(new Address[] { new Address(replyTo) });
        }

        if (mInReplyTo != null) {
            message.setInReplyTo(mInReplyTo);
        }

        if (mReferences != null) {
            message.setReferences(mReferences);
        }

        // Build the body.
        // TODO FIXME - body can be either an HTML or Text part, depending on whether we're in
        // HTML mode or not.  Should probably fix this so we don't mix up html and text parts.
        TextBody body = null;
        if (mPgpData.getEncryptedData() != null) {
            String text = mPgpData.getEncryptedData();
            body = new TextBody(text);
        } else {
            body = buildText(isDraft);
        }

        // text/plain part when mMessageFormat == MessageFormat.HTML
        TextBody bodyPlain = null;

        final boolean hasAttachments = layoutAttachments.getChildCount() > 0;

        if (mMessageFormat == SimpleMessageFormat.HTML) {
            // HTML message (with alternative text part)

            // This is the compiled MIME part for an HTML message.
            MimeMultipart composedMimeMessage = new MimeMultipart();
            composedMimeMessage.setSubType("alternative");   // Let the receiver select either the text or the HTML part.
            composedMimeMessage.addBodyPart(new MimeBodyPart(body, "text/html"));
            bodyPlain = buildText(isDraft, SimpleMessageFormat.TEXT);
            composedMimeMessage.addBodyPart(new MimeBodyPart(bodyPlain, "text/plain"));

            if (hasAttachments) {
                // If we're HTML and have attachments, we have a MimeMultipart container to hold the
                // whole message (mp here), of which one part is a MimeMultipart container
                // (composedMimeMessage) with the user's composed messages, and subsequent parts for
                // the attachments.
                MimeMultipart mp = new MimeMultipart();
                mp.addBodyPart(new MimeBodyPart(composedMimeMessage));
                addAttachmentsToMessage(mp);
                message.setBody(mp);
            } else {
                // If no attachments, our multipart/alternative part is the only one we need.
                message.setBody(composedMimeMessage);
            }
        } else if (mMessageFormat == SimpleMessageFormat.TEXT) {
            // Text-only message.
            if (hasAttachments) {
                MimeMultipart mp = new MimeMultipart();
                mp.addBodyPart(new MimeBodyPart(body, "text/plain"));
                addAttachmentsToMessage(mp);
                message.setBody(mp);
            } else {
                // No attachments to include, just stick the text body in the message and call it good.
                message.setBody(body);
            }
        }

        // If this is a draft, add metadata for thawing.
        if (isDraft) {
            // Add the identity to the message.
            message.addHeader(NYXMail.IDENTITY_HEADER, buildIdentityHeader(body, bodyPlain));
        }

        return message;
    }

    private String buildIdentityHeader(final TextBody body, final TextBody bodyPlain) {
        Uri.Builder uri = new Uri.Builder();
        if (body.getComposedMessageLength() != null && body.getComposedMessageOffset() != null) {
            // See if the message body length is already in the TextBody.
            uri.appendQueryParameter(IdentityField.LENGTH.value(), body.getComposedMessageLength().toString());
            uri.appendQueryParameter(IdentityField.OFFSET.value(), body.getComposedMessageOffset().toString());
        } else {
            // If not, calculate it now.
            uri.appendQueryParameter(IdentityField.LENGTH.value(), Integer.toString(body.getText().length()));
            uri.appendQueryParameter(IdentityField.OFFSET.value(), Integer.toString(0));
        }
        if (mQuotedHtmlContent != null) {
            uri.appendQueryParameter(IdentityField.FOOTER_OFFSET.value(),
                    Integer.toString(mQuotedHtmlContent.getFooterInsertionPoint()));
        }
        if (bodyPlain != null) {
            if (bodyPlain.getComposedMessageLength() != null && bodyPlain.getComposedMessageOffset() != null) {
                // See if the message body length is already in the TextBody.
                uri.appendQueryParameter(IdentityField.PLAIN_LENGTH.value(), bodyPlain.getComposedMessageLength().toString());
                uri.appendQueryParameter(IdentityField.PLAIN_OFFSET.value(), bodyPlain.getComposedMessageOffset().toString());
            } else {
                // If not, calculate it now.
                uri.appendQueryParameter(IdentityField.PLAIN_LENGTH.value(), Integer.toString(body.getText().length()));
                uri.appendQueryParameter(IdentityField.PLAIN_OFFSET.value(), Integer.toString(0));
            }
        }
        // Save the quote style (useful for forwards).
        uri.appendQueryParameter(IdentityField.QUOTE_STYLE.value(), mQuoteStyle.name());

        // Save the message format for this offset.
        uri.appendQueryParameter(IdentityField.MESSAGE_FORMAT.value(), mMessageFormat.name());

        // If we're not using the standard identity of signature, append it on to the identity blob.
        if (mIdentity.getSignatureUse() && mSignatureChanged) {
            //uri.appendQueryParameter(IdentityField.SIGNATURE.value(), mSignatureView.getCharacters());
        }

        if (mIdentityChanged) {
            uri.appendQueryParameter(IdentityField.NAME.value(), mIdentity.getName());
            uri.appendQueryParameter(IdentityField.EMAIL.value(), mIdentity.getEmail());
        }

        if (mMessageReference != null) {
            uri.appendQueryParameter(IdentityField.ORIGINAL_MESSAGE.value(), mMessageReference.toIdentityString());
        }

        uri.appendQueryParameter(IdentityField.CURSOR_POSITION.value(), Integer.toString(editMessage.getSelectionStart()));

        uri.appendQueryParameter(IdentityField.QUOTED_TEXT_MODE.value(), mQuotedTextMode.name());

        String k9identity = IDENTITY_VERSION_1 + uri.build().getEncodedQuery();

        if (NYXMail.DEBUG) {
            Log.d(NYXMail.LOG_TAG, "Generated identity: " + k9identity);
        }

        return k9identity;
    }

    private void addAttachmentsToMessage(final MimeMultipart mp) throws MessagingException {
        Body body;
        for (int i = 0, count = layoutAttachments.getChildCount(); i < count; i++) {
            Attachment attachment = (Attachment) layoutAttachments.getChildAt(i).getTag();

            if (attachment.state != Attachment.LoadingState.COMPLETE) {
                continue;
            }

            String contentType = attachment.contentType;
            if (MimeUtil.isMessage(contentType)) {
                body = new TempFileMessageBody(attachment.filename);
            } else {
                body = new TempFileBody(attachment.filename);
            }
            MimeBodyPart bp = new MimeBodyPart(body);

            /*
             * Correctly encode the filename here. Otherwise the whole
             * header value (all parameters at once) will be encoded by
             * MimeHeader.writeTo().
             */
            bp.addHeader(MimeHeader.HEADER_CONTENT_TYPE, String.format("%s;\r\n name=\"%s\"",
                    contentType,
                    EncoderUtil.encodeIfNecessary(attachment.name,
                            EncoderUtil.Usage.WORD_ENTITY, 7)));

            bp.setEncoding(MimeUtility.getEncodingforType(contentType));

            /*
             * TODO: Oh the joys of MIME...
             *
             * From RFC 2183 (The Content-Disposition Header Field):
             * "Parameter values longer than 78 characters, or which
             *  contain non-ASCII characters, MUST be encoded as specified
             *  in [RFC 2184]."
             *
             * Example:
             *
             * Content-Type: application/x-stuff
             *  title*1*=us-ascii'en'This%20is%20even%20more%20
             *  title*2*=%2A%2A%2Afun%2A%2A%2A%20
             *  title*3="isn't it!"
             */
            bp.addHeader(MimeHeader.HEADER_CONTENT_DISPOSITION, String.format(Locale.US,
                    "attachment;\r\n filename=\"%s\";\r\n size=%d",
                    attachment.name, attachment.size));

            mp.addBodyPart(bp);
        }
    }

    private void processSourceMessageText(Message message, Integer bodyOffset, Integer bodyLength,
                                          boolean viewMessageContent) throws MessagingException {
        Part textPart = MimeUtility.findFirstPartByMimeType(message, "text/plain");
        if (textPart != null) {
            String text = MimeUtility.getTextFromPart(textPart);

            // If we had a body length (and it was valid), separate the composition from the quoted text
            // and put them in their respective places in the UI.
            if (bodyLength > 0) {
                try {
                    String bodyText = text.substring(bodyOffset, bodyOffset + bodyLength);

                    // Regenerate the quoted text without our user content in it nor added newlines.
                    StringBuilder quotedText = new StringBuilder();
                    if (bodyOffset == 0 && text.substring(bodyLength, bodyLength + 4).equals("\r\n\r\n")) {
                        // top-posting: ignore two newlines at start of quote
                        quotedText.append(text.substring(bodyLength + 4));
                    } else if (bodyOffset + bodyLength == text.length() &&
                            text.substring(bodyOffset - 2, bodyOffset).equals("\r\n")) {
                        // bottom-posting: ignore newline at end of quote
                        quotedText.append(text.substring(0, bodyOffset - 2));
                    } else {
                        quotedText.append(text.substring(0, bodyOffset));   // stuff before the reply
                        quotedText.append(text.substring(bodyOffset + bodyLength));
                    }

                    if (viewMessageContent) {
                        editMessage.setCharacters(bodyText);
                    }

                    mQuotedText.setCharacters(quotedText);
                } catch (IndexOutOfBoundsException e) {
                    // Invalid bodyOffset or bodyLength.  The draft was edited outside of K-9 Mail?
                    if (viewMessageContent) {
                        editMessage.setCharacters(text);
                    }
                }
            } else {
                if (viewMessageContent) {
                    editMessage.setCharacters(text);
                }
            }
        }
    }

    private String getBodyTextFromMessage(final Message message, final SimpleMessageFormat format)
            throws MessagingException {
        Part part;
        if (format == SimpleMessageFormat.HTML) {
            // HTML takes precedence, then text.
            part = MimeUtility.findFirstPartByMimeType(message, "text/html");
            if (part != null) {
                if (NYXMail.DEBUG) {
                    Log.d(NYXMail.LOG_TAG, "getBodyTextFromMessage: HTML requested, HTML found.");
                }
                return MimeUtility.getTextFromPart(part);
            }

            part = MimeUtility.findFirstPartByMimeType(message, "text/plain");
            if (part != null) {
                if (NYXMail.DEBUG) {
                    Log.d(NYXMail.LOG_TAG, "getBodyTextFromMessage: HTML requested, text found.");
                }
                return HtmlConverter.textToHtml(MimeUtility.getTextFromPart(part));
            }
        } else if (format == SimpleMessageFormat.TEXT) {
            // Text takes precedence, then html.
            part = MimeUtility.findFirstPartByMimeType(message, "text/plain");
            if (part != null) {
                if (NYXMail.DEBUG) {
                    Log.d(NYXMail.LOG_TAG, "getBodyTextFromMessage: Text requested, text found.");
                }
                return MimeUtility.getTextFromPart(part);
            }

            part = MimeUtility.findFirstPartByMimeType(message, "text/html");
            if (part != null) {
                if (NYXMail.DEBUG) {
                    Log.d(NYXMail.LOG_TAG, "getBodyTextFromMessage: Text requested, HTML found.");
                }
                return MimeUtility.getTextFromPart(part);
            }
        }

        // If we had nothing interesting, return an empty string.
        return "";
    }

    private String quoteOriginalTextMessage(final Message originalMessage, final String messageBody, final QuoteStyle quoteStyle) throws MessagingException {
        String body = messageBody == null ? "" : messageBody;
        String sentDate = getSentDateText(originalMessage);
        if (quoteStyle == QuoteStyle.PREFIX) {
            StringBuilder quotedText = new StringBuilder(body.length() + QUOTE_BUFFER_LENGTH);
            if (sentDate.length() != 0) {
                quotedText.append(String.format(
                        getString(R.string.message_compose_reply_header_fmt_with_date) + "\r\n",
                        sentDate,
                        Address.toString(originalMessage.getFrom())));
            } else {
                quotedText.append(String.format(
                                getString(R.string.message_compose_reply_header_fmt) + "\r\n",
                                Address.toString(originalMessage.getFrom()))
                );
            }

            final String prefix = mAccount.getQuotePrefix();
            final String wrappedText = Utility.wrap(body, REPLY_WRAP_LINE_WIDTH - prefix.length());

            // "$" and "\" in the quote prefix have to be escaped for
            // the replaceAll() invocation.
            final String escapedPrefix = prefix.replaceAll("(\\\\|\\$)", "\\\\$1");
            quotedText.append(wrappedText.replaceAll("(?m)^", escapedPrefix));

            return quotedText.toString().replaceAll("\\\r", "");
        } else if (quoteStyle == QuoteStyle.HEADER) {
            StringBuilder quotedText = new StringBuilder(body.length() + QUOTE_BUFFER_LENGTH);
            quotedText.append("\r\n");
            quotedText.append(getString(R.string.message_compose_quote_header_separator)).append("\r\n");
            if (originalMessage.getFrom() != null && Address.toString(originalMessage.getFrom()).length() != 0) {
                quotedText.append(getString(R.string.lbl_from)).append(" ").append(Address.toString(originalMessage.getFrom())).append("\r\n");
            }
            if (sentDate.length() != 0) {
                quotedText.append(getString(R.string.message_compose_quote_header_send_date)).append(" ").append(sentDate).append("\r\n");
            }
            if (originalMessage.getRecipients(RecipientType.TO) != null && originalMessage.getRecipients(RecipientType.TO).length != 0) {
                quotedText.append(getString(R.string.lbl_to)).append(" ").append(Address.toString(originalMessage.getRecipients(RecipientType.TO))).append("\r\n");
            }
            if (originalMessage.getRecipients(RecipientType.CC) != null && originalMessage.getRecipients(RecipientType.CC).length != 0) {
                quotedText.append(getString(R.string.lbl_cc)).append(" ").append(Address.toString(originalMessage.getRecipients(RecipientType.CC))).append("\r\n");
            }
            if (originalMessage.getSubject() != null) {
                quotedText.append(getString(R.string.lbl_subject)).append(" ").append(originalMessage.getSubject()).append("\r\n");
            }
            quotedText.append("\r\n");

            quotedText.append(body);

            return quotedText.toString();
        } else {
            // Shouldn't ever happen.
            return body;
        }
    }

    private void populateUIWithQuotedMessage(boolean showQuotedText) throws MessagingException {
        MessageFormat origMessageFormat = mAccount.getMessageFormat();
        if (mForcePlainText || origMessageFormat == MessageFormat.TEXT) {
            // Use plain text for the quoted message
            mQuotedTextFormat = SimpleMessageFormat.TEXT;
        } else if (origMessageFormat == MessageFormat.AUTO) {
            // Figure out which message format to use for the quoted text by looking if the source
            // message contains a text/html part. If it does, we use that.
            mQuotedTextFormat =
                    (MimeUtility.findFirstPartByMimeType(mSourceMessage, "text/html") == null) ?
                            SimpleMessageFormat.TEXT : SimpleMessageFormat.HTML;
        } else {
            mQuotedTextFormat = SimpleMessageFormat.HTML;
        }

        // TODO -- I am assuming that mSourceMessageBody will always be a text part.  Is this a safe assumption?

        // Handle the original message in the reply
        // If we already have mSourceMessageBody, use that.  It's pre-populated if we've got crypto going on.
        String content = (mSourceMessageBody != null) ?
                mSourceMessageBody :
                getBodyTextFromMessage(mSourceMessage, mQuotedTextFormat);

        if (mQuotedTextFormat == SimpleMessageFormat.HTML) {
            // Strip signature.
            // closing tags such as </div>, </span>, </table>, </pre> will be cut off.
            if (mAccount.isStripSignature() &&
                    (mAction == Action.REPLY || mAction == Action.REPLY_ALL)) {
                Matcher dashSignatureHtml = DASH_SIGNATURE_HTML.matcher(content);
                if (dashSignatureHtml.find()) {
                    Matcher blockquoteStart = BLOCKQUOTE_START.matcher(content);
                    Matcher blockquoteEnd = BLOCKQUOTE_END.matcher(content);
                    List<Integer> start = new ArrayList<Integer>();
                    List<Integer> end = new ArrayList<Integer>();

                    while (blockquoteStart.find()) {
                        start.add(blockquoteStart.start());
                    }
                    while (blockquoteEnd.find()) {
                        end.add(blockquoteEnd.start());
                    }
                    if (start.size() != end.size()) {
                        Log.d(NYXMail.LOG_TAG, "There are " + start.size() + " <blockquote> tags, but " +
                                end.size() + " </blockquote> tags. Refusing to strip.");
                    } else if (start.size() > 0) {
                        // Ignore quoted signatures in blockquotes.
                        dashSignatureHtml.region(0, start.get(0));
                        if (dashSignatureHtml.find()) {
                            // before first <blockquote>.
                            content = content.substring(0, dashSignatureHtml.start());
                        } else {
                            for (int i = 0; i < start.size() - 1; i++) {
                                // within blockquotes.
                                if (end.get(i) < start.get(i + 1)) {
                                    dashSignatureHtml.region(end.get(i), start.get(i + 1));
                                    if (dashSignatureHtml.find()) {
                                        content = content.substring(0, dashSignatureHtml.start());
                                        break;
                                    }
                                }
                            }
                            if (end.get(end.size() - 1) < content.length()) {
                                // after last </blockquote>.
                                dashSignatureHtml.region(end.get(end.size() - 1), content.length());
                                if (dashSignatureHtml.find()) {
                                    content = content.substring(0, dashSignatureHtml.start());
                                }
                            }
                        }
                    } else {
                        // No blockquotes found.
                        content = content.substring(0, dashSignatureHtml.start());
                    }
                }

                // Fix the stripping off of closing tags if a signature was stripped,
                // as well as clean up the HTML of the quoted message.
                HtmlCleaner cleaner = new HtmlCleaner();
                CleanerProperties properties = cleaner.getProperties();

                // see http://htmlcleaner.sourceforge.net/parameters.php for descriptions
                properties.setNamespacesAware(false);
                properties.setAdvancedXmlEscape(false);
                properties.setOmitXmlDeclaration(true);
                properties.setOmitDoctypeDeclaration(false);
                properties.setTranslateSpecialEntities(false);
                properties.setRecognizeUnicodeChars(false);

                TagNode node = cleaner.clean(content);
                SimpleHtmlSerializer htmlSerialized = new SimpleHtmlSerializer(properties);

                content = htmlSerialized.getAsString(node, "UTF8");
                /*try {
                    content = htmlSerialized.getAsString(node, "UTF8");
                } catch (java.io.IOException ioe) {
                    // Can't imagine this happening.
                    Log.e(NYXMail.LOG_TAG, "Problem cleaning quoted message.", ioe);
                }*/
            }

            // Add the HTML reply header to the top of the content.
            mQuotedHtmlContent = quoteOriginalHtmlMessage(mSourceMessage, content, mQuoteStyle);

            // Load the message with the reply header.
            quoteView.setText(mQuotedHtmlContent.getQuotedContent());

            // TODO: Also strip the signature from the text/plain part
            mQuotedText.setCharacters(quoteOriginalTextMessage(mSourceMessage,
                    getBodyTextFromMessage(mSourceMessage, SimpleMessageFormat.TEXT), mQuoteStyle));

        } else if (mQuotedTextFormat == SimpleMessageFormat.TEXT) {
            if (mAccount.isStripSignature() &&
                    (mAction == Action.REPLY || mAction == Action.REPLY_ALL)) {
                if (DASH_SIGNATURE_PLAIN.matcher(content).find()) {
                    content = DASH_SIGNATURE_PLAIN.matcher(content).replaceFirst("\r\n");
                }
            }

            mQuotedText.setCharacters(quoteOriginalTextMessage(mSourceMessage, content, mQuoteStyle));
        }

        if (showQuotedText) {
            showOrHideQuotedText(QuotedTextMode.SHOW);
        } else {
            showOrHideQuotedText(QuotedTextMode.HIDE);
        }
    }

    private void performStalledAction() {
        mNumAttachmentsLoading -= 1;

        WaitingAction waitingFor = mWaitingForAttachments;
        mWaitingForAttachments = WaitingAction.NONE;

        if (waitingFor != WaitingAction.NONE) {
            if(loading != null && loading.isShowing())
                loading.dismiss();
        }

        switch (waitingFor) {
            case SEND: {
                performSend();
                break;
            }
            case SAVE: {
                performSave();
                break;
            }
            case NONE:
                break;
        }
    }

    private void processSourceMessage(Message message) {
        try {
            switch (mAction) {
                case REPLY:
                case REPLY_ALL: {
                    processMessageToReplyTo(message);
                    break;
                }
                case FORWARD: {
                    processMessageToForward(message);
                    break;
                }
                case EDIT_DRAFT: {
                    processDraftMessage(message);
                    break;
                }
                default: {
                    Log.w(NYXMail.LOG_TAG, "processSourceMessage() called with unsupported action");
                    break;
                }
            }
        } catch (MessagingException me) {
            /**
             * Let the user continue composing their message even if we have a problem processing
             * the source message. Log it as an error, though.
             */
            Log.e(NYXMail.LOG_TAG, "Error while processing source message: ", me);
        } finally {
            mSourceMessageProcessed = true;
            mDraftNeedsSaving = false;
        }

        updateMessageFormat();
    }

    private void processMessageToReplyTo(Message message) throws MessagingException {
        if (message.getSubject() != null) {
            final String subject = PREFIX.matcher(message.getSubject()).replaceFirst("");

            if (!subject.toLowerCase(Locale.US).startsWith("re:")) {
                editSubject.setText("Re: " + subject);
            } else {
                editSubject.setText(subject);
            }
        } else {
            editSubject.setText("");
        }

        /*
         * If a reply-to was included with the message use that, otherwise use the from
         * or sender address.
         */
        Address[] replyToAddresses;
        if (message.getReplyTo().length > 0) {
            replyToAddresses = message.getReplyTo();
        } else {
            replyToAddresses = message.getFrom();
        }

        // if we're replying to a message we sent, we probably meant
        // to reply to the recipient of that message
        if (mAccount.isAnIdentity(replyToAddresses)) {
            replyToAddresses = message.getRecipients(RecipientType.TO);
        }

        addAddresses(editTo, replyToAddresses);

        if (message.getMessageId() != null && message.getMessageId().length() > 0) {
            mInReplyTo = message.getMessageId();

            String[] refs = message.getReferences();
            if (refs != null && refs.length > 0) {
                mReferences = TextUtils.join("", refs) + " " + mInReplyTo;
            } else {
                mReferences = mInReplyTo;
            }

        } else {
            if (NYXMail.DEBUG) {
                Log.d(NYXMail.LOG_TAG, "could not get Message-ID.");
            }
        }

        // Quote the message and setup the UI.
        populateUIWithQuotedMessage(mAccount.isDefaultQuotedTextShown());

        if (mAction == Action.REPLY || mAction == Action.REPLY_ALL) {
            Identity useIdentity = IdentityHelper.getRecipientIdentityFromMessage(mAccount, message);
            Identity defaultIdentity = mAccount.getIdentity(0);
            if (useIdentity != defaultIdentity) {
                switchToIdentity(useIdentity);
            }
        }

        if (mAction == Action.REPLY_ALL) {
            if (message.getReplyTo().length > 0) {
                for (Address address : message.getFrom()) {
                    if (!mAccount.isAnIdentity(address) && !Utility.arrayContains(replyToAddresses, address)) {
                        addAddress(editTo, address);
                    }
                }
            }
            for (Address address : message.getRecipients(RecipientType.TO)) {
                if (!mAccount.isAnIdentity(address) && !Utility.arrayContains(replyToAddresses, address)) {
                    addAddress(editTo, address);
                }

            }
            if (message.getRecipients(RecipientType.CC).length > 0) {
                for (Address address : message.getRecipients(RecipientType.CC)) {
                    if (!mAccount.isAnIdentity(address) && !Utility.arrayContains(replyToAddresses, address)) {
                        addAddress(editCc, address);
                    }

                }
                layoutCc.setVisibility(View.VISIBLE);
            }
        }
    }

    private void processMessageToForward(Message message) throws MessagingException {
        String subject = message.getSubject();
        if (subject != null && !subject.toLowerCase(Locale.US).startsWith("fwd:")) {
            editSubject.setText("Fwd: " + subject);
        } else {
            editSubject.setText(subject);
        }
        mQuoteStyle = QuoteStyle.HEADER;

        // "Be Like Thunderbird" - on forwarded messages, set the message ID
        // of the forwarded message in the references and the reply to.  TB
        // only includes ID of the message being forwarded in the reference,
        // even if there are multiple references.
        if (!TextUtils.isEmpty(message.getMessageId())) {
            mInReplyTo = message.getMessageId();
            mReferences = mInReplyTo;
        } else {
            if (NYXMail.DEBUG) {
                Log.d(NYXMail.LOG_TAG, "could not get Message-ID.");
            }
        }

        // Quote the message and setup the UI.
        populateUIWithQuotedMessage(true);

        if (!mSourceMessageProcessed) {
            if (!loadAttachments(message, 0)) {
                mHandler.sendEmptyMessage(MSG_SKIPPED_ATTACHMENTS);
            }
        }
    }

    private void processDraftMessage(Message message) throws MessagingException {
        String showQuotedTextMode = "NONE";

        mDraftId = MessagingController.getInstance(getApplication()).getId(message);
        editSubject.setText(message.getSubject());
        addAddresses(editTo, message.getRecipients(RecipientType.TO));
        if (message.getRecipients(RecipientType.CC).length > 0) {
            addAddresses(editCc, message.getRecipients(RecipientType.CC));
            layoutCc.setVisibility(View.VISIBLE);
        }

        Address[] bccRecipients = message.getRecipients(RecipientType.BCC);
        if (bccRecipients.length > 0) {
            addAddresses(editBcc, bccRecipients);
            String bccAddress = mAccount.getAlwaysBcc();
            if (bccRecipients.length == 1 && bccAddress != null && bccAddress.equals(bccRecipients[0].toString())) {
                // If the auto-bcc is the only entry in the BCC list, don't show the Bcc fields.
                layoutBcc.setVisibility(View.GONE);
            } else {
                layoutBcc.setVisibility(View.VISIBLE);
            }
        }

        // Read In-Reply-To header from draft
        final String[] inReplyTo = message.getHeader("In-Reply-To");
        if ((inReplyTo != null) && (inReplyTo.length >= 1)) {
            mInReplyTo = inReplyTo[0];
        }

        // Read References header from draft
        final String[] references = message.getHeader("References");
        if ((references != null) && (references.length >= 1)) {
            mReferences = references[0];
        }

        if (!mSourceMessageProcessed) {
            loadAttachments(message, 0);
        }

        // Decode the identity header when loading a draft.
        // See buildIdentityHeader(TextBody) for a detailed description of the composition of this blob.
        Map<IdentityField, String> k9identity = new HashMap<IdentityField, String>();
        if (message.getHeader(NYXMail.IDENTITY_HEADER) != null && message.getHeader(NYXMail.IDENTITY_HEADER).length > 0 && message.getHeader(NYXMail.IDENTITY_HEADER)[0] != null) {
            k9identity = parseIdentityHeader(message.getHeader(NYXMail.IDENTITY_HEADER)[0]);
        }

        Identity newIdentity = new Identity();
        if (k9identity.containsKey(IdentityField.SIGNATURE)) {
            newIdentity.setSignatureUse(true);
            newIdentity.setSignature(k9identity.get(IdentityField.SIGNATURE));
            mSignatureChanged = true;
        } else {
            newIdentity.setSignatureUse(message.getFolder().getAccount().getSignatureUse());
            newIdentity.setSignature(mIdentity.getSignature());
        }

        if (k9identity.containsKey(IdentityField.NAME)) {
            newIdentity.setName(k9identity.get(IdentityField.NAME));
            mIdentityChanged = true;
        } else {
            newIdentity.setName(mIdentity.getName());
        }

        if (k9identity.containsKey(IdentityField.EMAIL)) {
            newIdentity.setEmail(k9identity.get(IdentityField.EMAIL));
            mIdentityChanged = true;
        } else {
            newIdentity.setEmail(mIdentity.getEmail());
        }

        if (k9identity.containsKey(IdentityField.ORIGINAL_MESSAGE)) {
            mMessageReference = null;
            try {
                String originalMessage = k9identity.get(IdentityField.ORIGINAL_MESSAGE);
                MessageReference messageReference = new MessageReference(originalMessage);

                // Check if this is a valid account in our database
                Preferences prefs = Preferences.getPreferences(getApplicationContext());
                Account account = prefs.getAccount(messageReference.accountUuid);
                if (account != null) {
                    mMessageReference = messageReference;
                }
            } catch (MessagingException e) {
                Log.e(NYXMail.LOG_TAG, "Could not decode message reference in identity.", e);
            }
        }

        int cursorPosition = 0;
        if (k9identity.containsKey(IdentityField.CURSOR_POSITION)) {
            try {
                cursorPosition = Integer.parseInt(k9identity.get(IdentityField.CURSOR_POSITION));
            } catch (Exception e) {
                Log.e(NYXMail.LOG_TAG, "Could not parse cursor position for MessageCompose; continuing.", e);
            }
        }

        if (k9identity.containsKey(IdentityField.QUOTED_TEXT_MODE)) {
            showQuotedTextMode = k9identity.get(IdentityField.QUOTED_TEXT_MODE);
        }

        mIdentity = newIdentity;

        updateSignature();
        updateFrom();

        Integer bodyLength = k9identity.get(IdentityField.LENGTH) != null
                ? Integer.valueOf(k9identity.get(IdentityField.LENGTH))
                : 0;
        Integer bodyOffset = k9identity.get(IdentityField.OFFSET) != null
                ? Integer.valueOf(k9identity.get(IdentityField.OFFSET))
                : 0;
        Integer bodyFooterOffset = k9identity.get(IdentityField.FOOTER_OFFSET) != null
                ? Integer.valueOf(k9identity.get(IdentityField.FOOTER_OFFSET))
                : null;
        Integer bodyPlainLength = k9identity.get(IdentityField.PLAIN_LENGTH) != null
                ? Integer.valueOf(k9identity.get(IdentityField.PLAIN_LENGTH))
                : null;
        Integer bodyPlainOffset = k9identity.get(IdentityField.PLAIN_OFFSET) != null
                ? Integer.valueOf(k9identity.get(IdentityField.PLAIN_OFFSET))
                : null;
        mQuoteStyle = k9identity.get(IdentityField.QUOTE_STYLE) != null
                ? QuoteStyle.valueOf(k9identity.get(IdentityField.QUOTE_STYLE))
                : mAccount.getQuoteStyle();


        QuotedTextMode quotedMode;
        try {
            quotedMode = QuotedTextMode.valueOf(showQuotedTextMode);
        } catch (Exception e) {
            quotedMode = QuotedTextMode.NONE;
        }

        // Always respect the user's current composition format preference, even if the
        // draft was saved in a different format.
        // TODO - The current implementation doesn't allow a user in HTML mode to edit a draft that wasn't saved with K9mail.
        String messageFormatString = k9identity.get(IdentityField.MESSAGE_FORMAT);

        MessageFormat messageFormat = null;
        if (messageFormatString != null) {
            try {
                messageFormat = MessageFormat.valueOf(messageFormatString);
            } catch (Exception e) { /* do nothing */ }
        }

        if (messageFormat == null) {
            // This message probably wasn't created by us. The exception is legacy
            // drafts created before the advent of HTML composition. In those cases,
            // we'll display the whole message (including the quoted part) in the
            // composition window. If that's the case, try and convert it to text to
            // match the behavior in text mode.
            editMessage.setCharacters(getBodyTextFromMessage(message, SimpleMessageFormat.TEXT));
            mForcePlainText = true;

            showOrHideQuotedText(quotedMode);
            return;
        }


        if (messageFormat == MessageFormat.HTML) {
            Part part = MimeUtility.findFirstPartByMimeType(message, "text/html");
            if (part != null) { // Shouldn't happen if we were the one who saved it.
                mQuotedTextFormat = SimpleMessageFormat.HTML;
                String text = MimeUtility.getTextFromPart(part);
                if (NYXMail.DEBUG) {
                    Log.d(NYXMail.LOG_TAG, "Loading message with offset " + bodyOffset + ", length " + bodyLength + ". Text length is " + text.length() + ".");
                }

                if (bodyOffset + bodyLength > text.length()) {
                    // The draft was edited outside of K-9 Mail?
                    Log.d(NYXMail.LOG_TAG, "The identity field from the draft contains an invalid LENGTH/OFFSET");
                    bodyOffset = 0;
                    bodyLength = 0;
                }
                // Grab our reply text.
                String bodyText = text.substring(bodyOffset, bodyOffset + bodyLength);
                editMessage.setCharacters(HtmlConverter.htmlToText(bodyText));

                // Regenerate the quoted html without our user content in it.
                StringBuilder quotedHTML = new StringBuilder();
                quotedHTML.append(text.substring(0, bodyOffset));   // stuff before the reply
                quotedHTML.append(text.substring(bodyOffset + bodyLength));
                if (quotedHTML.length() > 0) {
                    mQuotedHtmlContent = new InsertableHtmlContent();
                    mQuotedHtmlContent.setQuotedContent(quotedHTML);
                    // We don't know if bodyOffset refers to the header or to the footer
                    mQuotedHtmlContent.setHeaderInsertionPoint(bodyOffset);
                    if (bodyFooterOffset != null) {
                        mQuotedHtmlContent.setFooterInsertionPoint(bodyFooterOffset);
                    } else {
                        mQuotedHtmlContent.setFooterInsertionPoint(bodyOffset);
                    }
                    quoteView.setText(mQuotedHtmlContent.getQuotedContent());
                }
            }
            if (bodyPlainOffset != null && bodyPlainLength != null) {
                processSourceMessageText(message, bodyPlainOffset, bodyPlainLength, false);
            }
        } else if (messageFormat == MessageFormat.TEXT) {
            mQuotedTextFormat = SimpleMessageFormat.TEXT;
            processSourceMessageText(message, bodyOffset, bodyLength, true);
        } else {
            Log.e(NYXMail.LOG_TAG, "Unhandled message format.");
        }

        // Set the cursor position if we have it.
        try {
            editMessage.setSelection(cursorPosition);
        } catch (Exception e) {
            Log.e(NYXMail.LOG_TAG, "Could not set cursor position in MessageCompose; ignoring.", e);
        }

        showOrHideQuotedText(quotedMode);
    }

    private LoaderManager.LoaderCallbacks<Attachment> mAttachmentInfoLoaderCallback =
            new LoaderManager.LoaderCallbacks<Attachment>() {

            @Override
            public Loader<Attachment> onCreateLoader(int id, Bundle args) {
                onFetchAttachmentStarted();
                Attachment attachment = args.getParcelable(LOADER_ARG_ATTACHMENT);
                return new AttachmentInfoLoader(ComposeActivity.this, attachment);
            }

            @Override
            public void onLoadFinished(Loader<Attachment> loader, Attachment attachment) {
                int loaderId = loader.getId();

                View view = getAttachmentView(loaderId);
                if (view != null) {
                    view.setTag(attachment);

                    TextView nameView = (TextView) view.findViewById(R.id.txt_attachment_name);
                    nameView.setText(attachment.name);

                    attachment.loaderId = ++mMaxLoaderId;
                    initAttachmentContentLoader(attachment);
                } else {
                    onFetchAttachmentFinished();
                }

                getLoaderManager().destroyLoader(loaderId);
            }

            @Override
            public void onLoaderReset(Loader<Attachment> loader) {
                onFetchAttachmentFinished();
            }
    };

    private LoaderManager.LoaderCallbacks<Attachment> mAttachmentContentLoaderCallback =
            new LoaderManager.LoaderCallbacks<Attachment>() {

            @Override
            public Loader<Attachment> onCreateLoader(int id, Bundle args) {
                Attachment attachment = args.getParcelable(LOADER_ARG_ATTACHMENT);
                return new AttachmentContentLoader(ComposeActivity.this, attachment);
            }

            @Override
            public void onLoadFinished(Loader<Attachment> loader, Attachment attachment) {
                int loaderId = loader.getId();

                View view = getAttachmentView(loaderId);
                if (view != null) {
                    if (attachment.state == Attachment.LoadingState.COMPLETE) {
                        view.setTag(attachment);

                        View progressBar = view.findViewById(R.id.progressBar);
                        progressBar.setVisibility(View.GONE);
                    } else {
                        layoutAttachments.removeView(view);
                    }
                }

                onFetchAttachmentFinished();

                getLoaderManager().destroyLoader(loaderId);
            }

            @Override
            public void onLoaderReset(Loader<Attachment> loader) {
                onFetchAttachmentFinished();
            }
        };

    private void onFetchAttachmentStarted() {
        mNumAttachmentsLoading += 1;
    }

    private void onFetchAttachmentFinished() {
        // We're not allowed to perform fragment transactions when called from onLoadFinished().
        // So we use the Handler to call performStalledAction().
        mHandler.sendEmptyMessage(MSG_PERFORM_STALLED_ACTION);
    }

    private class SaveMessageTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            /*
             * Create the message from all the data the user has entered.
             */
            MimeMessage message;
            try {
                message = createMessage(true);  // isDraft = true
            } catch (MessagingException me) {
                Log.e(NYXMail.LOG_TAG, "Failed to create new message for send or save.", me);
                throw new RuntimeException("Failed to create a new message for send or save.", me);
            }

            /*
             * Save a draft
             */
            if (mAction == Action.EDIT_DRAFT) {
                /*
                 * We're saving a previously saved draft, so update the new message's uid
                 * to the old message's uid.
                 */
                if (mMessageReference != null) {
                    message.setUid(mMessageReference.uid);
                }
            }

            final MessagingController messagingController = MessagingController.getInstance(getApplication());
            Message draftMessage = messagingController.saveDraft(mAccount, message, mDraftId);
            mDraftId = messagingController.getId(draftMessage);

            mHandler.sendEmptyMessage(MSG_SAVED_DRAFT);
            return null;
        }
    }

    private class SendMessageTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            /*
             * Create the message from all the data the user has entered.
             */
            MimeMessage message;
            try {
                message = createMessage(false);  // isDraft = true
            } catch (MessagingException me) {
                throw new RuntimeException("Failed to create a new message for send or save.", me);
            }

            try {
                mContacts.markAsContacted(message.getRecipients(RecipientType.TO));
                mContacts.markAsContacted(message.getRecipients(RecipientType.CC));
                mContacts.markAsContacted(message.getRecipients(RecipientType.BCC));
            } catch (Exception e) {
            }

            MessagingController.getInstance(getApplication()).sendMessage(mAccount, message, null);
            long draftId = mDraftId;
            if (draftId != INVALID_DRAFT_ID) {
                mDraftId = INVALID_DRAFT_ID;
                MessagingController.getInstance(getApplication()).deleteDraft(mAccount, draftId);
            }

            return null;
        }
    }

    private String getSentDateText(Message message) {
        try {
            final int dateStyle = DateFormat.LONG;
            final int timeStyle = DateFormat.LONG;
            Date date = message.getSentDate();
            Locale locale = getResources().getConfiguration().locale;
            return DateFormat.getDateTimeInstance(dateStyle, timeStyle, locale)
                    .format(date);
        } catch (Exception e) {
            return "";
        }
    }

    private InsertableHtmlContent findInsertionPoints(final String content) {
        InsertableHtmlContent insertable = new InsertableHtmlContent();

        // If there is no content, don't bother doing any of the regex dancing.
        if (content == null || content.equals("")) {
            return insertable;
        }

        // Search for opening tags.
        boolean hasHtmlTag = false;
        boolean hasHeadTag = false;
        boolean hasBodyTag = false;
        // First see if we have an opening HTML tag.  If we don't find one, we'll add one later.
        Matcher htmlMatcher = FIND_INSERTION_POINT_HTML.matcher(content);
        if (htmlMatcher.matches()) {
            hasHtmlTag = true;
        }
        // Look for a HEAD tag.  If we're missing a BODY tag, we'll use the close of the HEAD to start our content.
        Matcher headMatcher = FIND_INSERTION_POINT_HEAD.matcher(content);
        if (headMatcher.matches()) {
            hasHeadTag = true;
        }
        // Look for a BODY tag.  This is the ideal place for us to start our content.
        Matcher bodyMatcher = FIND_INSERTION_POINT_BODY.matcher(content);
        if (bodyMatcher.matches()) {
            hasBodyTag = true;
        }

        if (NYXMail.DEBUG) {
            Log.d(NYXMail.LOG_TAG, "Open: hasHtmlTag:" + hasHtmlTag + " hasHeadTag:" + hasHeadTag + " hasBodyTag:" + hasBodyTag);
        }

        // Given our inspections, let's figure out where to start our content.
        // This is the ideal case -- there's a BODY tag and we insert ourselves just after it.
        if (hasBodyTag) {
            insertable.setQuotedContent(new StringBuilder(content));
            insertable.setHeaderInsertionPoint(bodyMatcher.end(FIND_INSERTION_POINT_FIRST_GROUP));
        } else if (hasHeadTag) {
            // Now search for a HEAD tag.  We can insert after there.

            // If BlackBerry sees a HEAD tag, it inserts right after that, so long as there is no BODY tag. It doesn't
            // try to add BODY, either.  Right or wrong, it seems to work fine.
            insertable.setQuotedContent(new StringBuilder(content));
            insertable.setHeaderInsertionPoint(headMatcher.end(FIND_INSERTION_POINT_FIRST_GROUP));
        } else if (hasHtmlTag) {
            // Lastly, check for an HTML tag.
            // In this case, it will add a HEAD, but no BODY.
            StringBuilder newContent = new StringBuilder(content);
            // Insert the HEAD content just after the HTML tag.
            newContent.insert(htmlMatcher.end(FIND_INSERTION_POINT_FIRST_GROUP), FIND_INSERTION_POINT_HEAD_CONTENT);
            insertable.setQuotedContent(newContent);
            // The new insertion point is the end of the HTML tag, plus the length of the HEAD content.
            insertable.setHeaderInsertionPoint(htmlMatcher.end(FIND_INSERTION_POINT_FIRST_GROUP) + FIND_INSERTION_POINT_HEAD_CONTENT.length());
        } else {
            // If we have none of the above, we probably have a fragment of HTML.  Yahoo! and Gmail both do this.
            // Again, we add a HEAD, but not BODY.
            StringBuilder newContent = new StringBuilder(content);
            // Add the HTML and HEAD tags.
            newContent.insert(FIND_INSERTION_POINT_START_OF_STRING, FIND_INSERTION_POINT_HEAD_CONTENT);
            newContent.insert(FIND_INSERTION_POINT_START_OF_STRING, FIND_INSERTION_POINT_HTML_CONTENT);
            // Append the </HTML> tag.
            newContent.append(FIND_INSERTION_POINT_HTML_END_CONTENT);
            insertable.setQuotedContent(newContent);
            insertable.setHeaderInsertionPoint(FIND_INSERTION_POINT_HTML_CONTENT.length() + FIND_INSERTION_POINT_HEAD_CONTENT.length());
        }

        // Search for closing tags. We have to do this after we deal with opening tags since it may
        // have modified the message.
        boolean hasHtmlEndTag = false;
        boolean hasBodyEndTag = false;
        // First see if we have an opening HTML tag.  If we don't find one, we'll add one later.
        Matcher htmlEndMatcher = FIND_INSERTION_POINT_HTML_END.matcher(insertable.getQuotedContent());
        if (htmlEndMatcher.matches()) {
            hasHtmlEndTag = true;
        }
        // Look for a BODY tag.  This is the ideal place for us to place our footer.
        Matcher bodyEndMatcher = FIND_INSERTION_POINT_BODY_END.matcher(insertable.getQuotedContent());
        if (bodyEndMatcher.matches()) {
            hasBodyEndTag = true;
        }

        if (NYXMail.DEBUG) {
            Log.d(NYXMail.LOG_TAG, "Close: hasHtmlEndTag:" + hasHtmlEndTag + " hasBodyEndTag:" + hasBodyEndTag);
        }

        // Now figure out where to put our footer.
        // This is the ideal case -- there's a BODY tag and we insert ourselves just before it.
        if (hasBodyEndTag) {
            insertable.setFooterInsertionPoint(bodyEndMatcher.start(FIND_INSERTION_POINT_FIRST_GROUP));
        } else if (hasHtmlEndTag) {
            // Check for an HTML tag.  Add ourselves just before it.
            insertable.setFooterInsertionPoint(htmlEndMatcher.start(FIND_INSERTION_POINT_FIRST_GROUP));
        } else {
            // If we have none of the above, we probably have a fragment of HTML.
            // Set our footer insertion point as the end of the string.
            insertable.setFooterInsertionPoint(insertable.getQuotedContent().length());
        }

        return insertable;
    }

    private InsertableHtmlContent quoteOriginalHtmlMessage(final Message originalMessage, final String messageBody, final QuoteStyle quoteStyle) throws MessagingException {
        InsertableHtmlContent insertable = findInsertionPoints(messageBody);

        String sentDate = getSentDateText(originalMessage);
        if (quoteStyle == QuoteStyle.PREFIX) {
            StringBuilder header = new StringBuilder(QUOTE_BUFFER_LENGTH);
            header.append("<div class=\"gmail_quote\">");
            if (sentDate.length() != 0) {
                header.append(HtmlConverter.textToHtmlFragment(String.format(
                                getString(R.string.message_compose_reply_header_fmt_with_date),
                                sentDate,
                                Address.toString(originalMessage.getFrom()))
                ));
            } else {
                header.append(HtmlConverter.textToHtmlFragment(String.format(
                                getString(R.string.message_compose_reply_header_fmt),
                                Address.toString(originalMessage.getFrom()))
                ));
            }
            header.append("<blockquote class=\"gmail_quote\" " +
                    "style=\"margin: 0pt 0pt 0pt 0.8ex; border-left: 1px solid rgb(204, 204, 204); padding-left: 1ex;\">\r\n");

            String footer = "</blockquote></div>";

            insertable.insertIntoQuotedHeader(header.toString());
            insertable.insertIntoQuotedFooter(footer);
        } else if (quoteStyle == QuoteStyle.HEADER) {

            StringBuilder header = new StringBuilder();
            header.append("<div style='font-size:10.0pt;font-family:\"Tahoma\",\"sans-serif\";padding:3.0pt 0in 0in 0in'>\r\n");
            header.append("<hr style='border:none;border-top:solid #E1E1E1 1.0pt'>\r\n"); // This gets converted into a horizontal line during html to text conversion.
            if (originalMessage.getFrom() != null && Address.toString(originalMessage.getFrom()).length() != 0) {
                header.append("<b>").append(getString(R.string.lbl_from)).append("</b> ")
                        .append(HtmlConverter.textToHtmlFragment(Address.toString(originalMessage.getFrom())))
                        .append("<br>\r\n");
            }
            if (sentDate.length() != 0) {
                header.append("<b>").append(getString(R.string.message_compose_quote_header_send_date)).append("</b> ")
                        .append(sentDate)
                        .append("<br>\r\n");
            }
            if (originalMessage.getRecipients(RecipientType.TO) != null && originalMessage.getRecipients(RecipientType.TO).length != 0) {
                header.append("<b>").append(getString(R.string.lbl_to)).append("</b> ")
                        .append(HtmlConverter.textToHtmlFragment(Address.toString(originalMessage.getRecipients(RecipientType.TO))))
                        .append("<br>\r\n");
            }
            if (originalMessage.getRecipients(RecipientType.CC) != null && originalMessage.getRecipients(RecipientType.CC).length != 0) {
                header.append("<b>").append(getString(R.string.lbl_cc)).append("</b> ")
                        .append(HtmlConverter.textToHtmlFragment(Address.toString(originalMessage.getRecipients(RecipientType.CC))))
                        .append("<br>\r\n");
            }
            if (originalMessage.getSubject() != null) {
                header.append("<b>").append(getString(R.string.lbl_subject)).append("</b> ")
                        .append(HtmlConverter.textToHtmlFragment(originalMessage.getSubject()))
                        .append("<br>\r\n");
            }
            header.append("</div>\r\n");
            header.append("<br>\r\n");

            insertable.insertIntoQuotedHeader(header.toString());
        }

        return insertable;
    }

    private boolean includeQuotedText() {
        return (mQuotedTextMode == QuotedTextMode.SHOW);
    }

    private void setMessageFormat(SimpleMessageFormat format) {
        // This method will later be used to enable/disable the rich text editing mode.

        mMessageFormat = format;
    }

    private void updateMessageFormat() {
        MessageFormat origMessageFormat = mAccount.getMessageFormat();
        SimpleMessageFormat messageFormat;
        if (origMessageFormat == MessageFormat.TEXT) {
            // The user wants to send text/plain messages. We don't override that choice under
            // any circumstances.
            messageFormat = SimpleMessageFormat.TEXT;
        } else if (mForcePlainText && includeQuotedText()) {
            // Right now we send a text/plain-only message when the quoted text was edited, no
            // matter what the user selected for the message format.
            messageFormat = SimpleMessageFormat.TEXT;
        }
        /*else if (mEncryptCheckbox.isChecked() || mCryptoSignatureCheckbox.isChecked()) {
            // Right now we only support PGP inline which doesn't play well with HTML. So force
            // plain text in those cases.
            messageFormat = SimpleMessageFormat.TEXT;
        }
        */else if (origMessageFormat == MessageFormat.AUTO) {
            if (mAction == Action.COMPOSE || mQuotedTextFormat == SimpleMessageFormat.TEXT ||
                    !includeQuotedText()) {
                // If the message format is set to "AUTO" we use text/plain whenever possible. That
                // is, when composing new messages and replying to or forwarding text/plain
                // messages.
                messageFormat = SimpleMessageFormat.TEXT;
            } else {
                messageFormat = SimpleMessageFormat.HTML;
            }
        } else {
            // In all other cases use HTML
            messageFormat = SimpleMessageFormat.HTML;
        }

        setMessageFormat(messageFormat);
    }

    class Listener extends MessagingListener {
        @Override
        public void loadMessageForViewStarted(Account account, String folder, String uid) {
            if ((mMessageReference == null) || !mMessageReference.uid.equals(uid)) {
                return;
            }

            mHandler.sendEmptyMessage(MSG_PROGRESS_ON);
        }

        @Override
        public void loadMessageForViewFinished(Account account, String folder, String uid, Message message) {
            if ((mMessageReference == null) || !mMessageReference.uid.equals(uid)) {
                return;
            }

            mHandler.sendEmptyMessage(MSG_PROGRESS_OFF);
        }

        @Override
        public void loadMessageForViewBodyAvailable(Account account, String folder, String uid, final Message message) {
            if ((mMessageReference == null) || !mMessageReference.uid.equals(uid)) {
                return;
            }

            mSourceMessage = message;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // We check to see if we've previously processed the source message since this
                    // could be called when switching from HTML to text replies. If that happens, we
                    // only want to update the UI with quoted text (which picks the appropriate
                    // part).
                    if (mSourceProcessed) {
                        try {
                            populateUIWithQuotedMessage(true);
                        } catch (MessagingException e) {
                            // Hm, if we couldn't populate the UI after source reprocessing, let's just delete it?
                            showOrHideQuotedText(QuotedTextMode.HIDE);
                        }
                        updateMessageFormat();
                    } else {
                        processSourceMessage(message);
                        mSourceProcessed = true;
                    }
                }
            });
        }

        @Override
        public void loadMessageForViewFailed(Account account, String folder, String uid, Throwable t) {
            if ((mMessageReference == null) || !mMessageReference.uid.equals(uid)) {
                return;
            }
            mHandler.sendEmptyMessage(MSG_PROGRESS_OFF);
            // TODO show network error
        }

        @Override
        public void messageUidChanged(Account account, String folder, String oldUid, String newUid) {
            // Track UID changes of the source message
            if (mMessageReference != null) {
                final Account sourceAccount = Preferences.getPreferences(ComposeActivity.this).getAccount(mMessageReference.accountUuid);
                final String sourceFolder = mMessageReference.folderName;
                final String sourceMessageUid = mMessageReference.uid;

                if (account.equals(sourceAccount) && (folder.equals(sourceFolder))) {
                    if (oldUid.equals(sourceMessageUid)) {
                        mMessageReference.uid = newUid;
                    }
                    if ((mSourceMessage != null) && (oldUid.equals(mSourceMessage.getUid()))) {
                        mSourceMessage.setUid(newUid);
                    }
                }
            }
        }
    }

    private boolean canAccessStorage() {
        return(hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

}
