package com.nyx.mailapptest;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyx.mailapptest.custom.view.RoundedImageView;

import java.io.IOException;

/**
 *
 * Created by nyx on 1/25/15.
 *
 */
public class AccountTypeFragment extends Fragment implements View.OnClickListener {

    private OnAccountListener mCallBack;

    private RoundedImageView imgUser;
    private TextView btnGoogle,btnYahoo,btnHotmail,btnOther;

    public interface OnAccountListener{
        void onAccountSelected(String type, int icon);
    }

    public static AccountTypeFragment newInstance(boolean fromActivity){
        AccountTypeFragment frag = new AccountTypeFragment();

        Bundle args = new Bundle();
        args.putBoolean("BACK", fromActivity);
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_account_type, container, false);

        Bundle args = getArguments();

        Toolbar toolbar = (Toolbar)v.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
        toolbar.setTitle("");
        activity.setSupportActionBar(toolbar);
        boolean enabled = args.getBoolean("BACK");
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(enabled);
        activity.getSupportActionBar().setHomeButtonEnabled(enabled);


        imgUser = (RoundedImageView)v.findViewById(R.id.img_user);
        btnGoogle = (TextView)v.findViewById(R.id.btnG);
        btnYahoo = (TextView)v.findViewById(R.id.btnY);
        btnHotmail = (TextView)v.findViewById(R.id.btnH);
        btnOther = (TextView)v.findViewById(R.id.btnO);

        btnGoogle.setOnClickListener(this);
        btnYahoo.setOnClickListener(this);
        btnHotmail.setOnClickListener(this);
        btnOther.setOnClickListener(this);

        if(getUserProfilePhoto(getActivity()) != null)
            imgUser.setImageBitmap(getUserProfilePhoto(getActivity()));
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            mCallBack = (OnAccountListener) activity;
        }catch(ClassCastException e){
            throw new ClassCastException(activity.toString()+ "Excepcion");
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mCallBack = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v){
        if(v == btnGoogle)
            mCallBack.onAccountSelected("Gmail", R.drawable.gmail_icon);
        else if(v == btnYahoo)
            mCallBack.onAccountSelected("Yahoo!", R.drawable.yahoo_icon);
        else if(v == btnHotmail)
            mCallBack.onAccountSelected("Outlook", R.drawable.outlook_icon);
        else if(v == btnOther)
            mCallBack.onAccountSelected("Otro", R.drawable.generic_icon);
    }

    private static Bitmap getUserProfilePhoto(Context context) {
        Uri imageUri = null;
        final ContentResolver content = context.getContentResolver();

        try {
            final Cursor cursor = content.query(
                    // Retrieves data rows for the device user's 'profile' contact
                    Uri.withAppendedPath(
                            ContactsContract.Profile.CONTENT_URI,
                            ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
                    new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO_URI,
                            ContactsContract.Contacts.Data.MIMETYPE},

                    // Selects only email addresses or names
                    ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                            + ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                            + ContactsContract.Contacts.Data.MIMETYPE + "=? OR "
                            + ContactsContract.Contacts.Data.MIMETYPE + "=?",
                    new String[]{
                            ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                    },

                    // Show primary rows first. Note that there won't be a primary email address if the
                    // user hasn't specified one.
                    ContactsContract.Contacts.Data.IS_PRIMARY + " DESC"
            );

            String mime_type;
            while (cursor.moveToNext()) {
                mime_type = cursor.getString(1);
                if (mime_type.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE))
                    imageUri = Uri.parse(cursor.getString(0));
            }

            cursor.close();

            try {
                return MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
            } catch (IOException e) {
                return null;
            }
        }catch(Exception e){
            return null;
        }
    }
}
