package com.nyx.mailapptest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nyx.mailapptest.controller.MessagingController;
import com.nyx.mailapptest.data.App;
import com.nyx.mailapptest.data.NYXMail;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;

/**
 *
 * Created by nyx on 2/26/15.
 *
 */
public class PasswordActivity extends AppCompatActivity implements TextWatcher{

    private TextView txtTitle, txtTries;
    private EditText editPass1;

    private Preferences pref;

    private int intentos = 3;
    //0 - Primera vez
    //1 - Validar
    //2 - Cambiar
    private int type;
    private String auxPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        pref = Preferences.getPreferences(this);

        txtTitle = (TextView)findViewById(R.id.txtPassword);
        txtTries = (TextView)findViewById(R.id.txtTries);
        editPass1 = (EditText)findViewById(R.id.editPass);

        editPass1.addTextChangedListener(this);

        Bundle extras = getIntent().getExtras();

        type = extras.getInt("type");
        switch (type){
            case 0:
                txtTitle.setText(R.string.lbl_password);
                break;
            case 1:
                txtTitle.setText(R.string.lbl_password);
                break;
            case 2:
                txtTitle.setText(R.string.lbl_pass_ant);
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int before, int after) {
                /* do nothing */
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(android.text.Editable s) {
        if(s.length() == 4) {
            checkPassword(s.toString());
        }
    }

    @Override
    public void onBackPressed(){
        if(type == 1){
            finish();
            App.close();
        }else
            super.onBackPressed();
    }

    private void checkPassword(String pass){
        switch (type){
            case 0:
                if(auxPass == null) {
                    auxPass = pass;
                    txtTitle.setText(R.string.lbl_repeat_pass);
                    editPass1.setText("");
                }else{
                    if(auxPass.equals(pass)){
                        acceptPassword(pass);
                    }else
                        Toast.makeText(this,R.string.lbl_wrong_pass,Toast.LENGTH_SHORT).show();
                }
                break;
            case 1:
                if(validatePass(pass)){
                    acceptPassword(pass);
                }else
                    failedPass();
                break;
            case 2:
                if(validatePass(pass)){
                    txtTitle.setText(R.string.lbl_new_pass);
                    editPass1.setText("");
                    type = 0;
                }else
                    failedPass();
                break;
        }

    }

    private boolean validatePass(String pass){
        String myPass = pref.getPassword();
        return myPass.equals(pass);
    }

    private void failedPass(){
        editPass1.setText("");
        Toast.makeText(this, R.string.lbl_wrong_pass, Toast.LENGTH_SHORT).show();
        if(type == 1) {
            intentos--;
            if(intentos != 0) {
                txtTries.setText("Te quedan " + intentos + " intentos");
            }else
                borrarCuentas();
        }
    }

    private void acceptPassword(String pass){
        switch (type){
            case 0:
                pref.setPassword(pass);
                pref.setHasPassword(true);
                if(pref.getAccounts().length == 0)
                    toNewAccount();
                else
                    finish();
                break;
            default:
                finish();
                break;
        }
    }

    private void toNewAccount(){
        Intent i = new Intent(PasswordActivity.this,NewAccountActivity.class);
        startActivity(i);
        finish();
    }

    private void borrarCuentas(){
        Account accs[] = pref.getAccounts();
        for(Account ac : accs){
            try {
                ac.getLocalStore().delete();
            } catch (Exception e) {
                // Ignore, this may lead to localStores on sd-cards that
                // are currently not inserted to be left
            }
            MessagingController.getInstance(NYXMail.app)
                    .notifyAccountCancel(this, ac);
            pref.deleteAccount(ac);
            NYXMail.setServicesEnabled(this);
        }
        Toast.makeText(this,"Las cuentas de correo han sido borradas",Toast.LENGTH_LONG).show();
        pref.setHasPassword(false);
        pref.setPassword("");
        finish();
        App.close();
    }

}
