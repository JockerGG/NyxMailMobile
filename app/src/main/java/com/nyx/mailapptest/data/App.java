package com.nyx.mailapptest.data;

import com.nyx.mailapptest.ComposeActivity;
import com.nyx.mailapptest.MailActivity;
import com.nyx.mailapptest.MainActivity;
import com.nyx.mailapptest.NewAccountActivity;
import com.nyx.mailapptest.SearchActivity;
import com.nyx.mailapptest.SettingsActivity;

/**
 *
 * Created by nyx on 2/26/15.
 *
 */
public class App
{
    ////////////////////////////////////////////////////////////////
    // INSTANTIATED ACTIVITY VARIABLES
    ////////////////////////////////////////////////////////////////

    public static MainActivity activity1;
    public static MailActivity activity2;
    public static ComposeActivity activity3;
    public static SearchActivity activity4;
    public static NewAccountActivity activity5;
    public static SettingsActivity activity6;

    ////////////////////////////////////////////////////////////////
    // CLOSE APP METHOD
    ////////////////////////////////////////////////////////////////

    public static void close()
    {
        if (App.activity6 != null) {App.activity6.finish();}
        if (App.activity5 != null) {App.activity5.finish();}
        if (App.activity4 != null) {App.activity4.finish();}
        if (App.activity3 != null) {App.activity3.finish();}
        if (App.activity2 != null) {App.activity2.finish();}
        if (App.activity1 != null) {App.activity1.finish();}
    }
}
