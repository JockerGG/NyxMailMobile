package com.nyx.mailapptest.lib;

import com.nyx.mailapptest.lib.store.UnavailableStorageException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Body {
    public InputStream getInputStream() throws MessagingException;
    public void setEncoding(String encoding) throws UnavailableStorageException, MessagingException;
    public void writeTo(OutputStream out) throws IOException, MessagingException;
}
