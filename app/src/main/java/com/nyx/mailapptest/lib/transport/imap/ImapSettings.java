package com.nyx.mailapptest.lib.transport.imap;

import com.nyx.mailapptest.lib.AuthType;
import com.nyx.mailapptest.lib.ConnectionSecurity;
import com.nyx.mailapptest.lib.store.ImapStore;
import com.nyx.mailapptest.lib.store.ImapStore.ImapConnection;

/**
 * Settings source for IMAP. Implemented in order to remove coupling between {@link ImapStore} and {@link ImapConnection}.
 */
public interface ImapSettings {
    String getHost();

    int getPort();

    ConnectionSecurity getConnectionSecurity();

    AuthType getAuthType();

    String getUsername();

    String getPassword();

    String getClientCertificateAlias();

    boolean useCompression(int type);

    String getPathPrefix();

    void setPathPrefix(String prefix);

    String getPathDelimeter();

    void setPathDelimeter(String delimeter);

    String getCombinedPrefix();

    void setCombinedPrefix(String prefix);
}
