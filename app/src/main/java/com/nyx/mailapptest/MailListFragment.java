package com.nyx.mailapptest;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.view.ActionMode;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nyx.mailapptest.activity.FolderInfoHolder;
import com.nyx.mailapptest.activity.MessageReference;
import com.nyx.mailapptest.adapters.DividerItemDecoration;
import com.nyx.mailapptest.cache.EmailProviderCache;
import com.nyx.mailapptest.controller.MessagingController;
import com.nyx.mailapptest.custom.CursorRecyclerViewAdapter;
import com.nyx.mailapptest.custom.multiselector.ModalMultiSelectorCallback;
import com.nyx.mailapptest.custom.multiselector.MultiSelector;
import com.nyx.mailapptest.custom.multiselector.SwappingHolder;
import com.nyx.mailapptest.data.ActivityListener;
import com.nyx.mailapptest.data.NYXMail;
import com.nyx.mailapptest.helper.MergeCursorWithUniqueId;
import com.nyx.mailapptest.helper.MessageHelper;
import com.nyx.mailapptest.helper.StringUtils;
import com.nyx.mailapptest.lib.Address;
import com.nyx.mailapptest.lib.Flag;
import com.nyx.mailapptest.lib.Folder;
import com.nyx.mailapptest.lib.Message;
import com.nyx.mailapptest.lib.MessagingException;
import com.nyx.mailapptest.lib.store.LocalStore;
import com.nyx.mailapptest.lib.store.LocalStore.LocalFolder;
import com.nyx.mailapptest.provider.EmailProvider;
import com.nyx.mailapptest.search.ConditionsTreeNode;
import com.nyx.mailapptest.search.LocalSearch;
import com.nyx.mailapptest.search.SearchSpecification;
import com.nyx.mailapptest.search.SearchSpecification.Attribute;
import com.nyx.mailapptest.search.SearchSpecification.SearchCondition;
import com.nyx.mailapptest.search.SearchSpecification.Searchfield;
import com.nyx.mailapptest.search.SqlQueryBuilder;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Account.SortType;
import com.nyx.mailapptest.provider.EmailProvider.MessageColumns;
import com.nyx.mailapptest.provider.EmailProvider.ThreadColumns;
import com.nyx.mailapptest.provider.EmailProvider.SpecialColumns;
import com.nyx.mailapptest.utils.Preferences;
import com.nyx.mailapptest.utils.Utility;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * Created by nyx on 1/25/15.
 *
 */
public class MailListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String[] THREADED_PROJECTION = {
            MessageColumns.ID,
            MessageColumns.UID,
            MessageColumns.INTERNAL_DATE,
            MessageColumns.SUBJECT,
            MessageColumns.DATE,
            MessageColumns.SENDER_LIST,
            MessageColumns.TO_LIST,
            MessageColumns.CC_LIST,
            MessageColumns.READ,
            MessageColumns.FLAGGED,
            MessageColumns.ANSWERED,
            MessageColumns.FORWARDED,
            MessageColumns.ATTACHMENT_COUNT,
            MessageColumns.FOLDER_ID,
            MessageColumns.PREVIEW,
            ThreadColumns.ROOT,
            SpecialColumns.ACCOUNT_UUID,
            SpecialColumns.FOLDER_NAME,

            SpecialColumns.THREAD_COUNT,
    };

    private static final int ID_COLUMN = 0;
    private static final int UID_COLUMN = 1;
    private static final int INTERNAL_DATE_COLUMN = 2;
    private static final int SUBJECT_COLUMN = 3;
    private static final int DATE_COLUMN = 4;
    private static final int SENDER_LIST_COLUMN = 5;
    private static final int TO_LIST_COLUMN = 6;
    private static final int CC_LIST_COLUMN = 7;
    private static final int READ_COLUMN = 8;
    private static final int FLAGGED_COLUMN = 9;
    private static final int ANSWERED_COLUMN = 10;
    private static final int FORWARDED_COLUMN = 11;
    private static final int ATTACHMENT_COUNT_COLUMN = 12;
    private static final int FOLDER_ID_COLUMN = 13;
    private static final int PREVIEW_COLUMN = 14;
    private static final int THREAD_ROOT_COLUMN = 15;
    private static final int ACCOUNT_UUID_COLUMN = 16;
    private static final int FOLDER_NAME_COLUMN = 17;
    private static final int THREAD_COUNT_COLUMN = 18;

    private static final String[] PROJECTION = Arrays.copyOf(THREADED_PROJECTION,
            THREAD_COUNT_COLUMN);

    private static final String ARG_SEARCH = "searchObject";
    private static final String ARG_THREADED_LIST = "threadedList";
    private static final String ARG_IS_THREAD_DISPLAY = "isThreadedDisplay";
    private static final String ARG_IS_DRAFT = "isDraft";

    private static final String STATE_SELECTED_MESSAGES = "selectedMessages";
    private static final String STATE_ACTIVE_MESSAGE = "activeMessage";
    private static final String STATE_REMOTE_SEARCH_PERFORMED = "remoteSearchPerformed";
    private static final String STATE_MESSAGE_LIST = "listState";

    public static final String EXTRA_SEARCH_ACCOUNT = "com.nyx.mailapp.search_account";


    public static class ReverseComparator<T> implements Comparator<T> {
        private Comparator<T> mDelegate;

        /**
         * @param delegate
         *         Never {@code null}.
         */
        public ReverseComparator(final Comparator<T> delegate) {
            mDelegate = delegate;
        }

        @Override
        public int compare(final T object1, final T object2) {
            // arg1 & 2 are mixed up, this is done on purpose
            return mDelegate.compare(object2, object1);
        }
    }

    /**
     * Chains comparator to find a non-0 result.
     *
     * @param <T>
     */
    public static class ComparatorChain<T> implements Comparator<T> {
        private List<Comparator<T>> mChain;

        /**
         * @param chain
         *         Comparator chain. Never {@code null}.
         */
        public ComparatorChain(final List<Comparator<T>> chain) {
            mChain = chain;
        }

        @Override
        public int compare(T object1, T object2) {
            int result = 0;
            for (final Comparator<T> comparator : mChain) {
                result = comparator.compare(object1, object2);
                if (result != 0) {
                    break;
                }
            }
            return result;
        }
    }

    public static class ReverseIdComparator implements Comparator<Cursor> {
        private int mIdColumn = -1;

        @Override
        public int compare(Cursor cursor1, Cursor cursor2) {
            if (mIdColumn == -1) {
                mIdColumn = cursor1.getColumnIndex("_id");
            }
            long o1Id = cursor1.getLong(mIdColumn);
            long o2Id = cursor2.getLong(mIdColumn);
            return (o1Id > o2Id) ? -1 : 1;
        }
    }

    public static class AttachmentComparator implements Comparator<Cursor> {

        @Override
        public int compare(Cursor cursor1, Cursor cursor2) {
            int o1HasAttachment = (cursor1.getInt(ATTACHMENT_COUNT_COLUMN) > 0) ? 0 : 1;
            int o2HasAttachment = (cursor2.getInt(ATTACHMENT_COUNT_COLUMN) > 0) ? 0 : 1;
            return o1HasAttachment - o2HasAttachment;
        }
    }

    public static class FlaggedComparator implements Comparator<Cursor> {

        @Override
        public int compare(Cursor cursor1, Cursor cursor2) {
            int o1IsFlagged = (cursor1.getInt(FLAGGED_COLUMN) == 1) ? 0 : 1;
            int o2IsFlagged = (cursor2.getInt(FLAGGED_COLUMN) == 1) ? 0 : 1;
            return o1IsFlagged - o2IsFlagged;
        }
    }

    public static class UnreadComparator implements Comparator<Cursor> {

        @Override
        public int compare(Cursor cursor1, Cursor cursor2) {
            int o1IsUnread = cursor1.getInt(READ_COLUMN);
            int o2IsUnread = cursor2.getInt(READ_COLUMN);
            return o1IsUnread - o2IsUnread;
        }
    }

    public static class DateComparator implements Comparator<Cursor> {

        @Override
        public int compare(Cursor cursor1, Cursor cursor2) {
            long o1Date = cursor1.getLong(DATE_COLUMN);
            long o2Date = cursor2.getLong(DATE_COLUMN);
            if (o1Date < o2Date) {
                return -1;
            } else if (o1Date == o2Date) {
                return 0;
            } else {
                return 1;
            }
        }
    }

    public static class ArrivalComparator implements Comparator<Cursor> {

        @Override
        public int compare(Cursor cursor1, Cursor cursor2) {
            long o1Date = cursor1.getLong(INTERNAL_DATE_COLUMN);
            long o2Date = cursor2.getLong(INTERNAL_DATE_COLUMN);
            if (o1Date == o2Date) {
                return 0;
            } else if (o1Date < o2Date) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    public static class SubjectComparator implements Comparator<Cursor> {

        @Override
        public int compare(Cursor cursor1, Cursor cursor2) {
            String subject1 = cursor1.getString(SUBJECT_COLUMN);
            String subject2 = cursor2.getString(SUBJECT_COLUMN);

            if (subject1 == null) {
                return (subject2 == null) ? 0 : -1;
            } else if (subject2 == null) {
                return 1;
            }

            return subject1.compareToIgnoreCase(subject2);
        }
    }

    public static class SenderComparator implements Comparator<Cursor> {

        @Override
        public int compare(Cursor cursor1, Cursor cursor2) {
            String sender1 = getSenderAddressFromCursor(cursor1);
            String sender2 = getSenderAddressFromCursor(cursor2);

            if (sender1 == null && sender2 == null) {
                return 0;
            } else if (sender1 == null) {
                return 1;
            } else if (sender2 == null) {
                return -1;
            } else {
                return sender1.compareToIgnoreCase(sender2);
            }
        }
    }

    private static final Map<SortType, Comparator<Cursor>> SORT_COMPARATORS;

    static {
        // fill the mapping at class time loading

        final Map<SortType, Comparator<Cursor>> map =
                new EnumMap<SortType, Comparator<Cursor>>(SortType.class);
        map.put(SortType.SORT_ATTACHMENT, new AttachmentComparator());
        map.put(SortType.SORT_DATE, new DateComparator());
        map.put(SortType.SORT_ARRIVAL, new ArrivalComparator());
        map.put(SortType.SORT_FLAGGED, new FlaggedComparator());
        map.put(SortType.SORT_SUBJECT, new SubjectComparator());
        map.put(SortType.SORT_SENDER, new SenderComparator());
        map.put(SortType.SORT_UNREAD, new UnreadComparator());

        // make it immutable to prevent accidental alteration (content is immutable already)
        SORT_COMPARATORS = Collections.unmodifiableMap(map);
    }

    private static enum FolderOperation {
        COPY, MOVE
    }

    OnMailSelected mCallBack;

    private SortType mSortType = SortType.SORT_DATE;
    private boolean mSortAscending = true;
    private boolean mSortDateAscending = false;

    private MultiSelector mMultiSelector = new MultiSelector();
    private RelativeLayout layoutLoading;
    private ImageButton fabCompose;
    private SwipeRefreshLayout mSwipe;
    private RecyclerView mList;
    private RecyclerView.LayoutManager mLayoutManager;
    private MailAdapter mAdapter;
    private TextView imgEmpty;
    private MessageHelper mMessageHelper;
    private Preferences mPreferences;

    private boolean mLoaderJustInitialized;
    private MessageReference mActiveMessage;

   private MessageListHandler mHandler = new MessageListHandler(this);
   private final ActivityListener mListener = new MessageListActivityListener();

    private FolderInfoHolder mCurrentFolder;
    private FolderInfoHolder mCurrentFolder2;

    private LocalSearch mSearch = null;
    private MessagingController mController;

    private Account mAccount;
    private String[] mAccountUuids;

    private String mFolderName;
    private String mFolderName2;
    private String mTitle;

    private boolean mIsDraft;
    private boolean mThreadedList;
    private boolean mIsThreadDisplay;
    private boolean mSingleAccountMode;
    private boolean mSingleFolderMode;
    private boolean mAllAccounts;
    private Boolean mHasConnectivity;
    private Context mContext;

    private LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mCacheBroadcastReceiver;
    private IntentFilter mCacheIntentFilter;

    private Cursor[] mCursors;
    private boolean[] mCursorValid;
    private int mUniqueIdColumn;

    public interface OnMailSelected{
        public void onMailSelected(MessageReference ref);
        public void onMailSelected(MessageReference ref, int pos);
        public void onDraftSelected(MessageReference ref);
        public void onMailCompose();
    }

    static class MessageListHandler extends Handler {
        private static final int ACTION_FOLDER_LOADING = 1;
        private static final int ACTION_REFRESH_TITLE = 2;
        private static final int ACTION_PROGRESS = 3;
        private static final int ACTION_REMOTE_SEARCH_FINISHED = 4;
        private static final int ACTION_GO_BACK = 5;
        private static final int ACTION_RESTORE_LIST_POSITION = 6;
        private static final int ACTION_OPEN_MESSAGE = 7;

        private WeakReference<MailListFragment> mFragment;

        public MessageListHandler(MailListFragment fragment) {
            mFragment = new WeakReference<MailListFragment>(fragment);
        }
        public void folderLoading(String folder, boolean loading) {
            android.os.Message msg = android.os.Message.obtain(this, ACTION_FOLDER_LOADING,
                    (loading) ? 1 : 0, 0, folder);
            sendMessage(msg);
        }

        public void refreshTitle() {
            android.os.Message msg = android.os.Message.obtain(this, ACTION_REFRESH_TITLE);
            sendMessage(msg);
        }

        public void progress(final boolean progress) {
            android.os.Message msg = android.os.Message.obtain(this, ACTION_PROGRESS,
                    (progress) ? 1 : 0, 0);
            sendMessage(msg);
        }

        public void remoteSearchFinished() {
            android.os.Message msg = android.os.Message.obtain(this, ACTION_REMOTE_SEARCH_FINISHED);
            sendMessage(msg);
        }

        public void updateFooter(final String message) {
            post(new Runnable() {
                @Override
                public void run() {
                    MailListFragment fragment = mFragment.get();
                    if (fragment != null) {
                        //fragment.updateFooter(message);
                    }
                }
            });
        }

        public void goBack() {
            android.os.Message msg = android.os.Message.obtain(this, ACTION_GO_BACK);
            sendMessage(msg);
        }

        public void restoreListPosition() {
            MailListFragment fragment = mFragment.get();
            if (fragment != null) {
                //android.os.Message msg = android.os.Message.obtain(this, ACTION_RESTORE_LIST_POSITION,
                //      fragment.mSavedListState);
                //fragment.mSavedListState = null;
                //sendMessage(msg);
            }
        }

        public void openMessage(MessageReference messageReference) {
            android.os.Message msg = android.os.Message.obtain(this, ACTION_OPEN_MESSAGE,
                    messageReference);
            sendMessage(msg);
        }

        @Override
        public void handleMessage(android.os.Message msg) {
            MailListFragment fragment = mFragment.get();
            if (fragment == null) {
                return;
            }

            // The following messages don't need an attached activity.
            switch (msg.what) {
                case ACTION_REMOTE_SEARCH_FINISHED: {
                   // fragment.remoteSearchFinished();
                    return;
                }
            }

            // Discard messages if the fragment isn't attached to an activity anymore.
            Activity activity = fragment.getActivity();
            if (activity == null) {
                return;
            }

            switch (msg.what) {
                case ACTION_FOLDER_LOADING: {
                    String folder = (String) msg.obj;
                    boolean loading = (msg.arg1 == 1);
                    //fragment.folderLoading(folder, loading);
                    break;
                }
                case ACTION_REFRESH_TITLE: {
                  //  fragment.updateTitle();
                    break;
                }
                case ACTION_PROGRESS: {
                    boolean progress = (msg.arg1 == 1);
                  //  fragment.progress(progress);
                    break;
                }
                case ACTION_GO_BACK: {
                 //   fragment.mFragmentListener.goBack();
                    break;
                }
                case ACTION_RESTORE_LIST_POSITION: {
                    //fragment.mList.onRestoreInstanceState((Parcelable) msg.obj);
                    break;
                }
                case ACTION_OPEN_MESSAGE: {
                    MessageReference messageReference = (MessageReference) msg.obj;
                   // fragment.mFragmentListener.openMessage(messageReference);
                    break;
                }
            }
        }
    }

    public static MailListFragment newInstance(LocalSearch search, boolean isThreadDisplay, boolean threadedList) {
        MailListFragment fragment = new MailListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_SEARCH, search);
        args.putBoolean(ARG_IS_THREAD_DISPLAY, isThreadDisplay);
        args.putBoolean(ARG_THREADED_LIST, threadedList);
        fragment.setArguments(args);
        return fragment;
    }

    public static MailListFragment newInstance(LocalSearch search, boolean isThreadDisplay, boolean threadedList, boolean isDraft) {
        MailListFragment fragment = new MailListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_SEARCH, search);
        args.putBoolean(ARG_IS_THREAD_DISPLAY, isThreadDisplay);
        args.putBoolean(ARG_THREADED_LIST, threadedList);
        args.putBoolean(ARG_IS_DRAFT, isDraft);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);

        mPreferences = Preferences.getPreferences(getActivity().getApplicationContext());
        mController = MessagingController.getInstance(getActivity().getApplication());

        restoreInstanceState(savedInstanceState);
        decodeArguments();

        createCacheBroadcastReceiver(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mSwipe = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_container);
        mList = (RecyclerView)rootView.findViewById(R.id.list_mails);
        imgEmpty = (TextView)rootView.findViewById(R.id.img_empty);
        layoutLoading = (RelativeLayout)rootView.findViewById(R.id.layout_loading);
        fabCompose = (ImageButton)rootView.findViewById(R.id.fab_compose);

        fabCompose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallBack.onMailCompose();
            }
        });

        mList.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mList.setLayoutManager(mLayoutManager);
        mList.setItemAnimator(new DefaultItemAnimator());

        mList.addItemDecoration(
                new DividerItemDecoration(getActivity(), null));

        mList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int threshold = 1;
                int count = mList.getChildCount();

                LinearLayoutManager layoutManager = ((LinearLayoutManager)mList.getLayoutManager());

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (layoutManager.findLastVisibleItemPosition() >= count
                            - threshold) {
                        try {
                            mController.loadMoreMessages(mAccount, mFolderName, null);
                            if(mFolderName2 != null)
                                mController.loadMoreMessages(mAccount, mFolderName2, null);
                        }catch (IllegalArgumentException e){
                            mList.setOnScrollListener(null);
                        }
                    }
                }
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mList.setAdapter(mAdapter);

        mSwipe.setOnRefreshListener(this);
        mSwipe.setColorSchemeResources(R.color.accent);

        //mSwipe.setEnabled(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSwipe.setRefreshing(true);
        mSwipe.setEnabled(false);

        mMessageHelper = MessageHelper.getInstance(getActivity());

        initializeMessageList();

        // This needs to be done before initializing the cursor loader below
        initializeSortSettings();

        mLoaderJustInitialized = true;
        LoaderManager loaderManager = getLoaderManager();
        int len = mAccountUuids.length;
        mCursors = new Cursor[len];
        mCursorValid = new boolean[len];
        for (int i = 0; i < len; i++) {
            loaderManager.initLoader(i, null, this);
            mCursorValid[i] = false;
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if(mPreferences == null)
            mPreferences = Preferences.getPreferences(getActivity().getApplicationContext());
      //  if(mController == null)
        //    mController = MessagingController.getInstance(getActivity().getApplication());

        mHasConnectivity = Utility.hasConnectivity(getActivity().getApplication());

        if(!mHasConnectivity){
            Toast.makeText(getActivity(),R.string.alert_no_internet,Toast.LENGTH_SHORT).show();
        }

        if (!mLoaderJustInitialized) {
            restartLoader();
        } else {
            mLoaderJustInitialized = false;
        }

        mLocalBroadcastManager.registerReceiver(mCacheBroadcastReceiver, mCacheIntentFilter);
        mListener.onResume(getActivity());
        mController.addListener(mListener);
    }

    @Override
    public void onPause() {
        super.onPause();

        mLocalBroadcastManager.unregisterReceiver(mCacheBroadcastReceiver);
        mListener.onPause(getActivity());
        mController.removeListener(mListener);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mContext = activity.getApplicationContext();

        try{
            mCallBack = (OnMailSelected) activity;
        }catch(ClassCastException e){
            throw new ClassCastException(activity.toString()+ "Excepcion");
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mCallBack = null;
    }

    @Override
    public void onRefresh() {
        checkMail();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(layoutLoading.getVisibility() == View.VISIBLE){
                    mSwipe.setRefreshing(false);
                    mSwipe.setEnabled(true);
                    showEmpty(true);
                    layoutLoading.setVisibility(View.GONE);
                }
            }
        }, 3000);

        String accountUuid = mAccountUuids[id];
        Account account = mPreferences.getAccount(accountUuid);

        String threadId = getThreadId(mSearch);

        Uri uri;
        String[] projection;
        boolean needConditions;
        if (threadId != null) {
            uri = Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + accountUuid + "/thread/" + threadId);
            projection = PROJECTION;
            needConditions = false;
        } else if (mThreadedList) {
            uri = Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + accountUuid + "/messages/threaded");
            projection = THREADED_PROJECTION;
            needConditions = true;
        } else {
            uri = Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + accountUuid + "/messages");
            projection = PROJECTION;
            needConditions = true;
        }

        StringBuilder query = new StringBuilder();
        List<String> queryArgs = new ArrayList<>();
        if (needConditions) {
            boolean selectActive = mActiveMessage != null && mActiveMessage.accountUuid.equals(accountUuid);

            if (selectActive) {
                query.append("(" + MessageColumns.UID + " = ? AND " + SpecialColumns.FOLDER_NAME + " = ?) OR (");
                queryArgs.add(mActiveMessage.uid);
                queryArgs.add(mActiveMessage.folderName);
            }

            SqlQueryBuilder.buildWhereClause(account, mSearch.getConditions(), query, queryArgs);

            if (selectActive) {
                query.append(')');
            }
        }

        String selection = query.toString();
        String[] selectionArgs = queryArgs.toArray(new String[0]);

        String sortOrder = buildSortOrder();

        return new CursorLoader(getActivity(), uri, projection, selection, selectionArgs,
                sortOrder);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mMultiSelector.clearSelections();
        mAdapter.swapCursor(null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data != null) {
            if (mIsThreadDisplay && data.getCount() == 0) {
                mHandler.goBack();
                showEmpty(true);
                return;
            } else if (data.getCount() == 0) {
                showEmpty(true);
                return;
            }
        }else{
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(
                    new Runnable() {
                        @Override
                        public void run() {
                            ((MainActivity) getActivity()).onOpenAccount(Preferences.getPreferences(getActivity()).getDefaultAccount());
                        }
                    });
        }

        showEmpty(false);

        if(layoutLoading.getVisibility() == View.VISIBLE){
            layoutLoading.setVisibility(View.GONE);
        }

        // Remove the "Loading..." view
        //mPullToRefreshView.setEmptyView(null);

       // mSwipe.setProgressViewOffset(false, 0,
         //       (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics()));
        mSwipe.setRefreshing(false);
        mSwipe.setEnabled(true);
        //setPullToRefreshEnabled(isPullToRefreshAllowed());

        final int loaderId = loader.getId();
        mCursors[loaderId] = data;
        mCursorValid[loaderId] = true;

        Cursor cursor;
        if (mCursors.length > 1) {
            cursor = new MergeCursorWithUniqueId(mCursors, getComparator());
            mUniqueIdColumn = cursor.getColumnIndex("_id");
        } else {
            cursor = data;
            mUniqueIdColumn = ID_COLUMN;
        }

        if (mIsThreadDisplay) {
            if (cursor.moveToFirst()) {
                mTitle = cursor.getString(SUBJECT_COLUMN);
                if (!StringUtils.isNullOrEmpty(mTitle)) {
                    mTitle = Utility.stripSubject(mTitle);
                }
                if (StringUtils.isNullOrEmpty(mTitle)) {
                    mTitle = getString(R.string.general_no_subject);
                }
            } else {
                //TODO: empty thread view -> return to full message list
            }
        }

        //cleanupSelected(cursor);
        //updateContextMenu(cursor);

        if(cursor != null)
            mAdapter.swapCursor(cursor);

      //  resetActionMode();
       // computeBatchDirection();

        if (isLoadFinished()) {
           // if (mSavedListState != null) {
                mHandler.restoreListPosition();
           // }

           // mFragmentListener.updateMenu();
        }
    }

    public boolean isLoadFinished() {
        if (mCursorValid == null) {
            return false;
        }

        for (boolean cursorValid : mCursorValid) {
            if (!cursorValid) {
                return false;
            }
        }

        return true;
    }

    private void createCacheBroadcastReceiver(Context appContext) {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(appContext);

        mCacheBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mAdapter.notifyDataSetChanged();
            }
        };

        mCacheIntentFilter = new IntentFilter(EmailProviderCache.ACTION_CACHE_UPDATED);
    }

    private void restoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }

        //restoreSelectedMessages(savedInstanceState);

        //mRemoteSearchPerformed = savedInstanceState.getBoolean(STATE_REMOTE_SEARCH_PERFORMED);
        //mSavedListState = savedInstanceState.getParcelable(STATE_MESSAGE_LIST);
        mActiveMessage = savedInstanceState.getParcelable(STATE_ACTIVE_MESSAGE);
    }

    private void restartLoader() {
        if (mCursorValid == null) {
            return;
        }

        // Refresh the message list
        LoaderManager loaderManager = getLoaderManager();
        for (int i = 0; i < mAccountUuids.length; i++) {
            loaderManager.restartLoader(i, null, this);
            mCursorValid[i] = false;
        }
    }

    private String buildSortOrder() {
        String sortColumn = MessageColumns.ID;
        switch (mSortType) {
            case SORT_ARRIVAL: {
                sortColumn = MessageColumns.INTERNAL_DATE;
                break;
            }
            case SORT_ATTACHMENT: {
                sortColumn = "(" + MessageColumns.ATTACHMENT_COUNT + " < 1)";
                break;
            }
            case SORT_FLAGGED: {
                sortColumn = "(" + MessageColumns.FLAGGED + " != 1)";
                break;
            }
            case SORT_SENDER: {
                sortColumn = MessageColumns.SENDER_LIST;
                break;
            }
            case SORT_SUBJECT: {
                sortColumn = MessageColumns.SUBJECT + " COLLATE NOCASE";
                break;
            }
            case SORT_UNREAD: {
                sortColumn = MessageColumns.READ;
                break;
            }
            case SORT_DATE:
            default: {
                sortColumn = MessageColumns.DATE;
            }
        }

        String sortDirection = (mSortAscending) ? " ASC" : " DESC";
        String secondarySort;
        if (mSortType == SortType.SORT_DATE || mSortType == SortType.SORT_ARRIVAL) {
            secondarySort = "";
        } else {
            secondarySort = MessageColumns.DATE + ((mSortDateAscending) ? " ASC, " : " DESC, ");
        }

        String sortOrder = sortColumn + sortDirection + ", " + secondarySort +
                MessageColumns.ID + " DESC";
        return sortOrder;
    }

    private void showEmpty(boolean show){
        imgEmpty.setVisibility(show ? View.VISIBLE: View.GONE);
    }

    private String getThreadId(LocalSearch search) {
        for (ConditionsTreeNode node : search.getLeafSet()) {
            SearchCondition condition = node.mCondition;
            if (condition.field == Searchfield.THREAD_ID) {
                return condition.value;
            }
        }

        return null;
    }

    protected Comparator<Cursor> getComparator() {
        final List<Comparator<Cursor>> chain =
                new ArrayList<Comparator<Cursor>>(3 /* we add 3 comparators at most */);

        // Add the specified comparator
        final Comparator<Cursor> comparator = SORT_COMPARATORS.get(mSortType);
        if (mSortAscending) {
            chain.add(comparator);
        } else {
            chain.add(new ReverseComparator<Cursor>(comparator));
        }

        // Add the date comparator if not already specified
        if (mSortType != SortType.SORT_DATE && mSortType != SortType.SORT_ARRIVAL) {
            final Comparator<Cursor> dateComparator = SORT_COMPARATORS.get(SortType.SORT_DATE);
            if (mSortDateAscending) {
                chain.add(dateComparator);
            } else {
                chain.add(new ReverseComparator<Cursor>(dateComparator));
            }
        }

        // Add the id comparator
        chain.add(new ReverseIdComparator());

        // Build the comparator chain
        return new ComparatorChain<Cursor>(chain);
    }

    private void initializeSortSettings() {
        if (mSingleAccountMode) {
            mSortType = mAccount.getSortType();
            mSortAscending = mAccount.isSortAscending(mSortType);
            mSortDateAscending = mAccount.isSortAscending(SortType.SORT_DATE);
        } else {
            mSortType = NYXMail.getSortType();
            mSortAscending = NYXMail.isSortAscending(mSortType);
            mSortDateAscending = NYXMail.isSortAscending(SortType.SORT_DATE);
        }
    }

    private void decodeArguments() {
        Bundle args = getArguments();

        if(args.getString(SearchManager.QUERY) != null){
            String query = args.getString(SearchManager.QUERY).trim();

            mSearch = new LocalSearch("Busqueda");
            mSearch.setManualSearch(true);
            //mNoThreading = true;

            mSearch.or(new SearchCondition(Searchfield.SENDER, Attribute.CONTAINS, query));
            mSearch.or(new SearchCondition(Searchfield.SUBJECT, Attribute.CONTAINS, query));
            mSearch.or(new SearchCondition(Searchfield.MESSAGE_CONTENTS, Attribute.CONTAINS, query));

            if(args.getString(EXTRA_SEARCH_ACCOUNT) != null){
                mSearch.addAccountUuid(args.getString(EXTRA_SEARCH_ACCOUNT));
            // searches started from a folder list activity will provide an account, but no folder
            } else {
                mSearch.addAccountUuid(LocalSearch.ALL_ACCOUNTS);
            }

        }else {

            mIsDraft = args.getBoolean(ARG_IS_DRAFT, false);
            mThreadedList = args.getBoolean(ARG_THREADED_LIST, false);
            mIsThreadDisplay = args.getBoolean(ARG_IS_THREAD_DISPLAY, false);
            mSearch = args.getParcelable(ARG_SEARCH);
            mTitle = mSearch.getName();
        }

        String[] accountUuids = mSearch.getAccountUuids();

        mSingleAccountMode = false;
        if (accountUuids.length == 1 && !mSearch.searchAllAccounts()) {
            mSingleAccountMode = true;
            mAccount = mPreferences.getAccount(accountUuids[0]);
        }

        mSingleFolderMode = false;
        if (mSingleAccountMode && (mSearch.getFolderNames().size() == 1)) {
            mSingleFolderMode = true;
            mFolderName = mSearch.getFolderNames().get(0);
            mCurrentFolder = getFolder(mFolderName, mAccount);
            switch (mFolderName){
                case "Bandeja de Salida":
                    mFolderName2 = getString(R.string.special_mailbox_name_outbox2);
                    break;
                case "Borradores":
                    mFolderName2 = getString(R.string.special_mailbox_name_drafts2);
                    break;
                case "Papelera":
                    mFolderName2 = getString(R.string.special_mailbox_name_trash2);
                    break;
                case "Enviados":
                    mFolderName2 = getString(R.string.special_mailbox_name_sent2);
                    break;
                case "Guardados":
                    mFolderName2 = getString(R.string.special_mailbox_name_archive2);
                    break;
            }
            if(mFolderName2 != null) {
                mSearch.addAllowedFolder(mFolderName2);
            }
        }

        mAllAccounts = false;
        if (mSingleAccountMode) {
            mAccountUuids = new String[]{mAccount.getUuid()};
        } else {
            if (accountUuids.length == 1 &&
                    accountUuids[0].equals(SearchSpecification.ALL_ACCOUNTS)) {
                mAllAccounts = true;

                Account[] accounts = mPreferences.getAccounts();

                mAccountUuids = new String[accounts.length];
                for (int i = 0, len = accounts.length; i < len; i++) {
                    mAccountUuids[i] = accounts[i].getUuid();
                }

                if (mAccountUuids.length == 1) {
                    mSingleAccountMode = true;
                    mAccount = accounts[0];
                }
            } else {
                mAccountUuids = accountUuids;
            }
        }

    }

    private void initializeMessageList(){
        mAdapter = new MailAdapter(getActivity());

        if (mFolderName != null) {
            mCurrentFolder = getFolder(mFolderName, mAccount);
        }
        if(mFolderName2 != null){
            mCurrentFolder2 = getFolder(mFolderName2, mAccount);
        }

        if (mSingleFolderMode) {
           // mListView.addFooterView(getFooterView(mListView));
           // updateFooterView();
        }

        mList.setAdapter(mAdapter);

        checkMail();
    }

    public void checkMail() {
        if (isSingleAccountMode() && isSingleFolderMode()) {
            mController.synchronizeMailbox(mAccount, mFolderName, mListener, null);
            if(mFolderName2 != null)
                mController.synchronizeMailbox(mAccount, mFolderName2, mListener, null);

            mController.sendPendingMessages(mAccount, mListener);
        } else if (mAllAccounts) {
            mController.checkMail(getActivity().getApplicationContext(), null, true, true, mListener);
        } else {
            for (String accountUuid : mAccountUuids) {
                Account account = mPreferences.getAccount(accountUuid);
                mController.checkMail(getActivity().getApplicationContext(), account, true, true, mListener);
            }
        }
    }

    private FolderInfoHolder getFolder(String folder, Account account) {
        LocalFolder localFolder = null;
        try {
            LocalStore localStore = account.getLocalStore();
            localFolder = localStore.getFolder(folder);
            return new FolderInfoHolder(getActivity().getApplicationContext(), localFolder, account);
        } catch (Exception e) {
            return null;
        } finally {
            if (localFolder != null) {
                localFolder.close();
            }
        }
    }

    private static String getSenderAddressFromCursor(Cursor cursor) {
        String fromList = cursor.getString(SENDER_LIST_COLUMN);
        Address[] fromAddrs = Address.unpack(fromList);
        return (fromAddrs.length > 0) ? fromAddrs[0].getAddress() : null;
    }

    private Account getAccountFromCursor(Cursor cursor) {
        String accountUuid = cursor.getString(ACCOUNT_UUID_COLUMN);
        return mPreferences.getAccount(accountUuid);
    }

    public boolean isSingleAccountMode() {
        return mSingleAccountMode;
    }

    public boolean isSingleFolderMode() {
        return mSingleFolderMode;
    }

    private void displayFolderChoice(final Folder folder, String accountUuid, String lastSelectedFolderName,
                                     final List<Message> messages) {

        Resources res = getActivity().getResources();
        final String[] folders = res.getStringArray(R.array.folders);
        new MaterialDialog.Builder(getActivity())
                .title(R.string.alert_move_title)
                .titleColor(res.getColor(R.color.accent))
                .items(R.array.folders)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        move(messages, folders[which]);

                    }
                })
                .show();
    }

    private int getPosition(MessageReference messageReference) {
        for (int i = 0, len = mAdapter.getItemCount(); i < len; i++) {
            Cursor cursor = (Cursor) mAdapter.getItem(i);

            String accountUuid = cursor.getString(ACCOUNT_UUID_COLUMN);
            String folderName = cursor.getString(FOLDER_NAME_COLUMN);
            String uid = cursor.getString(UID_COLUMN);

            if (accountUuid.equals(messageReference.accountUuid) &&
                    folderName.equals(messageReference.folderName) &&
                    uid.equals(messageReference.uid)) {
                return i;
            }
        }

        return -1;
    }

    public boolean openPrevious(MessageReference messageReference) {
        int position = getPosition(messageReference);
        if (position <= 0) {
            return false;
        }

        openMessageAtPosition(position - 1);
        return true;
    }

    public boolean openNext(MessageReference messageReference) {
        int position = getPosition(messageReference);
        if (position < 0 || position == mAdapter.getItemCount() - 1) {
            return false;
        }

        openMessageAtPosition(position + 1);
        return true;
    }

    public void openMessageAtPosition(int position) {
        MessageReference ref = getReferenceForPosition(position);
        mCallBack.onMailSelected(ref, position);
    }

    private MessageReference getReferenceForPosition(int position) {
        Cursor cursor = (Cursor) mAdapter.getItem(position);
        MessageReference ref = new MessageReference();
        ref.accountUuid = cursor.getString(ACCOUNT_UUID_COLUMN);
        ref.folderName = cursor.getString(FOLDER_NAME_COLUMN);
        ref.uid = cursor.getString(UID_COLUMN);

        return ref;
    }

    //Action Mode Methods
    private boolean checkCopyOrMovePossible(final List<Message> messages, final FolderOperation operation) {

        if (messages.isEmpty()) {
            return false;
        }

        boolean first = true;
        for (final Message message : messages) {
            if (first) {
                first = false;
                // account check
                final Account account = message.getFolder().getAccount();
                if ((operation == FolderOperation.MOVE && !mController.isMoveCapable(account)) ||
                        (operation == FolderOperation.COPY && !mController.isCopyCapable(account))) {
                    return false;
                }
            }
            // message check
            if ((operation == FolderOperation.MOVE && !mController.isMoveCapable(message)) ||
                    (operation == FolderOperation.COPY && !mController.isCopyCapable(message))) {
                final Toast toast = Toast.makeText(getActivity(), R.string.toast_cannot_move,
                        Toast.LENGTH_LONG);
                toast.show();
                return false;
            }
        }
        return true;
    }

    private void onMove(List<Message> messages) {
        if (!checkCopyOrMovePossible(messages, FolderOperation.MOVE)) {
            return;
        }

        final Folder folder;
        if (mIsThreadDisplay) {
            folder = messages.get(0).getFolder();
        } else if (mSingleFolderMode) {
            folder = mCurrentFolder.folder;
        } else {
            folder = null;
        }


        displayFolderChoice(folder,
                messages.get(0).getFolder().getAccount().getUuid(), null,
                messages);
    }

    private void onArchive(final List<Message> messages) {
        Map<Account, List<Message>> messagesByAccount = groupMessagesByAccount(messages);

        for (Map.Entry<Account, List<Message>> entry : messagesByAccount.entrySet()) {
            Account account = entry.getKey();
            String archiveFolder = account.getArchiveFolderName();

            if (!NYXMail.FOLDER_NONE.equals(archiveFolder)) {
                move(entry.getValue(), archiveFolder);
            }
        }
    }

    private void move(List<Message> messages, final String destination) {

        Map<String, List<Message>> folderMap = new HashMap<>();

        for (Message message : messages) {
            if (!mController.isMoveCapable(message) ) {

                Toast.makeText(getActivity(), R.string.toast_cannot_move,
                        Toast.LENGTH_LONG).show();

                // XXX return meaningful error value?

                // message isn't synchronized
                return;
            }

            String folderName = message.getFolder().getName();
            if (folderName.equals(destination)) {
                // Skip messages already in the destination folder
                continue;
            }

            List<Message> outMessages = folderMap.get(folderName);
            if (outMessages == null) {
                outMessages = new ArrayList<>();
                folderMap.put(folderName, outMessages);
            }

            outMessages.add(message);
        }

        for (Map.Entry<String, List<Message>> entry : folderMap.entrySet()) {
            String folderName = entry.getKey();
            List<Message> outMessages = entry.getValue();
            Account account = outMessages.get(0).getFolder().getAccount();

            if (mThreadedList) {
                mController.moveMessagesInThread(account, folderName, outMessages, destination);
            } else {
                mController.moveMessages(account, folderName, outMessages, destination, null);
            }
        }
    }

    private Map<Account, List<Message>> groupMessagesByAccount(final List<Message> messages) {
        Map<Account, List<Message>> messagesByAccount = new HashMap<Account, List<Message>>();
        for (Message message : messages) {
            Account account = message.getFolder().getAccount();

            List<Message> msgList = messagesByAccount.get(account);
            if (msgList == null) {
                msgList = new ArrayList<>();
                messagesByAccount.put(account, msgList);
            }

            msgList.add(message);
        }
        return messagesByAccount;
    }

    private void onDelete(List<Message> messages) {
        if (mThreadedList) {
            mController.deleteThreads(messages);
        } else {
            mController.deleteMessages(messages, null);
        }
        Toast.makeText(getActivity(),R.string.toast_deleted,Toast.LENGTH_SHORT).show();
    }

    private void setFlagForSelected(final Flag flag, final boolean newState) {
        if (mMultiSelector.getSelectedPositions().isEmpty()) {
            return;
        }

        Map<Account, List<Long>> messageMap = new HashMap<Account, List<Long>>();
        Map<Account, List<Long>> threadMap = new HashMap<Account, List<Long>>();
        Set<Account> accounts = new HashSet<Account>();

        for (int position = 0, end = mAdapter.getItemCount(); position < end; position++) {

            if (mMultiSelector.isSelected(position, 0)) {
                Cursor cursor = (Cursor) mAdapter.getItem(position);
                String uuid = cursor.getString(ACCOUNT_UUID_COLUMN);
                Account account = mPreferences.getAccount(uuid);
                accounts.add(account);

                if (mThreadedList && cursor.getInt(THREAD_COUNT_COLUMN) > 1) {
                    List<Long> threadRootIdList = threadMap.get(account);
                    if (threadRootIdList == null) {
                        threadRootIdList = new ArrayList<Long>();
                        threadMap.put(account, threadRootIdList);
                    }

                    threadRootIdList.add(cursor.getLong(THREAD_ROOT_COLUMN));
                } else {
                    List<Long> messageIdList = messageMap.get(account);
                    if (messageIdList == null) {
                        messageIdList = new ArrayList<Long>();
                        messageMap.put(account, messageIdList);
                    }

                    messageIdList.add(cursor.getLong(ID_COLUMN));
                }
            }
        }

        for (Account account : accounts) {
            List<Long> messageIds = messageMap.get(account);
            List<Long> threadRootIds = threadMap.get(account);

            if (messageIds != null) {
                mController.setFlag(account, messageIds, flag, newState);
            }

            if (threadRootIds != null) {
                mController.setFlagForThreads(account, threadRootIds, flag, newState);
            }
        }

        computeBatchDirection();
    }

    private void computeBatchDirection() {
        boolean isBatchFlag = false;
        boolean isBatchRead = false;

        for (int i = 0, end = mAdapter.getItemCount(); i < end; i++) {

            if (mMultiSelector.isSelected(i, 0)) {
                Cursor cursor = (Cursor) mAdapter.getItem(i);
                boolean read = (cursor.getInt(READ_COLUMN) == 1);
                boolean flagged = (cursor.getInt(FLAGGED_COLUMN) == 1);

                if (!flagged) {
                    isBatchFlag = true;
                }
                if (!read) {
                    isBatchRead = true;
                }

                if (isBatchFlag && isBatchRead) {
                    break;
                }
            }
        }

        //mDeleteMode.showMarkAsRead(isBatchRead);
        //mDeleteMode.showFlag(isBatchFlag);
    }

    //DeleteMode
    private ActionMode.Callback mDeleteMode = new ModalMultiSelectorCallback(mMultiSelector) {

        private MenuItem mMarkAsRead;
        private MenuItem mMarkAsUnread;

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu){
            mMultiSelector.setSelectable(true);

            mMarkAsRead = menu.findItem(R.id.action_read);
            mMarkAsUnread = menu.findItem(R.id.action_unread);

            // we don't support cross account actions atm
            if (!mSingleAccountMode) {
                // show all
                menu.findItem(R.id.action_move).setVisible(true);
                menu.findItem(R.id.action_archive).setVisible(true);

                Set<String> accountUuids = getAccountUuidsForSelected();

                for (String accountUuid : accountUuids) {
                    Account account = mPreferences.getAccount(accountUuid);
                    if (account != null) {
                        setContextCapabilities(account, menu);
                    }
                }

            }
            return false;
        }

        private Set<String> getAccountUuidsForSelected() {
            int maxAccounts = mAccountUuids.length;
            Set<String> accountUuids = new HashSet<>(maxAccounts);

            for (int position = 0, end = mAdapter.getItemCount(); position < end; position++) {

                if (mMultiSelector.isSelected(position, 0)) {
                    Cursor cursor = (Cursor) mAdapter.getItem(position);
                    String accountUuid = cursor.getString(ACCOUNT_UUID_COLUMN);
                    accountUuids.add(accountUuid);

                    if (accountUuids.size() == mAccountUuids.length) {
                        break;
                    }
                }
            }

            return accountUuids;
        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            getActivity().getMenuInflater().inflate(R.menu.mail_selected, menu);

            setContextCapabilities(mAccount, menu);

            return true;
        }

        private void setContextCapabilities(Account account, Menu menu) {
            if (!mSingleAccountMode) {
                menu.findItem(R.id.action_move).setVisible(false);

                // belong to non-POP3 accounts
                menu.findItem(R.id.action_archive).setVisible(false);

            } else {
                // hide unsupported

                if (!mController.isMoveCapable(account)) {
                    menu.findItem(R.id.action_move).setVisible(false);
                    menu.findItem(R.id.action_archive).setVisible(false);
                }

                if (!account.hasArchiveFolder()) {
                    menu.findItem(R.id.action_archive).setVisible(false);
                }
            }
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.action_delete:
                    final ActionMode ac = actionMode;
                    final List<Message> messages = getCheckedMessages();

                    Resources res = getResources();
                    new MaterialDialog.Builder(getActivity())
                            .title(R.string.alert_title_delete_many)
                            .content(R.string.alert_delete_many)
                            .titleColor(res.getColor(R.color.accent))
                            .positiveColor(res.getColor(R.color.accent))
                            .negativeColor(res.getColor(R.color.gray_text))
                            .positiveText(R.string.btn_ok)
                            .negativeText(R.string.btn_cancel)
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    ac.finish();
                                    onDelete(messages);
                                    mMultiSelector.clearSelections();
                                }
                            })
                            .show();

                    return true;

                case R.id.action_move:
                    onMove(getCheckedMessages());
                    actionMode.finish();
                    mMultiSelector.clearSelections();
                    return true;

                case R.id.action_archive:
                    onArchive(getCheckedMessages());
                    actionMode.finish();
                    mMultiSelector.clearSelections();
                    return true;

                case R.id.action_read:
                    setFlagForSelected(Flag.SEEN, true);
                    actionMode.finish();
                    mMultiSelector.clearSelections();
                    return true;

                case R.id.action_unread:
                    setFlagForSelected(Flag.SEEN, false);
                    actionMode.finish();
                    mMultiSelector.clearSelections();
                    return true;

                /*
                case R.id.menu_item_delete_crime:
                    // Need to finish the action mode before doing the following,
                    // not after. No idea why, but it crashes.
                    actionMode.finish();

                    for (int i = mCrimes.size()-1; i >= 0; i--) {
                        if (mMultiSelector.isSelected(i, 0)) {
                            Crime crime = mCrimes.get(i);
                            CrimeLab.get(getActivity()).deleteCrime(crime);
                            mRecyclerView.getAdapter().notifyItemRemoved(i);
                        }
                    }

                    mMultiSelector.clearSelections();
                    return true;
                    */
                default:
                    break;
            }
            return false;
        }

        public void showMarkAsRead(boolean show) {
            if (mDeleteMode != null) {
                mMarkAsRead.setVisible(show);
                mMarkAsUnread.setVisible(!show);
            }
        }

        private Folder getFolderById(Account account, long folderId) {
            try {
                LocalStore localStore = account.getLocalStore();
                LocalFolder localFolder = localStore.getFolderById(folderId);
                localFolder.open(Folder.OPEN_MODE_RO);
                return localFolder;
            } catch (Exception e) {
                Log.e(NYXMail.LOG_TAG, "getFolderNameById() failed.", e);
                return null;
            }
        }

        private Message getMessageAtPosition(int adapterPosition) {
            if (adapterPosition == AdapterView.INVALID_POSITION) {
                return null;
            }

            Cursor cursor = (Cursor) mAdapter.getItem(adapterPosition);
            String uid = cursor.getString(UID_COLUMN);

            Account account = getAccountFromCursor(cursor);
            long folderId = cursor.getLong(FOLDER_ID_COLUMN);
            Folder folder = getFolderById(account, folderId);

            try {
                return folder.getMessage(uid);
            } catch (MessagingException e) {
                Log.e(NYXMail.LOG_TAG, "Something went wrong while fetching a message", e);
            }

            return null;
        }

        private List<Message> getCheckedMessages() {
            List<Message> messages = new ArrayList<>(mMultiSelector.getSelectedPositions().size());
            for (int position = 0, end = mAdapter.getItemCount(); position < end; position++) {

                if (mMultiSelector.isSelected(position, 0)) {
                    Cursor cursor = (Cursor) mAdapter.getItem(position);
                    Message message = getMessageAtPosition(position);
                    if (message != null) {
                        messages.add(message);
                    }
                }
            }

            return messages;
        }

    };

    class MessageListActivityListener extends ActivityListener {
        @Override
        public void remoteSearchFailed(Account acct, String folder, final String err) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Activity activity = getActivity();
                    if (activity != null) {
                  //      Toast.makeText(activity, R.string.remote_search_error,
                    //            Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        @Override
        public void remoteSearchStarted(Account acct, String folder) {
            mHandler.progress(true);
           // mHandler.updateFooter(mContext.getString(R.string.remote_search_sending_query));
        }

        @Override
        public void enableProgressIndicator(boolean enable) {
            mHandler.progress(enable);
        }

        @Override
        public void remoteSearchFinished(Account acct, String folder, int numResults, List<Message> extraResults) {
            mHandler.progress(false);
            mHandler.remoteSearchFinished();
           // mExtraSearchResults = extraResults;
            if (extraResults != null && extraResults.size() > 0) {
            //    mHandler.updateFooter(String.format(mContext.getString(R.string.load_more_messages_fmt), maxResults));
            } else {
                mHandler.updateFooter("");
            }
         //   mFragmentListener.setMessageListProgress(Window.PROGRESS_END);

        }

        @Override
        public void remoteSearchServerQueryComplete(Account account, String folderName, int numResults) {
            mHandler.progress(true);
            if (account != null &&  account.getRemoteSearchNumResults() != 0 && numResults > account.getRemoteSearchNumResults()) {
             //   mHandler.updateFooter(mContext.getString(R.string.remote_search_downloading_limited,
               //         account.getRemoteSearchNumResults(), numResults));
            } else {
              //  mHandler.updateFooter(mContext.getString(R.string.remote_search_downloading, numResults));
            }
           // mFragmentListener.setMessageListProgress(Window.PROGRESS_START);
        }

        @Override
        public void informUserOfStatus() {
            mHandler.refreshTitle();
        }

        @Override
        public void synchronizeMailboxStarted(Account account, String folder) {
            if (updateForMe(account, folder)) {
                mHandler.progress(true);
                mHandler.folderLoading(folder, true);
            }
            super.synchronizeMailboxStarted(account, folder);
        }

        @Override
        public void synchronizeMailboxFinished(Account account, String folder,
                                               int totalMessagesInMailbox, int numNewMessages) {

            if (updateForMe(account, folder)) {
                mHandler.progress(false);
                mHandler.folderLoading(folder, false);
            }
            super.synchronizeMailboxFinished(account, folder, totalMessagesInMailbox, numNewMessages);
        }

        @Override
        public void synchronizeMailboxFailed(Account account, String folder, String message) {

            if (updateForMe(account, folder)) {
                mHandler.progress(false);
                mHandler.folderLoading(folder, false);
            }
            super.synchronizeMailboxFailed(account, folder, message);
        }

        @Override
        public void folderStatusChanged(Account account, String folder, int unreadMessageCount) {
            if (isSingleAccountMode() && isSingleFolderMode() && mAccount.equals(account) &&
                    mFolderName.equals(folder)) {
                //mUnreadMessageCount = unreadMessageCount;
            }
            super.folderStatusChanged(account, folder, unreadMessageCount);
        }

        private boolean updateForMe(Account account, String folder) {
            if (account == null || folder == null) {
                return false;
            }

            if (!Utility.arrayContains(mAccountUuids, account.getUuid())) {
                return false;
            }

            List<String> folderNames = mSearch.getFolderNames();
            return (folderNames.isEmpty() || folderNames.contains(folder));
        }
    }

    private void setFlag(int adapterPosition, final Flag flag, final boolean newState) {
        if (adapterPosition == AdapterView.INVALID_POSITION) {
            return;
        }

        Cursor cursor = (Cursor) mAdapter.getItem(adapterPosition);
        Account account = mPreferences.getAccount(cursor.getString(ACCOUNT_UUID_COLUMN));

        if (mThreadedList && cursor.getInt(THREAD_COUNT_COLUMN) > 1) {
            long threadRootId = cursor.getLong(THREAD_ROOT_COLUMN);
            mController.setFlagForThreads(account,
                    Collections.singletonList(Long.valueOf(threadRootId)), flag, newState);
        } else {
            long id = cursor.getLong(ID_COLUMN);
            mController.setFlag(account, Collections.singletonList(Long.valueOf(id)), flag,
                    newState);
        }

        computeBatchDirection();
    }

    private void toggleMessageFlagWithAdapterPosition(int adapterPosition) {
        Cursor cursor = (Cursor) mAdapter.getItem(adapterPosition);
        boolean flagged = (cursor.getInt(FLAGGED_COLUMN) == 1);

        setFlag(adapterPosition,Flag.FLAGGED, !flagged);
    }

    //MailAdapter
    private class MailAdapter extends CursorRecyclerViewAdapter<MailAdapter.ViewHolder> {

       // private List<MailObject> mDataSet;
        private ActionMode mode;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends SwappingHolder implements
                View.OnClickListener,
                View.OnLongClickListener {
            // each data item is just a string in this case
            protected TextView mContacto,mAsunto,mContenido,mFecha;
            protected ImageButton mStarred;
            protected ImageView mUnread,mAttachment;

            public ViewHolder(View v) {
                super(v, mMultiSelector);
                mUnread = (ImageView)v.findViewById(R.id.img_unread);
                mContacto = (TextView)v.findViewById(R.id.lbl_contacto);
                mAsunto = (TextView)v.findViewById(R.id.lbl_asunto);
                mContenido = (TextView)v.findViewById(R.id.lbl_contenido);
                mFecha = (TextView)v.findViewById(R.id.lbl_fecha);
                mStarred = (ImageButton)v.findViewById(R.id.starred);
                mAttachment = (ImageView)v.findViewById(R.id.img_attachment);

                mStarred.setOnClickListener(this);
                v.setOnClickListener(this);
                v.setOnLongClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (!mMultiSelector.tapSelection(this)) {
                    if (v instanceof ImageButton) {
                        toggleMessageFlagWithAdapterPosition(getPosition());
                    } else {
                        Cursor cursor = (Cursor) getItem(getPosition());
                        if (cursor == null) {
                            return;
                        }
                        MessageReference ref = new MessageReference();
                        ref.accountUuid = cursor.getString(ACCOUNT_UUID_COLUMN);
                        ref.folderName = cursor.getString(FOLDER_NAME_COLUMN);
                        ref.uid = cursor.getString(UID_COLUMN);
                        if(!mIsDraft)
                            mCallBack.onMailSelected(ref);
                        else
                            mCallBack.onDraftSelected(ref);
                    }
                }else{
                    mode.setTitle(""+mMultiSelector.getSelectedPositions().size());
                    if(mMultiSelector.getSelectedPositions().size() == 0){
                        if(mode != null) {
                            mode.finish();
                            mode = null;
                        }
                    }
                }
            }

            @Override
            public boolean onLongClick(View v) {
                AppCompatActivity activity = (AppCompatActivity)getActivity();
                    mode = activity.startSupportActionMode(mDeleteMode);
                mMultiSelector.setSelected(this, true);
                if(mode != null){
                    mode.setTitle("" + mMultiSelector.getSelectedPositions().size());
                }
                return true;
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MailAdapter(Context context){
            super(context,null);
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_mail_unread, parent, false);

            // set the view's size, margins, paddings and layout parameters
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, Cursor cursor) {
            Account account = getAccountFromCursor(cursor);

            String fromList = cursor.getString(SENDER_LIST_COLUMN);
            String toList = cursor.getString(TO_LIST_COLUMN);
            String ccList = cursor.getString(CC_LIST_COLUMN);
            int attachments = cursor.getInt(ATTACHMENT_COUNT_COLUMN);
            Address[] fromAddrs = Address.unpack(fromList);
            Address[] toAddrs = Address.unpack(toList);
            Address[] ccAddrs = Address.unpack(ccList);

            CharSequence displayName = mMessageHelper.getDisplayName(account, fromAddrs, toAddrs);
            CharSequence displayDate = DateUtils.getRelativeTimeSpanString(getActivity(), cursor.getLong(DATE_COLUMN));

            String subject = cursor.getString(SUBJECT_COLUMN);
            if (TextUtils.isEmpty(subject)) {
                subject = getString(R.string.general_no_subject);
            }
            boolean read = (cursor.getInt(READ_COLUMN) == 1);
            boolean flagged = (cursor.getInt(FLAGGED_COLUMN) == 1);

            String preview = cursor.getString(PREVIEW_COLUMN);

            if(attachments > 0){
                holder.mAttachment.setVisibility(View.VISIBLE);
            }else{
                holder.mAttachment.setVisibility(View.INVISIBLE);
            }

            if(read){
               holder.mUnread.setVisibility(View.INVISIBLE);
                holder.mContacto.setTextColor(getResources().getColor(R.color.black));
            }else{
                holder.mUnread.setVisibility(View.VISIBLE);
                holder.mContacto.setTextColor(getResources().getColor(R.color.accent));
            }
            holder.mContacto.setText(displayName);
            holder.mAsunto.setText(subject);
            holder.mContenido.setText(preview);
            holder.mFecha.setText(displayDate);
            holder.mStarred.setSelected(flagged);
        }

    }
}
