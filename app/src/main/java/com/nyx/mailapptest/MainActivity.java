package com.nyx.mailapptest;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

import com.nyx.mailapptest.activity.MessageReference;
import com.nyx.mailapptest.data.App;
import com.nyx.mailapptest.search.LocalSearch;
import com.nyx.mailapptest.search.SearchAccount;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.BaseAccount;
import com.nyx.mailapptest.utils.Preferences;


public class MainActivity extends BaseActivity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks,
        MailListFragment.OnMailSelected{

    public static final String EXTRA_STARTUP = "startup";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private static final int MAIL_ACTIVITY = 1;
    private static final int SETTINGS_ACTIVITY = 2;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private boolean mNoThreading = false;

    private Account mAccount;

    private MailListFragment listFragment;
    private LocalSearch mSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = "Inbox";

        // Set up the drawer.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                toolbar);

        onOpenAccount(Preferences.getPreferences(this).getDefaultAccount());

    }

    @Override
    public void onNavigationDrawerIntent(){
        Intent i = new Intent(MainActivity.this, NewAccountActivity.class);
        i.putExtra("newAccount", true);
        startActivity(i);
    }

    @Override
    public void onNavigationDrawerSettings(){
        Intent i = new Intent(MainActivity.this, SettingsActivity.class);
        startActivityForResult(i, SETTINGS_ACTIVITY);
    }

    @Override
    public void onNavigationDrawerIntent(BaseAccount account){
        onOpenAccount(account);
    }

    @Override
    public void onNavigationDrawerItemSelected(Account realAccount, int child, String folder) {
        // update the main content by replacing fragments

        if (getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();

        mAccount = realAccount;

        mSearch = new LocalSearch(folder);
        mSearch.addAllowedFolder(folder);

        if(child == 0) {
            mSearch = new LocalSearch(realAccount.getAutoExpandFolderName());
            mSearch.addAllowedFolder(realAccount.getAutoExpandFolderName());
        }

        mSearch.addAccountUuid(realAccount.getUuid());

        setTitle(folder);

        if(folder.equals(getResources().getString(R.string.special_mailbox_name_drafts)))
            listFragment = MailListFragment.newInstance(mSearch, false,!mNoThreading, true);
        else
            listFragment = MailListFragment.newInstance(mSearch, false,!mNoThreading);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, listFragment)
                .addToBackStack(null)
                .commit();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    private void setTitle(String title){
        mTitle = title;
        restoreActionBar();
    }

    @Override
    public void onStart(){
        super.onStart();
        App.activity1 = this;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        App.activity1 = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        Bundle extras = null;
        if(data != null)
            extras = data.getExtras();
        switch (requestCode){
            case MAIL_ACTIVITY:
                if(extras != null){
                    MessageReference ref = extras.getParcelable("message");
                    if (extras.getBoolean("next"))
                        listFragment.openNext(ref);
                    else
                        listFragment.openPrevious(ref);
                }
                break;
            case SETTINGS_ACTIVITY:
                if(extras != null){
                    if(extras.getBoolean("Deleted")){
                        if(Preferences.getPreferences(this).getAccounts().length == 0){
                            Intent i = new Intent(MainActivity.this, NewAccountActivity.class);
                            startActivity(i);
                            finish();
                        }else{
                            mNavigationDrawerFragment.refresh();
                        }
                    }
                }else{
                    mNavigationDrawerFragment.refresh();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);

            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_search:
                Intent i = new Intent(MainActivity.this, SearchActivity.class);
                i.putExtra(MailListFragment.EXTRA_SEARCH_ACCOUNT, mAccount.getUuid());
                startActivity(i);
                overridePendingTransition(0,0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // turn on the Navigation Drawer image;
        // this is called in the LowerLevelFragments
        if(mNavigationDrawerFragment.isDrawerOpen()){
            mNavigationDrawerFragment.closeDrawer();
        }else {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
                if(getFragmentManager().getBackStackEntryCount() > 0) {
                    setTitle("Inbox");
                    mNavigationDrawerFragment.selectChild(0);
                }
            }else {
                Preferences.getPreferences(this).setAppStatus(false);
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onMailCompose(){
        ComposeActivity.actionCompose(this, mAccount);
        overridePendingTransition(R.anim.appear_bottom, R.anim.disappear_zoom);
    }

    @Override
    public void onMailSelected(MessageReference reference){
        System.gc();
        Intent i = new Intent(MainActivity.this, MailActivity.class);
        i.putExtra("message",reference);
        startActivityForResult(i,MAIL_ACTIVITY);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onMailSelected(MessageReference reference, int pos){
        System.gc();
        Intent i = new Intent(MainActivity.this, MailActivity.class);
        i.putExtra("message",reference);
        startActivityForResult(i,MAIL_ACTIVITY);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDraftSelected(MessageReference ref){
        ComposeActivity.actionEditDraft(this, ref);
        overridePendingTransition(R.anim.appear_bottom, R.anim.disappear_zoom);
    }

    public void onOpenAccount(BaseAccount account) {
        setTitle("Inbox");

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        for(int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++)
            getFragmentManager().popBackStack();

        if (account instanceof SearchAccount) {
            SearchAccount searchAccount = (SearchAccount)account;

            mSearch = searchAccount.getRelatedSearch();
        } else {
            Account realAccount = (Account) account;

            mAccount = realAccount;

            if(mAccount == null){
                finish();
                return;
            }
            mSearch = new LocalSearch(realAccount.getAutoExpandFolderName());
            mSearch.addAllowedFolder(realAccount.getAutoExpandFolderName());
            mSearch.addAccountUuid(realAccount.getUuid());
        }
        listFragment = MailListFragment.newInstance(mSearch, false, false);
        transaction.replace(R.id.container, listFragment).commit();
    }

}
