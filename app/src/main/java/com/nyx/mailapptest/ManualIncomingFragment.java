package com.nyx.mailapptest;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nyx.mailapptest.lib.AuthType;
import com.nyx.mailapptest.lib.ConnectionSecurity;
import com.nyx.mailapptest.lib.ServerSettings;
import com.nyx.mailapptest.lib.Store;
import com.nyx.mailapptest.lib.Transport;
import com.nyx.mailapptest.lib.store.ImapStore;
import com.nyx.mailapptest.lib.store.Pop3Store;
import com.nyx.mailapptest.lib.store.WebDavStore;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;
import com.nyx.mailapptest.utils.Utility;

/**
 *
 * Created by nyx on 2/12/15.
 *
 */
public class ManualIncomingFragment extends Fragment {

    private static final String ARG_ACCOUNT = "Account";

    private static final String POP3_PORT = "110";
    private static final String POP3_SSL_PORT = "995";
    private static final String IMAP_PORT = "143";
    private static final String IMAP_SSL_PORT = "993";
    private static final String WEBDAV_PORT = "80";
    private static final String WEBDAV_SSL_PORT = "443";

    private Spinner spinType, spinSecurity;
    private EditText editUser, editPassword, editServer, editPort;
    private TextView txtServer;
    private Button btnNext;

    private String mDefaultPort = "";
    private String mDefaultSslPort = "";

    private Account mAccount;

    private  OnIncomingListener mCallBack;

    private String[] servers = {"Servidor IMAP","Servidor POP","Servidor Exchange"};

    public interface OnIncomingListener{
        public void incomingCreated(Account account);
    }

    public static ManualIncomingFragment newInstance(Account ac){
        ManualIncomingFragment frag = new ManualIncomingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ACCOUNT, ac.getUuid());
        frag.setArguments(args);
        return frag;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_manual_incoming, container, false);

        Toolbar toolbar = (Toolbar)v.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        toolbar.setTitle("Servidor de entrada");
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);

        spinType = (Spinner)v.findViewById(R.id.spinType);
        spinSecurity = (Spinner)v.findViewById(R.id.spinSecurity);
        editUser = (EditText)v.findViewById(R.id.editUser);
        editPassword = (EditText)v.findViewById(R.id.editPassword);
        editServer = (EditText)v.findViewById(R.id.editServer);
        editPort = (EditText)v.findViewById(R.id.editPort);
        txtServer = (TextView)v.findViewById(R.id.txtServer);
        btnNext = (Button)v.findViewById(R.id.btnNext);

        Bundle args = getArguments();

        String accountUuid = args.getString(ARG_ACCOUNT);
        mAccount = Preferences.getPreferences(getActivity()).getAccount(accountUuid);

        try {
            ServerSettings settings = Transport.decodeTransportUri(mAccount.getTransportUri());

            if (settings.username != null) {
                editUser.setText(settings.username);
            }

            if (settings.password != null) {
                editPassword.setText(settings.password);
            }

            if (settings.host != null) {
                editServer.setText(settings.host);
            }
        }catch (Exception e) {
        }

        mDefaultPort = IMAP_PORT;
        mDefaultSslPort = IMAP_SSL_PORT;
        txtServer.setText(servers[0]);
        editPort.setText(mDefaultPort);

        spinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txtServer.setText(servers[position]);
                switch (position){
                    case 0:
                        mDefaultPort = IMAP_PORT;
                        mDefaultSslPort = IMAP_SSL_PORT;
                        mAccount.setDeletePolicy(Account.DELETE_POLICY_ON_DELETE);
                        break;
                    case 1:
                        mDefaultPort = POP3_PORT;
                        mDefaultSslPort = POP3_SSL_PORT;
                        mAccount.setDeletePolicy(Account.DELETE_POLICY_NEVER);
                        break;
                    case 2:
                        mDefaultPort = WEBDAV_PORT;
                        mDefaultSslPort = WEBDAV_SSL_PORT;
                        mAccount.setDeletePolicy(Account.DELETE_POLICY_ON_DELETE);
                        break;
                }
                int secPos = spinSecurity.getSelectedItemPosition();
                if(secPos != 2){
                    editPort.setText(mDefaultPort);
                }else{
                    editPort.setText(mDefaultSslPort);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinSecurity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != 2){
                    editPort.setText(mDefaultPort);
                }else{
                    editPort.setText(mDefaultSslPort);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editPort.setKeyListener(DigitsKeyListener.getInstance("0123456789"));

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    onNext();
                }else
                    showAlert();
            }
        });
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            mCallBack = (OnIncomingListener) activity;
        }catch(ClassCastException e){
            throw new ClassCastException(activity.toString()+ "Excepcion");
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mCallBack = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAlert(){
        Resources res = getActivity().getResources();
        new MaterialDialog.Builder(getActivity())
                .title(R.string.alert_title_invalid)
                .content(R.string.alert_invalid_info)
                .titleColor(res.getColor(R.color.accent))
                .positiveColor(res.getColor(R.color.accent))
                .positiveText(R.string.btn_ok)
                .show();
    }

    private boolean validate(){
        return Utility.requiredFieldValid(editUser) && Utility.requiredFieldValid(editPassword)
                && Utility.requiredFieldValid(editServer) && Utility.requiredFieldValid(editPort);
    }

    private void onNext(){
        try {

            String mStoreType;
            ConnectionSecurity connectionSecurity;

            switch (spinSecurity.getSelectedItemPosition()){
                case 0:
                    connectionSecurity = ConnectionSecurity.NONE;
                    break;
                case 1:
                    connectionSecurity = ConnectionSecurity.STARTTLS_REQUIRED;
                    break;
                case 2:
                    connectionSecurity = ConnectionSecurity.SSL_TLS_REQUIRED;
                    break;
                default:
                    connectionSecurity = ConnectionSecurity.NONE;
                    break;
            }

            switch (spinType.getSelectedItemPosition()){
                case 0:
                    mStoreType = ImapStore.STORE_TYPE;
                    break;
                case 1:
                    mStoreType = Pop3Store.STORE_TYPE;
                    break;
                case 2:
                    mStoreType = WebDavStore.STORE_TYPE;
                    break;
                default:
                    mStoreType = ImapStore.STORE_TYPE;
                    break;
            }

            String username = editUser.getText().toString();
            String password = editPassword.getText().toString();

            String host = editServer.getText().toString();
            int port = Integer.parseInt(editPort.getText().toString());

            //mAccount.deleteCertificate(host, port, CheckDirection.INCOMING);
            ServerSettings settings = new ServerSettings(mStoreType, host, port,
                    connectionSecurity, AuthType.PLAIN, username, password, null, null);

            mAccount.setStoreUri(Store.createStoreUri(settings));

            mAccount.setCompression(Account.TYPE_MOBILE, true);
            mAccount.setCompression(Account.TYPE_WIFI, true);
            mAccount.setCompression(Account.TYPE_OTHER, true);
            mAccount.setSubscribedFoldersOnly(true);

            mCallBack.incomingCreated(mAccount);
        } catch (Exception e) {
        }

    }

}
