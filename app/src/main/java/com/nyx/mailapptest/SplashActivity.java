package com.nyx.mailapptest;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;

/**
 *
 * Created by nyx on 1/25/15.
 *
 */
public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1000;
    private Preferences pref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        pref = Preferences.getPreferences(this);

        if(pref.hasPassword())
            pref.setLockStatus(true);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(pref.getIsFirst()){
                    showPasswordDialog();
                }else{
                    startNormal();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void showPasswordDialog(){
        pref.setFirst();
        Resources res = getResources();
        new MaterialDialog.Builder(this)
                .cancelable(false)
                .title(R.string.title_pass)
                .content(R.string.content_pass)
                .titleColor(res.getColor(R.color.accent))
                .positiveColor(res.getColor(R.color.accent))
                .negativeColor(res.getColor(R.color.gray_text))
                .positiveText(R.string.btn_ok)
                .negativeText(R.string.btn_cancel)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        toPassword();
                    }
                    @Override
                    public void onNegative(MaterialDialog dialog){
                        startNormal();
                    }
                })
                .show();
    }

    private void startNormal(){
        Account[] accounts = pref.getAccounts();
        if(accounts.length < 1)
            toNewAccount();
        else
            toInbox();
    }

    private void toPassword(){
        Intent i = new Intent(SplashActivity.this,PasswordActivity.class);
        i.putExtra("type",0);
        startActivity(i);
        finish();
    }

    private void toNewAccount(){
        Intent i = new Intent(SplashActivity.this,NewAccountActivity.class);
        startActivity(i);
        finish();
    }

    private void toInbox(){
        Intent i = new Intent(SplashActivity.this,MainActivity.class);
        startActivity(i);
        finish();
    }
}
