package com.nyx.mailapptest;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.nyx.mailapptest.lib.ConnectionSecurity;
import com.nyx.mailapptest.lib.ServerSettings;
import com.nyx.mailapptest.lib.Transport;
import com.nyx.mailapptest.lib.transport.SmtpTransport;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;

/**
 *
 * Created by nyx on 2/12/15.
 *
 */
public class ManualOutgoingFragment extends Fragment {

    private static final String ARG_ACCOUNT = "Account";

    private static final String SMTP_PORT = "587";
    private static final String SMTP_SSL_PORT = "465";

    private CheckBox checkLogin;
    private Spinner spinSecurity;
    private EditText editUser, editPassword, editPort, editServer;
    private LinearLayout layoutLogin;
    private Button btnNext;

    private Account mAccount;
    private OnOutgoingListener mCallBack;

    public interface OnOutgoingListener{
        public void outgoingCreated(Account account);
    }

    public static ManualOutgoingFragment newInstance(Account account){
        ManualOutgoingFragment frag = new ManualOutgoingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ACCOUNT, account.getUuid());
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_manual_outgoing, container, false);

        Toolbar toolbar = (Toolbar)v.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        toolbar.setTitle("Correo saliente");
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);

        checkLogin = (CheckBox)v.findViewById(R.id.checkLogin);
        spinSecurity = (Spinner)v.findViewById(R.id.spinSecurity);
        editUser = (EditText)v.findViewById(R.id.editUser);
        editPassword = (EditText)v.findViewById(R.id.editPassword);
        editServer = (EditText)v.findViewById(R.id.editServer);
        editPort = (EditText)v.findViewById(R.id.editPort);
        layoutLogin = (LinearLayout)v.findViewById(R.id.layoutLogin);
        btnNext = (Button)v.findViewById(R.id.btnNext);

        Bundle args = getArguments();
        String accountUuid = args.getString(ARG_ACCOUNT);
        mAccount = Preferences.getPreferences(getActivity()).getAccount(accountUuid);

        editPort.setKeyListener(DigitsKeyListener.getInstance("0123456789"));

        checkLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                layoutLogin.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        spinSecurity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        editPort.setText(SMTP_PORT);
                        break;
                    case 1:
                        editPort.setText(SMTP_PORT);
                        break;
                    case 2:
                        editPort.setText(SMTP_SSL_PORT);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editPort.setKeyListener(DigitsKeyListener.getInstance("0123456789"));

        try {
            ServerSettings settings = Transport.decodeTransportUri(mAccount.getTransportUri());

            // Select currently configured security type
            spinSecurity.setSelection(settings.connectionSecurity.ordinal());


            if (settings.username != null && !settings.username.isEmpty()) {
                editUser.setText(settings.username);
            }

            if (settings.password != null) {
                editPassword.setText(settings.password);
            }

            if (settings.host != null) {
                editServer.setText(settings.host);
            }

            if (settings.port != -1) {
                editPort.setText(Integer.toString(settings.port));
            }
        } catch (Exception e) {
        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNext();
            }
        });
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            mCallBack = (OnOutgoingListener) activity;
        }catch(ClassCastException e){
            throw new ClassCastException(activity.toString()+ "Excepcion");
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mCallBack = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onNext(){
        ConnectionSecurity securityType;

        switch (spinSecurity.getSelectedItemPosition()){
            case 0:
                securityType = ConnectionSecurity.NONE;
                break;
            case 1:
                securityType = ConnectionSecurity.STARTTLS_REQUIRED;
                break;
            case 2:
                securityType = ConnectionSecurity.SSL_TLS_REQUIRED;
                break;
            default:
                securityType = ConnectionSecurity.NONE;
                break;
        }

        String uri;
        String username = null;
        String password = null;
        if (checkLogin.isChecked()) {
            username = editUser.getText().toString();
            password = editPassword.getText().toString();
        }

        String newHost = editServer.getText().toString();
        int newPort = Integer.parseInt(editPort.getText().toString());
        String type = SmtpTransport.TRANSPORT_TYPE;
        ServerSettings server = new ServerSettings(type, newHost, newPort, securityType, null, username, password, null);
        uri = Transport.createTransportUri(server);
        if(mAccount != null) {
            mAccount.setTransportUri(uri);
            mCallBack.outgoingCreated(mAccount);
        }
    }

}
