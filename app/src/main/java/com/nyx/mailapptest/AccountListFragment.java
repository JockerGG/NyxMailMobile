package com.nyx.mailapptest;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;

/**
 *
 * Created by nyx on 2/10/15.
 *
 */
public class AccountListFragment extends ListFragment{

    private Account accounts[];
    private OnAccountSelectedListener mCallBack;

    public interface OnAccountSelectedListener{
        void accountSelected(Account account);
        void onAbout();
        void onSecurity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        accounts = Preferences.getPreferences(getActivity()).getAccounts();
        setListAdapter(new AccountsAdapter(getActivity(), accounts));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_account_list, container, false);

        TextView txtAbout = (TextView)v.findViewById(R.id.txtAbout);
        TextView txtSecurity = (TextView)v.findViewById(R.id.txtSecurity);

        txtAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallBack.onAbout();
            }
        });

        txtSecurity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallBack.onSecurity();
            }
        });

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            mCallBack = (OnAccountSelectedListener) activity;
        }catch(ClassCastException e){
            throw new ClassCastException(activity.toString()+ "Excepcion");
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mCallBack = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Account item = accounts[position];
        mCallBack.accountSelected(item);
    }

    class AccountsAdapter extends ArrayAdapter {

        private Context context;

        public AccountsAdapter(Context context, Account[] items) {
            super(context, R.layout.item_settings_account, items);
            this.context = context;
        }

        /** * Holder for the list items. */
        private class ViewHolder{
            TextView txtName;
            TextView txtMail;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            Account item = (Account)getItem(position);
            View v;

             LayoutInflater mInflater = (LayoutInflater) context .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                v = mInflater.inflate(R.layout.item_settings_account, null);

                holder = new ViewHolder();
                holder.txtName = (TextView)v.findViewById(R.id.lbl_account);
                holder.txtMail = (TextView)v.findViewById(R.id.lbl_mail);
                v.setTag(holder);
            } else {
                v = convertView;
                holder = (ViewHolder) v.getTag();
            }

            holder.txtName.setText(item.getName());
            holder.txtMail.setText(item.getEmail());

            return v;
        }
    }
}
