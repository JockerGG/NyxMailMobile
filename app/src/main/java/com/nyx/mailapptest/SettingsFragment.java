package com.nyx.mailapptest;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.RingtonePreference;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nyx.mailapptest.controller.MessagingController;
import com.nyx.mailapptest.data.NYXMail;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;

/**
 *
 * Created by nyx on 2/9/15.
 *
 */
public class SettingsFragment extends PreferenceFragment implements
        Preference.OnPreferenceClickListener{

    private OnDefaultChanged mCallBack;

    private static final String EXTRA_ACCOUNT = "account";

    private static final String PREFERENCE_ACCOUNT_NAME = "AccountName";
    private static final String PREFERENCE_ACCOUNT_DEFAULT = "AccountDefault";
    private static final String PREFERENCE_ACCOUNT_DELETE = "AccountDelete";
    private static final String PREFERENCE_NOTIFICATION_ENABLE = "NotificationEnable";
    private static final String PREFERENCE_NOTIFICATION_TONE = "NotificationTone";
    private static final String PREFERENCE_NOTIFICATION_VIBRATE = "NotificationVibrate";
    private static final String PREFERENCE_SIGNATURE_ENABLE = "SignatureEnable";
    private static final String PREFERENCE_SIGNATURE_TEXT = "SignatureText";
    private static final String PREFERENCE_SIGNATURE_POSITION = "SignaturePosition";


    private EditTextPreference prefAccountName,prefSignatureText;
    private CheckBoxPreference prefAccountDefault,prefNotificationEnable,prefNotificationVibrate,prefSignatureEnable;
    private RingtonePreference prefNotificationTone;
    private ListPreference prefSignaturePosition;
    private Preference prefAccountDelete;
    //social,logOut,acerca,privacidad,version,tutorial;

    private Account mAccount;

    private boolean isDefault;

    public interface OnDefaultChanged{
        public void defaultChanged();
    }

    public static SettingsFragment newInstance(String accountId){
        SettingsFragment frag = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_ACCOUNT, accountId);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        decodeArguments();
        setupBar();

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.account_preferences);

        prefAccountName = (EditTextPreference)findPreference(PREFERENCE_ACCOUNT_NAME);
        prefAccountDefault = (CheckBoxPreference)findPreference(PREFERENCE_ACCOUNT_DEFAULT);
        prefAccountDelete = findPreference(PREFERENCE_ACCOUNT_DELETE);
        prefNotificationEnable = (CheckBoxPreference)findPreference(PREFERENCE_NOTIFICATION_ENABLE);
        prefNotificationTone = (RingtonePreference)findPreference(PREFERENCE_NOTIFICATION_TONE);
        prefNotificationVibrate = (CheckBoxPreference)findPreference(PREFERENCE_NOTIFICATION_VIBRATE);
        prefSignatureEnable = (CheckBoxPreference)findPreference(PREFERENCE_SIGNATURE_ENABLE);
        prefSignatureText = (EditTextPreference)findPreference(PREFERENCE_SIGNATURE_TEXT);
        prefSignaturePosition = (ListPreference)findPreference(PREFERENCE_SIGNATURE_POSITION);

        prefAccountName.setText(mAccount.getName());
        prefAccountName.setSummary(mAccount.getName());
        prefAccountName.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final String summary = newValue.toString();
                prefAccountName.setSummary(summary);
                prefAccountName.setText(summary);
                return false;
            }
        });

        isDefault = mAccount.equals(Preferences.getPreferences(getActivity()).getDefaultAccount());
        prefAccountDefault.setChecked(isDefault);
        prefAccountDelete.setOnPreferenceClickListener(this);

        prefNotificationEnable.setChecked(mAccount.isNotifyNewMail());

        SharedPreferences prefs = prefNotificationTone.getPreferenceManager().getSharedPreferences();
        String currentRingtone = (!mAccount.getNotificationSetting().shouldRing() ? null : mAccount.getNotificationSetting().getRingtone());
        prefs.edit().putString(PREFERENCE_NOTIFICATION_TONE, currentRingtone).apply();
        prefNotificationTone.setSummary(mAccount.getNotificationSetting().getRingtone());

        prefNotificationVibrate.setChecked(mAccount.getNotificationSetting().shouldVibrate());

        prefSignatureEnable.setChecked(mAccount.getSignatureUse());

        prefSignatureText.setSummary(mAccount.getSignature());
        prefSignatureText.setText(mAccount.getSignature());
        prefSignatureText.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final String summary = newValue.toString();
                prefSignatureText.setSummary(summary);
                prefSignatureText.setText(summary);
                return false;
            }
        });

        String[] signaturePositions = getActivity().getResources().getStringArray(R.array.pref_signature_position);
        boolean isSignatureBefore = mAccount.isSignatureBeforeQuotedText();

        prefSignaturePosition.setValue(String.valueOf(isSignatureBefore));
        prefSignaturePosition.setSummary(isSignatureBefore ? signaturePositions[0] : signaturePositions [1]);
        prefSignaturePosition.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final String summary = newValue.toString();
                int index = prefSignaturePosition.findIndexOfValue(summary);
                prefSignaturePosition.setSummary(prefSignaturePosition.getEntries()[index]);
                prefSignaturePosition.setValue(summary);
                return false;
            }
        });
    }

    @Override
    public void onAttach(Activity mContext) {
        super.onAttach(mContext);

        try {
            mCallBack = (OnDefaultChanged) mContext;
        } catch (ClassCastException e) {
            throw new ClassCastException(mContext.toString() + "Excepcion");
        }

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mCallBack = null;
    }

    @Override
    public void onPause() {
        saveSettings();
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPreferenceClick(Preference pref) {
        if(pref == prefAccountDelete){
            showDeleteDialog();
            return true;
        }
        return false;
    }

    private void decodeArguments(){
        Bundle args = getArguments();
        if(args != null) {
            String accountUuid = args.getString(EXTRA_ACCOUNT);
            mAccount = Preferences.getPreferences(getActivity()).getAccount(accountUuid);
        }
    }

    private void setupBar(){
        if(mAccount != null){
            AppCompatActivity activity = (AppCompatActivity)getActivity();
            activity.getSupportActionBar().setTitle(mAccount.getEmail());
        }
    }

    private void saveSettings() {
        if(isDefault != prefAccountDefault.isChecked())
            mCallBack.defaultChanged();

        if (prefAccountDefault.isChecked()) {
            Preferences.getPreferences(getActivity()).setDefaultAccount(mAccount);
        }

        mAccount.setName(prefAccountName.getText());
        mAccount.setNotifyNewMail(prefNotificationEnable.isChecked());

        SharedPreferences prefs = prefNotificationTone.getPreferenceManager().getSharedPreferences();
        String newRingtone = prefs.getString(PREFERENCE_NOTIFICATION_TONE, null);
        if (newRingtone != null) {
            mAccount.getNotificationSetting().setRing(true);
            mAccount.getNotificationSetting().setRingtone(newRingtone);
        } else {
            if (mAccount.getNotificationSetting().shouldRing()) {
                mAccount.getNotificationSetting().setRingtone(null);
            }
        }

        mAccount.getNotificationSetting().setVibrate(prefNotificationVibrate.isChecked());

        mAccount.setSignatureUse(prefSignatureEnable.isChecked());
        mAccount.setSignature(prefSignatureText.getText());

        final boolean pos = Boolean.getBoolean(prefSignaturePosition.getValue());
        mAccount.setSignatureBeforeQuotedText(pos);

        mAccount.save(Preferences.getPreferences(getActivity()));
    }

    private void showDeleteDialog(){
        Resources res = getResources();
        new MaterialDialog.Builder(getActivity())
                .title(R.string.pref_account_delete)
                .content(R.string.alert_delete_account)
                .titleColor(res.getColor(R.color.accent))
                .positiveColor(res.getColor(R.color.accent))
                .negativeColor(res.getColor(R.color.gray_text))
                .positiveText(R.string.btn_ok)
                .negativeText(R.string.btn_cancel)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        delete();
                    }
                })
                .show();
    }

    private void delete(){
        try {
            mAccount.getLocalStore().delete();
        } catch (Exception e) {
            // Ignore, this may lead to localStores on sd-cards that
            // are currently not inserted to be left
            Log.e("DELETE ACCOUNT" , e.getMessage());
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }

        MessagingController.getInstance(getActivity().getApplication())
                .notifyAccountCancel(getActivity(), mAccount);
        Preferences.getPreferences(getActivity())
                .deleteAccount(mAccount);
        NYXMail.setServicesEnabled(getActivity());

        mAccount.deleteCertificates();

        Toast.makeText(getActivity(),R.string.toast_account_deleted, Toast.LENGTH_SHORT).show();
        Intent i = new Intent();
        i.putExtra("Deleted",true);
        getActivity().setResult(Activity.RESULT_OK, i);
        getActivity().finish();
    }
}
