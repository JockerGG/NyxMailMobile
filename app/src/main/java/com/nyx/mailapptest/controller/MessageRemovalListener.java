package com.nyx.mailapptest.controller;

import com.nyx.mailapptest.lib.Message;

public interface MessageRemovalListener {
    public void messageRemoved(Message message);
}
