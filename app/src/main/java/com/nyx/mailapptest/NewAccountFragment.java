package com.nyx.mailapptest;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.EmailAddressValidator;
import com.nyx.mailapptest.utils.Preferences;
import com.nyx.mailapptest.utils.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * Created by nyx on 1/25/15.
 *
 */
public class NewAccountFragment extends Fragment{

    private OnNewAccountListener mCallBack;

    private ImageView imgAccount;
    private EditText editMail,editPassword;

    private String mTitle;
    private HashMap<String, List<String>>providers;

    private int mIcon;
    private EmailAddressValidator mEmailValidator = new EmailAddressValidator();

    public interface OnNewAccountListener{
        void onAccountCreated(String email, String password, int icon);
    }

    public static NewAccountFragment newInstance(String title, int icon){
        NewAccountFragment frag = new NewAccountFragment();
        Bundle args = new Bundle();
        args.putString("title",title);
        args.putInt("img",icon);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        providers = new HashMap<>();

        List<String> aux = new ArrayList<>();
        aux.add("gmail.com");
        aux.add("googlemail.com");
        aux.add("google.com");
        aux.add("android.com");

        providers.put("Gmail", aux);

        List<String> aux2 = new ArrayList<>();
        aux2.add("yahoo.com");
        aux2.add("yahoo.com.mx");
        aux2.add("ymail.com");
        aux2.add("rocketmail.com");

        providers.put("Yahoo!", aux2);

        List<String> aux3 = new ArrayList<>();
        aux3.add("live.com");
        aux3.add("live.com.mx");
        aux3.add("hotmail.com");
        aux3.add("hotmail.com.mx");
        aux3.add("outlook.com");
        aux3.add("outlook.mx");
        aux3.add("msn.com");
        aux3.add("msn.com.mx");

        providers.put("Outlook", aux3);

        List<String> aux4 = new ArrayList<>();
        aux4.add("itelcel.com");

        providers.put("iTelcel", aux4);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_new_account, container, false);

        Toolbar toolbar = (Toolbar)v.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        toolbar.setTitle("");
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);

        imgAccount = (ImageView)v.findViewById(R.id.img_account);
        editMail = (EditText)v.findViewById(R.id.editMail);
        editPassword = (EditText)v.findViewById(R.id.editPassword);
        CheckBox checkShowPassword = (CheckBox)v.findViewById(R.id.checkPassword);
        Button btnAdd = (Button)v.findViewById(R.id.btnAdd);

        setupBar();

        checkShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                showPassword(isChecked);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields()){
                    if(!validateMailProvider())
                        showAlert(R.string.alert_title_invalid, R.string.alert_provider);
                    else {
                        if (validateNewMail())
                            mCallBack.onAccountCreated(editMail.getText().toString(), editPassword.getText().toString(), mIcon);
                        else
                            showAlert(R.string.alert_title_invalid, R.string.alert_same_mail);
                    }
                }else
                    showAlert(R.string.alert_title_invalid,R.string.alert_invalid_info);
                }
        });

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            mCallBack = (OnNewAccountListener) activity;
        }catch(ClassCastException e){
            throw new ClassCastException(activity.toString()+ "Excepcion");
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mCallBack = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.empty, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupBar(){
        Bundle args = getArguments();

        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        ActionBar bar =  activity.getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        if(args != null){
            bar.setTitle(args.getString("title"));
            mTitle = args.getString("title");
            mIcon = args.getInt("img");
            imgAccount.setImageResource(mIcon);
        }
    }
    private boolean validateFields() {
        String email = editMail.getText().toString();

        return Utility.requiredFieldValid(editMail)
                && Utility.requiredFieldValid(editPassword)
                && mEmailValidator.isValidAddressOnly(email);
    }

    private boolean validateMailProvider(){
        String email = editMail.getText().toString();

        email = email.substring(email.indexOf("@")+1);
        List<String> aux = providers.get(mTitle);

        if(aux == null)
            return true;

        return aux.contains(email);
    }

    private boolean validateNewMail(){
        String email = editMail.getText().toString();
        Account[] accs = Preferences.getPreferences(getActivity().getApplicationContext()).getAccounts();
        for(int i = 0; i < accs.length; i++){
            Account acc = accs[i];
            if(acc.getEmail().equals(email)){
                return false;
            }
        }
        return true;
    }

    private void showPassword(boolean show) {
        if (show) {
            editPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            editPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    private void showAlert(int title, int desc){
        Resources res = getActivity().getResources();
        new MaterialDialog.Builder(getActivity())
                .title(title)
                .content(desc)
                .titleColor(res.getColor(R.color.accent))
                .positiveColor(res.getColor(R.color.accent))
                .positiveText(R.string.btn_ok)
                .show();
    }

}
