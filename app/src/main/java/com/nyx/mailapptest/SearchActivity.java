package com.nyx.mailapptest;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.ViewGroup;

import com.nyx.mailapptest.activity.MessageReference;
import com.nyx.mailapptest.data.App;

/**
 *
 * Created by nyx on 2/9/15.
 *
 */
public class SearchActivity extends BaseActivity implements
        MailListFragment.OnMailSelected{

    private static final int MAIL_ACTIVITY = 1;

    private SearchView searchView;
    private String mAccountId;

    private MailListFragment listFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Show the Up button in the action bar.
        setupActionBar();

        mAccountId = getIntent().getExtras().getString(MailListFragment.EXTRA_SEARCH_ACCOUNT);

        handleIntent(getIntent());
    }

    @Override
    public void onStart(){
        super.onStart();
        App.activity4 = this;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        App.activity4 = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setIconified(false);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        Bundle extras = null;
        if (data != null)
            extras = data.getExtras();
        switch (requestCode) {
            case MAIL_ACTIVITY:
                if (extras != null) {
                    MessageReference ref = extras.getParcelable("message");
                    if (extras.getBoolean("next"))
                        listFragment.openNext(ref);
                    else
                        listFragment.openPrevious(ref);
                }
                break;
        }
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getResources().getString(R.string.search_all_messages_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onMailCompose(){
        Intent i = new Intent(SearchActivity.this,ComposeActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.appear_bottom, R.anim.disappear_zoom);
    }

    @Override
    public void onMailSelected(MessageReference reference){
        Intent i = new Intent(SearchActivity.this, MailActivity.class);
        i.putExtra("message",reference);
        startActivityForResult(i,MAIL_ACTIVITY);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onMailSelected(MessageReference reference, int pos){
        Intent i = new Intent(SearchActivity.this, MailActivity.class);
        i.putExtra("message",reference);
        startActivityForResult(i,MAIL_ACTIVITY);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDraftSelected(MessageReference ref){
        ComposeActivity.actionEditDraft(this, ref);
    }

    public void onNewIntent(Intent i){
        setIntent(i);
        handleIntent(i);
    }

    private void handleIntent(Intent i){

        if(Intent.ACTION_SEARCH.equals(i.getAction())){
            String query = i.getStringExtra(SearchManager.QUERY);
            Bundle bundle = new Bundle();
            bundle.putString(SearchManager.QUERY, query);

            bundle.putString(MailListFragment.EXTRA_SEARCH_ACCOUNT, mAccountId);

            isFragmentActive();

            listFragment = new MailListFragment();
            listFragment.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.content, listFragment).commit();}

    }

    private boolean isFragmentActive(){

        Fragment fra = getFragmentManager().findFragmentById(R.id.content);
        if(fra != null)
        {
            getFragmentManager().beginTransaction().remove(fra).commit();
            ViewGroup vg = (ViewGroup)findViewById(R.id.content);
            vg.removeAllViews();
            return true;
        }
        return false;
    }
}
