package com.nyx.mailapptest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nyx.mailapptest.R;
import com.nyx.mailapptest.activity.FolderInfoHolder;
import com.nyx.mailapptest.search.SearchAccount;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.BaseAccount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * Created by nyx on 1/27/15.
 *
 */
public class FoldersAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<BaseAccount> mAccounts; // header titles
    private List<FolderInfoHolder> mFolders = new ArrayList<>();
    // child data in format of header title, child title
    private HashMap<BaseAccount, List<String>> _listDataChild;
    private int childSelected, parentSelected;

    private int[] folders = {R.drawable.inbox_selector, R.drawable.draft_selector, R.drawable.sent_selector,
            R.drawable.archive_selector, R.drawable.trash_selector,R.drawable.spam_selector};

    public FoldersAdapter(Context mContext, List<BaseAccount> mAccounts,
                                 HashMap<BaseAccount, List<String>> listChildData) {
        this.mContext = mContext;
        this.mAccounts = mAccounts;
        this._listDataChild = listChildData;
        childSelected = 0;
        parentSelected = 2;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this.mAccounts.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_folder, null);
        }

        ImageView imgFolder = (ImageView)convertView.findViewById(R.id.img_folder);
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lbl_folder);

        imgFolder.setImageResource(folders[childPosition]);

        if(groupPosition == parentSelected && childPosition == childSelected){
            imgFolder.setSelected(true);
            txtListChild.setTextColor(mContext.getResources().getColor(R.color.accent));
        }else{
            imgFolder.setSelected(false);
            txtListChild.setTextColor(mContext.getResources().getColor(R.color.black));
        }

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(groupPosition >= 2)
            return this._listDataChild.get(this.mAccounts.get(groupPosition)).size();
        else
            return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mAccounts.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mAccounts.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_account, null);
        }

        ImageView imgAccount = (ImageView)convertView.findViewById(R.id.img_account);
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lbl_account);

        if(parentSelected < 2 && groupPosition == parentSelected){
            imgAccount.setSelected(true);
            lblListHeader.setTextColor(mContext.getResources().getColor(R.color.accent));
        }else{
            imgAccount.setSelected(false);
            lblListHeader.setTextColor(mContext.getResources().getColor(R.color.black));
        }

        BaseAccount headerTitle = (BaseAccount) getGroup(groupPosition);
        if(headerTitle instanceof SearchAccount) {
            imgAccount.setImageResource(R.drawable.inbox_selector);
            lblListHeader.setText(headerTitle.getDescription());
        }else {
            Account ac = (Account)headerTitle;
            imgAccount.setImageResource(ac.getIcon());
            lblListHeader.setText(ac.getName());
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void selectedItem(int parentSelected, int childSelected){
        this.parentSelected = parentSelected;
        this.childSelected = childSelected;
        notifyDataSetChanged();
    }

    public void setSelectedChild(int childSelected){
        this.childSelected = childSelected;
        notifyDataSetChanged();
    }

}
