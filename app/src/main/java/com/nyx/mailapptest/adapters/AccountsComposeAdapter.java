package com.nyx.mailapptest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nyx.mailapptest.R;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * Created by nyx on 2/4/15.
 *
 */
public class AccountsComposeAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private List<Account> mItems;

    public AccountsComposeAdapter(Context context) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        List<Account> items = new ArrayList<>();
        Preferences prefs = Preferences.getPreferences(context.getApplicationContext());
        Collection<Account> accounts = prefs.getAvailableAccounts();

        Account aux = null;
        for (Account account : accounts) {
            Account mAc = account;
            if (mAc == prefs.getDefaultAccount()) {
                aux = mAc;
                items.remove(mAc);
            }else
                items.add(account);
        }
        mItems = items;
        mItems.add(0, aux);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return (mItems.get(position) instanceof Account) ? 0 : 1;
    }

    @Override
    public Account getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Account item = mItems.get(position);
        View view;

        if (convertView != null && convertView.getTag() instanceof AccountHolder) {
            view = convertView;
        } else {
            view = mLayoutInflater.inflate(R.layout.item_compose_account, parent, false);
            AccountHolder holder = new AccountHolder();
            holder.name = (TextView) view.findViewById(R.id.txt_account_id);
            view.setTag(holder);
        }

        AccountHolder holder = (AccountHolder) view.getTag();
        holder.name.setText(item.getDescription());

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        Account item = mItems.get(position);

        View view;

        if (convertView != null && convertView.getTag() instanceof AccountHolder) {
            view = convertView;
        } else {
            view = mLayoutInflater.inflate(R.layout.item_compose_dropdown, parent, false);
            AccountHolder holder = new AccountHolder();
            holder.name = (TextView) view.findViewById(R.id.txt_account_id);
            holder.mail = (TextView) view.findViewById(R.id.txt_account_mail);
            view.setTag(holder);
        }

        AccountHolder holder = (AccountHolder) view.getTag();
        holder.name.setText(item.getDescription());
        holder.mail.setText(item.getEmail());


        return view;
    }

    static class AccountHolder {
        public TextView name;
        public TextView mail;
    }

}
