package com.nyx.mailapptest;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.nyx.mailapptest.data.App;
import com.nyx.mailapptest.utils.Account;
import com.nyx.mailapptest.utils.Preferences;

/**
 *
 * Created by nyx on 2/10/15.
 *
 */
public class SettingsActivity extends BaseActivity implements
        AccountListFragment.OnAccountSelectedListener,
        SettingsFragment.OnDefaultChanged{

    private boolean isChanged = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Show the Up button in the action bar.
        setupActionBar();

        getFragmentManager().beginTransaction().replace(R.id.content, new AccountListFragment()).commit();

    }

    @Override
    public void onStart(){
        super.onStart();
        App.activity6 = this;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        App.activity6 = null;
    }

    @Override
    public void onBackPressed() {
        // turn on the Navigation Drawer image;
        // this is called in the LowerLevelFragments
        if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            getSupportActionBar().setTitle("Ajustes");
        }else {
            if (isChanged) {

                App.activity1.finish();

                Intent i = new Intent(SettingsActivity.this,MainActivity.class);
                startActivity(i);
                finish();

            } else{
                setResult(RESULT_OK);
            }
            super.onBackPressed();
        }
    }

    @Override
    public void accountSelected(Account account){
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, SettingsFragment.newInstance(account.getUuid()))
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onAbout(){
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, new AboutFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onSecurity(){
        if(Preferences.getPreferences(this).getPassword().isEmpty()){
            Intent i = new Intent(SettingsActivity.this, PasswordActivity.class);
            i.putExtra("type", 0);
            startActivity(i);
        }else {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content, new SecuritySettingsFragment()
                    )
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void defaultChanged(){
        isChanged = true;
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Ajustes");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
