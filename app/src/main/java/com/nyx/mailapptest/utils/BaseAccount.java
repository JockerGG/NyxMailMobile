package com.nyx.mailapptest.utils;

public interface BaseAccount {
    public String getEmail();
    public void setEmail(String email);
    public String getDescription();
    public void setDescription(String description);
    public String getUuid();
}
